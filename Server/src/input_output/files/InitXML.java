package input_output.files;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class InitXML {
	
	static String WORLD_FOLDERS_LOCATION = "Database\\";
	static String TEMPLATE_LOCATION = "Database\\Template\\";
	
	/**
	 * The function initializes the World by creating a folder and a 'Account -
	 * List' File by copying it from the Template folder.
	 * 
	 * @param WorldName
	 *            The name of the world and folder that will be created.
	 * @return True if it worked and false if the world name was taken
	 */
	public static boolean initWorld(String WorldName) {
		File dir = new File(WORLD_FOLDERS_LOCATION + WorldName);
		if (!dir.exists()) {
			dir.mkdir();
			dir = new File(WORLD_FOLDERS_LOCATION + WorldName + "\\Account");
			dir.mkdir();
			File from = new File(TEMPLATE_LOCATION
					+ "Template - Account List.xml");
			File to = new File(WORLD_FOLDERS_LOCATION + WorldName
					+ "\\Account\\Account List.xml");
			try {
				Files.copy(from.toPath(), to.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		} else
			return false;
	}
	
	/**
	 * Copy's a template Character from the "Template\\Character - Template.xml"
	 * and inserts it into the specified Account XML. Thereafter the function
	 * also alters the template code tagname for the specific new character.
	 * 
	 * @param WorldName
	 *            The name of the world in which folder the XML file is
	 * @param AccountName
	 *            The name of the account in whose XML file will be changed
	 * @param CharacterName
	 *            The name of the new character that will be added into the XML
	 *            file
	 * @return The function return 1 if succes, 0 if file couldn't be found, -1
	 *         if password is wrong and 2 if character name is taken.
	 */
	public static int addCharacter(String WorldName, String AccountName,
			String CharacterName) {
		int returnValue = checkWorldPath(WorldName);
		if (returnValue == 1) {
			returnValue = 0;
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(WORLD_FOLDERS_LOCATION
						+ WorldName + "\\Account\\" + AccountName + ".xml");

				DocumentBuilderFactory docFactory_Copy = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder_Copy = docFactory_Copy
						.newDocumentBuilder();
				Document doc_Copy = docBuilder_Copy.parse(TEMPLATE_LOCATION
						+ "\\Template - Account - Character.xml");

				Node imported = doc.importNode(doc_Copy.getFirstChild(), true);
				for (int i = 0; i < doc.getFirstChild().getChildNodes()
						.getLength(); i++)
					if (CharacterName.equals(doc.getFirstChild()
							.getChildNodes().item(i).getNodeName()))
						returnValue = 2;
				if (returnValue != 2) {
					doc.getFirstChild().appendChild(imported);
					returnValue = 1;
				}

				// write the content into XML file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(
						WORLD_FOLDERS_LOCATION + WorldName + "\\Account\\"
								+ AccountName + ".xml"));
				transformer.transform(source, result);

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			} catch (TransformerException tfe) {
				tfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (SAXException sae) {
				sae.printStackTrace();
			}

			if (returnValue == 1) {
				try {
					DocumentBuilderFactory docFactory = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder docBuilder = docFactory
							.newDocumentBuilder();
					Document doc = docBuilder.parse(WORLD_FOLDERS_LOCATION
							+ WorldName + "\\Account\\Account List.xml");
					Node acc = null;

					NodeList accounts = doc.getFirstChild().getChildNodes();
					for (int i = 0; i < accounts.getLength(); i++)
						if (AccountName.equals(accounts.item(i).getNodeName()))
							acc = accounts.item(i);

					NodeList accountNodes = acc.getChildNodes();
					Element newCharacter = doc.createElement("Character");
					newCharacter.setTextContent(CharacterName);
					for (int i = 0; i < accountNodes.getLength(); i++)
						if ("Characters".equals(accountNodes.item(i)
								.getNodeName()))
							accountNodes.item(i).appendChild(newCharacter);

					// write the content into XML file
					TransformerFactory transformerFactory = TransformerFactory
							.newInstance();
					Transformer transformer = transformerFactory
							.newTransformer();
					DOMSource source = new DOMSource(doc);
					StreamResult result = new StreamResult(new File(
							WORLD_FOLDERS_LOCATION + WorldName
									+ "\\Account\\Account List.xml"));
					transformer.transform(source, result);

				} catch (ParserConfigurationException pce) {
					pce.printStackTrace();
				} catch (TransformerException tfe) {
					tfe.printStackTrace();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				} catch (SAXException sae) {
					sae.printStackTrace();
				}
				returnValue = EditXML.renameCharacter(WorldName, AccountName,
						"CharacterName", CharacterName);
			}
		}
		return returnValue;
	}
	
	/**
	 * Creates an account that is registered in the "Account List.xml" file and
	 * gets a XML file with the AccountName as title.
	 * 
	 * @param WorldName
	 *            To find the correct folder
	 * @param AccountName
	 *            To know what to call the new account
	 * @param Password
	 *            Changes the default value 'Password' into the requested
	 *            Password
	 * @return Returns true if the creation worked and false if account name
	 *         already is in use.
	 */
	public static boolean createAccount(String WorldName, String AccountName,
			String Password) {
		File from = new File(TEMPLATE_LOCATION + "Template - Account.xml");
		File to = new File(WORLD_FOLDERS_LOCATION + WorldName + "\\Account\\"
				+ AccountName + ".xml");
		int folder = checkWorldPath(WorldName);
		if (!to.exists() && folder == 1) {
			try {
				Files.copy(from.toPath(), to.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(WORLD_FOLDERS_LOCATION
						+ WorldName + "\\Account\\Account List.xml");

				DocumentBuilderFactory docFactory_Copy = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder_Copy = docFactory_Copy
						.newDocumentBuilder();
				Document doc_Copy = docBuilder_Copy.parse(TEMPLATE_LOCATION
						+ "\\Template - Account List - Account.xml");

				Node imported = doc.importNode(doc_Copy.getFirstChild(), true);
				doc.getFirstChild().appendChild(imported);

				// write the content into XML file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(
						WORLD_FOLDERS_LOCATION + WorldName
								+ "\\Account\\Account List.xml"));
				transformer.transform(source, result);

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			} catch (TransformerException tfe) {
				tfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (SAXException sae) {
				sae.printStackTrace();
			}
			EditXML.renameAccount(WorldName, "AccountName", AccountName);
			EditXML.changePassword(WorldName, AccountName, Password);
			return true;
		} else {
			return false;
		}
	}

	public static int checkWorldPath(String WorldName) {
		int returnValue = 0;
		File folder = new File(InitXML.WORLD_FOLDERS_LOCATION + WorldName);
		if (folder.exists())
			returnValue = 1;
		return returnValue;
	}
}
