package input_output.files;


import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class WriteXML {

	/**
	 * Deletes the character from the specific account. Alters the information
	 * in "Account List.xml" and the account file.
	 * 
	 * @param WorldName
	 *            To find the correct folder
	 * @param AccountName
	 *            The name of the account that should be changed
	 * @param CharacterName
	 *            The characters name
	 * @return 1 if success, 0 if file couldn't be found
	 */
	public static int deleteCharacter(String WorldName, String AccountName,
			String CharacterName) {
		int returnValue = InitXML.checkWorldPath(WorldName);
		if (returnValue == 1) {
			returnValue = 0;
			// Vi b�rjar med "'AccountName'.xml"
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(InitXML.WORLD_FOLDERS_LOCATION
						+ WorldName + "\\Account\\" + AccountName + ".xml");
				/*
				 * Kolla vart karakt�ren �r i listan
				 */
				NodeList allChar = doc.getFirstChild().getChildNodes();
				for (int i = 0; i < allChar.getLength(); i++)
					if (CharacterName.equals(allChar.item(i).getNodeName())) {
						doc.getFirstChild().removeChild(allChar.item(i));
						returnValue = 1;
					}

				// write the content into XML file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(
						InitXML.WORLD_FOLDERS_LOCATION + WorldName + "\\Account\\"
								+ AccountName + ".xml"));
				transformer.transform(source, result);

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			} catch (TransformerException tfe) {
				tfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (SAXException sae) {
				sae.printStackTrace();
			}
		}
		returnValue = 0;
		// Uppdaterar "Account List.xml"
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc1 = docBuilder.parse(InitXML.WORLD_FOLDERS_LOCATION + WorldName
					+ "\\Account\\Account List.xml");

			NodeList accountNodes = null;
			
			NodeList accounts = doc1.getFirstChild().getChildNodes();

			// F� tag i r�tt account
			for (int i = 0; i < accounts.getLength(); i++) {
				if (AccountName.equals(accounts.item(i).getNodeName())){
					accountNodes = accounts.item(i).getChildNodes();
				}
			}

			// F� tag i alla characters under ett specifikt accountnamn samt
			// l�gger alla characters i en nodelista
			NodeList characters = null;
			Node characterParent = null;
			for (int i = 0; i < accountNodes.getLength(); i++)
				if ("Characters".equals(accountNodes.item(i).getNodeName())) {
					characterParent = accountNodes.item(i);
					characters = accountNodes.item(i).getChildNodes();
				}
			
			// Hittar position p� den character som vi vill f� bort
			for (int j = 0; j < characters.getLength(); j++){
				if (CharacterName.equals(characters.item(j).getTextContent())) {
					characterParent.removeChild(characters.item(j));
					returnValue = 1;
				}
			}
			// write the content into XML file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc1);
			StreamResult result = new StreamResult(new File(
					InitXML.WORLD_FOLDERS_LOCATION + WorldName
							+ "\\Account\\Account List.xml"));
			transformer.transform(source, result);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
		return returnValue;
	}

	/**
	 * Deletes an account which is specified by 'AccountName'. This removes the
	 * account file and changes the properties in 'Account List.xml'.
	 * 
	 * @param WorldName
	 *            To find the correct folder
	 * @param AccountName
	 *            This account will be deleted
	 * @return 1 if success, 0 if the file couldn't be found.
	 */
	public static int deleteAccount(String WorldName, String AccountName) {

		int returnValue = InitXML.checkWorldPath(WorldName);
		if (returnValue == 1) {
			returnValue = 0;
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(InitXML.WORLD_FOLDERS_LOCATION
						+ WorldName + "\\Account\\Account List.xml");

				NodeList accounts = doc.getFirstChild().getChildNodes();

				for (int i = 0; i < accounts.getLength(); i++)
					if (AccountName.equals(accounts.item(i).getNodeName())) {
						doc.getFirstChild().removeChild(accounts.item(i));
						returnValue = 1;
					}

				File accountFile = new File(InitXML.WORLD_FOLDERS_LOCATION + WorldName
						+ "\\Account\\" + AccountName + ".xml");
				accountFile.delete();

				// write the content into XML file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(
						InitXML.WORLD_FOLDERS_LOCATION + WorldName
								+ "\\Account\\Account List.xml"));
				transformer.transform(source, result);

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			} catch (TransformerException tfe) {
				tfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (SAXException sae) {
				sae.printStackTrace();
			}
		}
		return returnValue;
	}

	/**
	 * Deletes the world folder.
	 * 
	 * @param WorldName
	 *            The name of the folder that will be deleted.
	 * @return True if delete, false if the file didn't exist or wasn't deleted.
	 */
	public static boolean deleteWorld(String WorldName) {
		File dir = new File(InitXML.WORLD_FOLDERS_LOCATION + WorldName);
		return deleteDir(dir);
	}

	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		// The directory is now empty so delete it
		return dir.delete();
	}

}
