/*
 * A general XML reader that reads a single branch in a
 * XML file and retracts the desired elements from it.
 * 
 * Will return an array list with every Branch
 * that matches the description.
 */

package input_output.files;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class ReadXML {
	static final String ACCOUNT = "Account";
	static final String NAME = "Name";
	static final String PASS = "Password";
	static final String CHAR = "Character";
	static XMLEventReader eventReader;
	static String[] temp = null;
	static ArrayList<String[]> tempret;
	
	/**
	 * 
	 * @param XMLFile path for file to be read
	 * @param Branches the branch to be read from
	 * @param Elements the Elements to be read
	 * @return
	 */
	public static ArrayList<String[]> readConfig(String XMLFile, String Branch, ArrayList<String> Elements) {
		tempret = new ArrayList<String[]>();
		
//		System.out.println(XMLFile);
//		System.out.println(Branch);
//		for(int i = 0;i<Elements.size();i++){
//			System.out.println(Elements.get(i));
//		}

		
		try {
			// First create a new XMLInputFactory
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			// Setup a new eventReader
			InputStream in = new FileInputStream(XMLFile);
			eventReader = inputFactory.createXMLEventReader(in);
			System.out.println("---");
			branch(Branch, Elements);
			// Read the XML document


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		
		return tempret;
	}

	private static void branch(String Branch, ArrayList<String> Elements){
		boolean add = false;
		while (eventReader.hasNext()) {
			try {
				XMLEvent event = eventReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					if (startElement.getName().getLocalPart().equals(Branch)) {
						add = true;
						temp = new String[Elements.size()];
						for(int i = 0;i<Elements.size();i++){
							temp[i] = "";
						}
					}
					if(add==true && event.isStartElement()){
//						System.out.println(event.asStartElement().getName().getLocalPart());
						for(int i = 0;i<Elements.size();i++){
							if(event.asStartElement().getName().getLocalPart().equals(Elements.get(i))) {
								event = eventReader.nextEvent();
								if(temp[i].equals("")){
									temp[i] = (event.asCharacters().getData());
								}
								else{
									temp[i] += "&" + (event.asCharacters().getData());
								}
//								System.out.println(Elements.get(i));
								break;
							}
						}
					}

				}
				if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					if (endElement.getName().getLocalPart().equals(Branch)) {
						add = false;
//						System.out.println("Adding: " + temp[0]);
						tempret.add(temp);
					}
				}

			} catch (XMLStreamException e) {
				e.printStackTrace();
			}
		}
	}
//	public static void main(String[] args){
//		ArrayList<String> test = new ArrayList<String>();
//		test.add("Name");
//		test.add("Password");
//		test.add("Character");
//
//		ArrayList<String[]> TestList = readConfig("Database\\Accounts.xml", "Account2", test);
//		for(int i = 0;i<TestList.size();i++){
//			for(int j = 0;j<TestList.get(i).length;j++){
//				System.out.println(TestList.get(i)[j]);
//			}
//		}
//	}

} 