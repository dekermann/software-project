package input_output.files;


import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class EditXML {
	
	/**
	 * Modify() changes the specific Account defined by 'AccountName', therein
	 * the specific Character, specified by 'CharacterName' and therein the
	 * specific Tag value.
	 * 
	 * @param WorldName
	 *            To find the correct folder
	 * @param AccountName
	 *            The name of the account that should be changed
	 * @param CharacterName
	 *            The characters name
	 * @param Tagname
	 *            The name of the tag that can be found in the XML file, in
	 *            other word the skill or character attributes
	 * @param newValue
	 *            The new Value that will replace the old value
	 * @return 2 if success, 1 if the file was found but the Tagname was
	 *         incorrect, 0 if file couldn't be found
	 * @see Change in the specified account XML
	 */
	public static int Modify(String WorldName, String AccountName,
			String CharacterName, String Tagname, String newValue) {
		int returnValue = InitXML.checkWorldPath(WorldName);
		if (returnValue == 1) {
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(InitXML.WORLD_FOLDERS_LOCATION
						+ WorldName + "\\Account\\" + AccountName + ".xml");

				/*
				 * Kolla vart karakt�ren �r i listan
				 */
				NodeList allChar = doc.getChildNodes();
				int charPos = 0;
				for (int i = 0; i < allChar.getLength(); i++)
					if (CharacterName.equals(allChar.item(i).getNodeName()))
						charPos = i;

				/*
				 * H�mta alla Tags med 'TagName' och hitta karakt�rens tag
				 */
				NodeList allTags = doc.getElementsByTagName(Tagname);

				/*
				 * �ndra i den karakt�ren som �ndringen ska ske i. OBS!!
				 * 'CharPos' multipliceras d� XML noderna laddas �ver dubbelt
				 * f�r tagsen
				 */
				if (allTags.item(charPos) != null && allTags.getLength() != 0) {
					allTags.item(charPos).setTextContent(newValue);
					returnValue = 2;
				}

				// write the content into XML file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(
						InitXML.WORLD_FOLDERS_LOCATION + WorldName + "\\Account\\"
								+ AccountName + ".xml"));
				transformer.transform(source, result);

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			} catch (TransformerException tfe) {
				tfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (SAXException sae) {
				sae.printStackTrace();
			}
		}
		return returnValue;
	}
	
	/**
	 * RenameCharacter() changes the name of the character by entering both the
	 * account file and the account list and changing the character Node names
	 * of the specified character. If the given password isn't correct nothing
	 * will be changed.
	 * 
	 * @param WorldName
	 *            To find the folder
	 * @param AccountName
	 *            To find the correct account
	 * @param CharacterName
	 *            To find the correct character that will be changed
	 * @param NewCharacterName
	 *            The current character name will be changed with this name
	 * @return 1 if success and 0 if the file, account or character coulnd't be
	 *         found.
	 */
	public static int renameCharacter(String WorldName, String AccountName,
			String CharacterName, String NewCharacterName) {
		int returnValue = InitXML.checkWorldPath(WorldName);
		if (returnValue == 1) {
			returnValue = 0;
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(InitXML.WORLD_FOLDERS_LOCATION
						+ WorldName + "\\Account\\" + AccountName + ".xml");

				NodeList characters = doc.getFirstChild().getChildNodes();

				for (int i = 0; i < characters.getLength(); i++) {
					if (CharacterName.equals(characters.item(i).getNodeName())) {
						doc.renameNode(characters.item(i), null,
								NewCharacterName);
						returnValue = 1;
					}
				}
				// write the content into XML file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(
						InitXML.WORLD_FOLDERS_LOCATION + WorldName + "\\Account\\"
								+ AccountName + ".xml"));
				transformer.transform(source, result);

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			} catch (TransformerException tfe) {
				tfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (SAXException sae) {
				sae.printStackTrace();
			}
			if (returnValue == 1) {
				returnValue = 0;
				try {
					DocumentBuilderFactory docFactory = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder docBuilder = docFactory
							.newDocumentBuilder();
					Document doc = docBuilder.parse(InitXML.WORLD_FOLDERS_LOCATION
							+ WorldName + "\\Account\\Account List.xml");

					NodeList accounts = doc.getFirstChild().getChildNodes();
					NodeList accountNodes = null;
					for (int i = 0; i < accounts.getLength(); i++)
						if (AccountName.equals(accounts.item(i).getNodeName()))
							accountNodes = accounts.item(i).getChildNodes();

					NodeList characters = null;
					for (int a = 0; a < accountNodes.getLength(); a++)
						if ("Characters".equals(accountNodes.item(a)
								.getNodeName()))
							characters = accountNodes.item(a).getChildNodes();

					for (int a = 0; a < characters.getLength(); a++)
						if (CharacterName.equals(characters.item(a)
								.getNodeValue())) {
							characters.item(a).setTextContent(NewCharacterName);
							returnValue = 1;
						}

					// write the content into XML file
					TransformerFactory transformerFactory = TransformerFactory
							.newInstance();
					Transformer transformer = transformerFactory
							.newTransformer();
					DOMSource source = new DOMSource(doc);
					StreamResult result = new StreamResult(new File(
							InitXML.WORLD_FOLDERS_LOCATION + WorldName
									+ "\\Account\\Account List.xml"));
					transformer.transform(source, result);

				} catch (ParserConfigurationException pce) {
					pce.printStackTrace();
				} catch (TransformerException tfe) {
					tfe.printStackTrace();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				} catch (SAXException sae) {
					sae.printStackTrace();
				}
			}
		}
		return returnValue;
	}

	/**
	 * This funktion renames the given account and checks the password in the
	 * process to make shure the change is authorized.
	 * 
	 * @param WorldName
	 *            To find the correct folder
	 * @param Name
	 *            To find the correct account
	 * @param NewName
	 *            Used in changing the name
	 * @return 1 if the change is made and 0 if the account or file couldn't be
	 *         found.
	 */
	public static int renameAccount(String WorldName, String AccountName,
			String NewName) {
		int returnValue = InitXML.checkWorldPath(WorldName);
		if (returnValue == 1) {
			returnValue = 0;
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(InitXML.WORLD_FOLDERS_LOCATION
						+ WorldName + "\\Account\\Account List.xml");

				NodeList accounts = doc.getFirstChild().getChildNodes();

				for (int i = 0; i < accounts.getLength(); i++) {
					if (AccountName.equals(accounts.item(i).getNodeName())) {
						doc.renameNode(accounts.item(i), null, NewName);
						returnValue = 1;
						File accountFile = new File(InitXML.WORLD_FOLDERS_LOCATION
								+ WorldName + "\\Account\\" + AccountName
								+ ".xml");
						File newAccountFile = new File(InitXML.WORLD_FOLDERS_LOCATION
								+ WorldName + "\\Account\\" + NewName + ".xml");
						accountFile.renameTo(newAccountFile);
					}
				}
				// write the content into XML file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(
						InitXML.WORLD_FOLDERS_LOCATION + WorldName
								+ "\\Account\\Account List.xml"));
				transformer.transform(source, result);

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			} catch (TransformerException tfe) {
				tfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (SAXException sae) {
				sae.printStackTrace();
			}
		}
		return returnValue;
	}
	
	/**
	 * This funktion can change the account password and by sending in the same
	 * password for OldPassword.
	 * 
	 * @param WorldName
	 *            To find the correct folder
	 * @param AccountName
	 *            This account will be checked
	 * @param NewPassword
	 *            If the old password is correct then the password will be
	 *            exchanged with the new.
	 * @return 1 if success and been changed and 0 if the file couldn't be
	 *         found.
	 */
	public static int changePassword(String WorldName, String AccountName,
			String NewPassword) {
		int returnValue = InitXML.checkWorldPath(WorldName);
		if (returnValue == 1) {
			returnValue = 0;
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(InitXML.WORLD_FOLDERS_LOCATION
						+ WorldName + "\\Account\\Account List.xml");

				NodeList accounts = doc.getFirstChild().getChildNodes();
				for (int i = 0; i < accounts.getLength(); i++) {
					if (AccountName.equals(accounts.item(i).getNodeName()))
						accounts.item(i).getChildNodes().item(1)
								.setTextContent(NewPassword);
					returnValue = 1;
				}

				// Write the content into XML file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(
						InitXML.WORLD_FOLDERS_LOCATION + WorldName
								+ "\\Account\\Account List.xml"));
				transformer.transform(source, result);

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			} catch (TransformerException tfe) {
				tfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (SAXException sae) {
				sae.printStackTrace();
			}
		}
		return returnValue;
	}
}
