package input_output.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Sender {
	
	private DatagramSocket DS;
	
	/**
	 * Constructs a sender
	 */
	public Sender(){
		init();
	}
	
	/**
	 * Attempts to send a message to desired client
	 * @param s Message to be send
	 * @param IP Client to receive message
	 * @param Port Used port by client
	 */
	public void sendMessage(String s, InetAddress IP, int Port){
//		System.out.println("Attempting to send: " + s + " to: " + IP + " on port: " + Port);
		byte[] bmsg = s.getBytes();
		DatagramPacket reply = new DatagramPacket(bmsg, bmsg.length,  
				IP, Port);
		try {
			DS.send(reply);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Closes the socket
	 */
	public void close(){
		DS.close();
	}
	
	private void init(){
		try {
			DS = new DatagramSocket();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
