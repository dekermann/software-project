/*
 * Read the file 'Account Login Standards' for the buffer standard.
 * This class will be waiting in it's own thread to receive a datagram
 * from a new client.
 */

package input_output.network;

import helpers.ConversionHelper;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import server.database.account.AccountHandler;


public class Account_Listener extends Thread {

	private DatagramSocket DS;
	private AccountHandler AH;
	private DatagramPacket DP_buffer;

	/**
	 * Attempts to create a thread Listening on a port ranged from PortStart - PortStop.
	 * @param PortStart attempts to start with this port if it fails increases the port by +1
	 * @param PortStop the last port to be tried
	 * @param AH need to be the servers AccountHandler, this is where the collected data will be sent.
	 */
	public Account_Listener(int PortStart, int PortStop, AccountHandler AH){
		this.AH = AH;
		
		for(int i = PortStart;i<PortStop;i++){
			try {
				DS = new DatagramSocket(i);
				System.out.println("Successfully created a Socket on port: " + i);
				break;
			}
			catch (SocketException e) {
				e.printStackTrace();
				System.err.println("Failed to create server on port: " + i);
			}
		}
		
		byte[] buffer = new byte[36];
		DP_buffer = new DatagramPacket(buffer, buffer.length);
	}

	/**
	 * Starts the listening thread. This thread will listen to incoming data
	 * on the chosen socket and relay it to the AccountHandler.
	 */
	public void run(){
		System.out.println("Account_Listener has been started on port: " + DS.getLocalPort());
		while(true){
			try {
				DS.receive(DP_buffer);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Login Request Received:");
			//This will convert the received data to an array of AccountName and AccountPassword
			String[] temp = ConversionHelper.convertLoginRequest(DP_buffer.getData());
			//This will then send the received information + IP address to the account handler
			AH.loginRequest(temp[0], temp[1], temp[2], DP_buffer.getAddress());
		}
	}
	
	/**
	 * 
	 * @return the used port number.
	 */
	public int getPort(){
		return DS.getLocalPort();
	}
}