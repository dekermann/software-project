/*
 * Read the file 'User Request Standards' for better buffer and conversion understanding
 */

package input_output.network;

import helpers.ConversionHelper;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import server.client.ClientHandler;

public class User_Listener extends Thread {
	
	private ClientHandler CH;
	private DatagramSocket DS;
	private DatagramPacket DP_buffer;
	
	/**
	 * 
	 * @param PortStart
	 * @param PortStop
	 * @param CH
	 */
	public User_Listener(int PortStart, int PortStop, ClientHandler CH) {
		this.CH = CH;
		
		for(int i = PortStart; i < PortStop; i++) {
			try {
				DS = new DatagramSocket(i);
				System.out.println("Successfully created a Socket on port: " + i);
				break;
			} catch (SocketException e) {
				e.printStackTrace();
				System.err.println("Failed to create server on port: " + i);
			}
		}
		


		
	}
	
	public void run(){
		System.out.println("User_Listener has been started on port: " + DS.getLocalPort());
		byte[] buffer = new byte[100];
		while(true){
			DP_buffer = new DatagramPacket(buffer, buffer.length);
			try {
				DS.receive(DP_buffer);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String[] temp = ConversionHelper.convertUserRequestType(DP_buffer.getData());
			CH.userRequest(temp, DP_buffer.getAddress());
		}
	}
	
	/**
	 * 
	 * @return the used port number.
	 */
	public int getPort(){
		return DS.getLocalPort();
	}
}
