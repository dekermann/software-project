/*
 * This class will send objects to the client instead of bruteforce convert
 * all objects to byte arrays
 */

package input_output.network;

import java.net.*;
import java.io.*;

public class ObjectSender {
	
	/**
	 * Converts an Object threw an ObjectOutputStream and sends it to desired client
	 * @param Object to be converted
	 * @param IP of the client
	 * @param Port the client listens on (server socket port)
	 */
	public static void sendObject(Object Object, InetAddress IP, int Port){
		try{
			Socket receivingSocket = new Socket(IP, Port);
			OutputStream OS = receivingSocket.getOutputStream();
			ObjectOutputStream OOS = new ObjectOutputStream(OS);
			
			OOS.writeObject(Object);
			OOS.flush();
			OOS.close();
			OS.close();
			receivingSocket.close();
		}
		catch(Exception e){
			System.err.println("Failed to send object to: " + IP + " on port: " + Port);
			System.err.println(e);
		}
	}
}