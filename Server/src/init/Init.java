/*
 * Server version 0.1
 * 
 * To use the code or parts of it please contact: 
 * David Ekermann on daeke@kth.se or dekermann on bitbucket.
 */

package init;
import server.Server;

public class Init {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Server Server = new Server();
		Server.start();

	}
}
