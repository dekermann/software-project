/*
 * A helper class to convert byte arrays to string arrays.
 */

package helpers;

public class ConversionHelper {

	private static final char NULL = '0';

	/**
	 * Converts a login request from an array of bytes to 2 set of strings containing
	 * the AccountName and AccountPassword. Will remove all the 0's in the end of the account.
	 * @param buffer to be converted (must be 32bytes long)
	 * @return Answer array, containing AccountName in the first slot and AccountPassword in the second. 
	 * Returns null if fail.
	 */
	public static String[] convertLoginRequest(byte[] buffer){
		if(buffer.length != 36){
			return null;
		}
		String AccountName = "";
		for(int i = 0;i<16;i++){
			if ((char) buffer[i] == NULL){
				break;
			}
			AccountName += (char) buffer[i];
		}
		System.out.println("AccountName: " + AccountName);
		String AccountPassword = "";
		for(int i = 16;i<32;i++){
			if (buffer[i] == NULL){
				break;
			}
			AccountPassword += (char) buffer[i];
		}
		System.out.println("AccountPassword: " + AccountPassword);
		String ClientPort = "";
		for(int i = 32;i<36;i++){
			ClientPort += (char) buffer[i];
		}
		System.out.println("DesiredPort: " + ClientPort);
		String[] Answer = new String[3];
		Answer[0] = AccountName;
		Answer[1] = AccountPassword;
		Answer[2] = ClientPort;
		return Answer;
	}

	/**
	 * Converts the request from a user to 2 strings in an array with the first string being the Type of request
	 * and the second string being the request Data
	 * @param userRequest Request to be converted
	 * @return Size = String[2]: String[0] = Type, String[1] = Data
	 */
	public static String[] convertUserRequestType(byte[] userRequest){
		String[] temp = new String[2];
		temp[0] = "Bad Request";
		temp[1] = "";
		switch((char) userRequest[0]) {
		case '0':
			temp[0] = "COMMAND";
			for(int i = 1;i<10;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;
		case '1': 
			temp[0] = "CREATE CHARACTER";
			for(int i = 1;i<17;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;
		case '2':
			temp[0] = "CHOOSE CHARACTER";
			for(int i = 1;i<17;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;
		case '3':
			temp[0] = "DELETE CHARACTER";
			for(int i = 1;i<17;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;
		case '4':
			temp[0] = "MOVE";
			for(int i = 1;i<33;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;
		case '5':
			temp[0] = "ATTACK";
			for(int i = 1;i<33;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;
		case '6':
			temp[0] = "SPECIAL";
			for(int i = 1;i<33;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;
		case '7':
			temp[0] = "SELF SPECIAL";
			for(int i = 1;i<17;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;
		case '8':
			temp[0] = "USE OBJECT";
			for(int i = 1;i<33;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;
		case '9':
			temp[0] = "CHAT";
			for(int i = 1;i<101;i++){
				temp[1] += (char) userRequest[i];
			}
			return temp;

		}
		return temp;
	}
}
