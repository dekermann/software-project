/*
 * The Server class works as an umbrella for all the other classes,
 * it will contain all the necessary references and acknowledge server-wide
 * updates from clients.
 */

package server;

import java.net.InetAddress;
import input_output.network.Account_Listener;
import input_output.network.Sender;
import input_output.network.User_Listener;
import server.NPC.NPCHandler;
import server.broadcast.Broadcast;
import server.client.ClientHandler;
import server.database.account.AccountHandler;
import server.database.items.ItemLoader;
import server.mechanics.AttackInteraction;


public class Server {

	private int ClientsID = -1;
	private boolean running = false;
	private ClientHandler CH;
	private AccountHandler AH;
	private Account_Listener AL;
	private Sender S;
	private User_Listener UL;
	public static int publicPort;
	public static int privatePort;
	private NPCHandler NPCH;
	private WorldInit InitGameWorld;
	/**
	 * Constructs the Server
	 */
	public Server(){
		init();
	}

	/**
	 * Initializer for the Server
	 */
	private void init(){
		CH = new ClientHandler(this);
		AH = new AccountHandler(this);
		AL = new Account_Listener(1,100, AH);
		UL = new User_Listener(4444,8888, CH);
		NPCH = new NPCHandler(this);
		NPCH.setNPCHch(CH);
		publicPort = AL.getPort();
		privatePort = UL.getPort();
		ItemLoader.LoadItems("Database\\World\\Item\\Item List");
		AttackInteraction.setClientHandler(CH);
		AttackInteraction.setNPCHandler(NPCH);
		S = new Sender();
		NPCH.setNPCHsender(S);
		Broadcast.setSender(S);
		Broadcast.setClientHandler(CH);
		Broadcast.setNPCHandler(NPCH);
		//Places objects and npc in the game world;
		InitGameWorld = new WorldInit(NPCH);
		InitGameWorld.spawn();
	}

	/**
	 * Starts the already constructed server
	 */
	public void start(){
		this.running = true;
		AL.start();
		UL.start();
	}

	/**
	 * Creates and adds a Client to the ClientHandler, will also respond with the set of available characters
	 * @param IP to be added to the Client
	 * @param Characters to be sent back to the Client
	 * @param AccountName that the Client logged in with
	 * @param Port that the client wishes to be contacted on
	 */
	public void addClient(InetAddress IP, String Characters, String AccountName, int Port){
		if (running){
			CH.addNewClient(ClientsID, IP, Port, AccountName);
			ClientsID--;
			System.out.println(Characters);
			System.out.println(IP);
			System.out.println(Port);

			S.sendMessage(privatePort + "&" + Characters + "?", IP, Port);
		}
	}

	public void ConfirmUserRequest(String Msg, InetAddress IP , int Port ){
		S.sendMessage(Msg, IP, Port);
	}
}
