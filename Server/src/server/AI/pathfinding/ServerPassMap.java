package server.AI.pathfinding;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.BitSet; 
import java.util.Scanner;

/**
 * 
 * @author Victor
 *
 */
public class ServerPassMap 
{
	/**
	 * Load a map and translate it to a bitset that will serve as our passmap
	 * @param path			The path to the mapfile we want to convert to a passmap
	 * @return				A passmap in the for of a  bitset with 1 being passable 0 not
	 */
	
	public static BitSet setPass(String path)
	{

		BitSet passMap = new BitSet();
		try {
			//Open file and get the map size
			File file = new File(path);
			Scanner scanner = new Scanner(file);
			short mapSizeX = scanner.nextShort();
			short mapSizeY = scanner.nextShort();

			//Now, for every passable terrain, set the corresponding bit to 1 in the bitset, and leave the rest unchanged
			for (int y=0; y<mapSizeY; y++)
				for (int x=0; x<mapSizeX; x++)
					if(passCheck(scanner.nextShort()))
						passMap.set(x+(y*mapSizeX));
			scanner.close();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}

		
		return passMap;
	}

	/**
	 * This function simply checks if a tile is passable or not
	 * @param tile			The tile's type number.
	 * @return				True if passable, false if not
	 */
	private static boolean passCheck(short tile)
	{
		switch(tile){
		case -1:
		case 1: 
		case 2: 
		case 3: 
		case 4: 
		case 6: 
		case 7: 
		case 8: 
		case 9: 
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
			//15 till 27 = passable
		case 28:
		case 29:
		case 30:
		case 31:
			//32 till 44 = passable
		case 45:
		case 46:
		case 47:
		case 48:
			return false;
		default:
			return true;
		}
	}
}