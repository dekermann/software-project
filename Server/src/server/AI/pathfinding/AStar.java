package server.AI.pathfinding;

import java.awt.Point;
import java.util.LinkedList;
import java.util.BitSet; 

import server.broadcast.Objects;

public class AStar {
	private static LinkedList<point> openSet = new LinkedList<point>(); //List with possible nodes we can check
	private static LinkedList<point> closedSet = new LinkedList<point>(); //List with nodes that has been checked
	private static LinkedList<Point> path = new LinkedList<Point>(); //The list with path that we return once we are done
	private static point current;

	/**
	 * 
	 * @param Src 			The position the enemy starts on
	 * @param goal			The destination for the enemy
	 * @param passSize		The size of the area (PassSize*PassSize) of the passmap we want to actually check.
	 * @param passMap		The bitset with the entire maps passmap
	 * @return				A Point list with the path from start to finish with each step in order
	 */
	//The function to call when you want to run A*
	//The current formula expects a square map. If the map is not square, line 19 and 29 will break
	//If it for some reason needs to be changed, we would need to add another parameter with the maps actual width
	public static LinkedList<Point> runAStar(Point Src,Point goal, int passSize, BitSet passMap)
	{

		//Calculate the offset for the area we are checking on the map
		int xOffset = Src.x-passSize/2;
		int yOffset = Src.y-passSize/2;

		//Calculate the relative positions for start and destination coordinates
		int startX = passSize/2;
		int startY = passSize/2;
		int stopX = goal.x-(Src.x-passSize/2);
		int stopY = goal.y-(Src.y-passSize/2);

		//Check if our start/stop position is outside the map
		if(startX < 0 || startY < 0 || stopX < 0 || stopY < 0)
			return null;
		if (startX >= Math.sqrt(passMap.length()) || startY >= Math.sqrt(passMap.length()) || stopX >= Math.sqrt(passMap.length()) || stopY >= Math.sqrt(passMap.length()))
			return null;

		//Create a node grid based on the pass map
		point [][] grid = new point[passSize][passSize];
		for(int y = 0; y < passSize; y++)
			for(int x = 0; x < passSize; x++)
			{
				grid[x][y]= new point(x,y);
				if(passMap.get(xOffset + x + ((yOffset + y) * (int) Math.sqrt(passMap.length()))))
					grid[x][y].pass = true;
				else
					grid[x][y].pass = false;				
			}

		//Run A* and return it
		return pathFinding(grid[startX][startY], grid[stopX][stopY], grid,Src.x-passSize/2,Src.y-passSize/2);
	}

	/**
	 * 
	 * @param start			Starting position
	 * @param stop			Destination
	 * @param grid			The area we are checking for a path for
	 * @param originX		The offset X for the area we are checking. 
	 * @param originY		The offset Y
	 * @return				A Point list with the path from start to finish with each step in order
	 */
	//This is the actual A* algorithm
	public static LinkedList<Point> pathFinding(point start, point stop, point [][] grid,int originX,int originY)
	{
		//First, check if we are standing on an impassable node. If we do, break
		if (!start.pass)
			return null;

		//Start with marking the start position as the origin node, giving it correct cost and parent (NULL) 
		start.gCost = 0;
		start.fCost = hueristicCost(start,stop);
		start.parent = null;
		openSet.add(start);

		//As long as we have options left, perform the A* algorithm
		while (! openSet.isEmpty())
		{
			//Pick the node with the lowest final cost and continue from there
			currentNode();

			//If the node is the destination, reconstruct the path and send it back
			if (current == stop)
			{
				reconstructPath(current.parent,(originX),(originY));

				//Always remember to clear your linked list once done
				openSet.clear();
				closedSet.clear();

//				for(int i = 0; i<path.size();i++){
//					System.out.println("(" + path.get(i).x+" " + path.get(i).y + ")");
//				}

				return path;
			}

			//Move the current node from opened to closed set
			closedSet.add(current);
			openSet.remove(current);

			//Now, check around the current node for traversable nodes and calculate their costs
			//Check in a 3x3 grid with the current node in the middle
			for(int y= current.y-1; y < current.y+2; y++)
				for(int x = current.x-1; x < current.x+2; x++)
					//Now rule out nodes outside the room, the current node itself, impassable nodes and nodes we've already checked
					if(x>=0 && x < grid.length && y>=0 && y < grid.length)
						if (!(x == current.x && y == current.y))
							if(grid[x][y].pass && !closedSet.contains(grid[x][y]))
								if(!  checkForObject(x+originX,y+originY) || (stop.x == x && stop.y == y))
									//If anode has already been added to the open set before, check if we need to update the costs and if, do it
									if (openSet.contains(grid[x][y]))
									{
										if (x != current.x && y != current.y)
										{
											//Diagonal movement
											if(current.gCost + 14 < grid[x][y].gCost)
											{
												grid[x][y].gCost = current.gCost + 14;
												grid[x][y].fCost = grid[x][y].gCost + hueristicCost(grid[x][y],stop);
												grid[x][y].parent = current;
											}
										}
										else
										{
											//Orthogonal movement
											if(current.gCost + 10 < grid[x][y].gCost)
											{
												grid[x][y].gCost = current.gCost + 10;
												grid[x][y].fCost = grid[x][y].gCost + hueristicCost(grid[x][y],stop);
												grid[x][y].parent = current;
											}
										}
									}
									else
									{
										//If the node has not been added to the set of possible traversable nodes before, do it and assign a cost
										openSet.add(grid[x][y]);
										grid[x][y].parent = current;
										//Diagonal movement
										if (x != current.x && y != current.y)
											grid[x][y].gCost = current.gCost + 14;
										//Orthogonal movement
										else
											grid[x][y].gCost = current.gCost + 10;
										grid[x][y].fCost = grid[x][y].gCost + hueristicCost(grid[x][y],stop);
									}
		}
		return null; //If everything else fails, return null and end the algorithm
	}

	private static boolean checkForObject(int x,int y)
	{
		return Objects.containKey(new Point(x,y));
	}

	//Find the node in the open set with lowest cost.
	//If there are several nodes with the lowest cost,
	//prioritize the newest
	private static void currentNode()
	{
		current = openSet.getFirst();
		for (int i=1; i < openSet.size(); i++)
			if (current.fCost >= openSet.get(i).fCost)
				current = openSet.get(i);
	}

	/**
	 * 
	 * @param node					
	 * @param originX			The offset X for the area we are checking a path for
	 * @param originY			The offset Y
	 */
	//Trace back the path from stop to start and add it to a list
	private static void reconstructPath(point node,int originX,int originY)
	{
		//if ((  node.parent  ).parent != null){
		if(  node.parent != null  ){
			if ((  node.parent  ).parent != null){
				reconstructPath(node.parent,originX,originY);
			}
		}
		path.add(new Point(node.x+originX,node.y+originY));
	}

	/**
	 * 
	 * @param start			Current position
	 * @param stop			Destination
	 * @return				Estimated lowest cost for reaching destination without obstacles
	 */
	//Use the Manhattan method to estimate the hueristic cost
	private static int hueristicCost(point start, point stop)
	{
		return 10*(Math.abs(start.x - stop.x) + Math.abs(start.y - stop.y));
	}
}
