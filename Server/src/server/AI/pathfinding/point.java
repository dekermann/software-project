package server.AI.pathfinding;


/**
 * 
 * @author Victor Davidsson
 *
 */

public class point {

	public int x,y;
	public int gCost,fCost; 
	public boolean pass;
	public point parent;
	
	public point(int xPos, int yPos)
	{
		x = xPos;
		y = yPos;
	}
}
