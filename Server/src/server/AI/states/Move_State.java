package server.AI.states;

import java.awt.Point;
import java.util.LinkedList;

import server.AI.pathfinding.AStar;
import server.AI.pathfinding.ServerPassMap;
import server.NPC.NPC;
import server.broadcast.Broadcast;
import server.broadcast.Objects;


public class Move_State implements AIState {

	private String changeState = AIStateManager.MOVE;
	private final long timer = 1200;
	private final int wait = 5;

	@Override
	public String whichState(NPC npc) {
		return AIStateManager.MOVE;
	}

	@Override
	public String changeState(NPC npc) {
		changeState = AIStateManager.MOVE;
		/*
		 * if the monster is close enough to the target, it stops and changes its state to inactive.
		 */
		if(npc.hasTarget()){
			if(!npc.getTarget().DEADPLAYER){
				int xDistance = Math.abs(npc.getMonster().getCoordinates().x - npc.getTarget().onlineOn().getCharacter().getCoordinates().x); 
				int yDistance = Math.abs(npc.getMonster().getCoordinates().y - npc.getTarget().onlineOn().getCharacter().getCoordinates().y);

				if(  Math.hypot(xDistance, yDistance) > 14  ){
					changeState = AIStateManager.INACTIVE;

				}
				else if (Math.hypot(xDistance, yDistance) < 2){
					changeState = AIStateManager.ATTACK;
				}

				if(npc.getPath() == null){
					changeState = AIStateManager.INACTIVE;
				}
			}
			else{
				npc.clearTarget();
				changeState = AIStateManager.INACTIVE;
			}
		}
		else{
			changeState = AIStateManager.INACTIVE;
		}

		if(  !npc.getMonster().isAlive()  ){
			changeState = AIStateManager.DEAD;
		}

		return changeState;
	}

	public void enter(NPC npc) {
		//		System.out.println("Enters Move_State " + npc.getID());
		changeState = AIStateManager.MOVE;
		//System.out.println(System.getProperty("user.dir") + "\\tileMap.map");

		npc.clearPath();

		double xDistance = Math.abs(npc.getMonster().getCoordinates().x - npc.getTarget().onlineOn().getCharacter().getCoordinates().x); 
		double yDistance = Math.abs(npc.getMonster().getCoordinates().y - npc.getTarget().onlineOn().getCharacter().getCoordinates().y);
		double distance =  Math.hypot(xDistance, yDistance);

		//System.out.println("target: " + (npc.getTarget() !=null));
		if(distance > 1){
			npc.setPath(  AStar.runAStar(  npc.getMonster().getCoordinates(),  npc.getTarget().onlineOn().getCharacter().getCoordinates(),  30,  ServerPassMap.setPass(System.getProperty("user.dir") + "\\tileMap.map")));
			//			System.out.println("AStar i MOVE_STATE:Enter");
		}
		npc.setTime(System.currentTimeMillis());
	}

	public boolean canAct(NPC npc){
		long temp = System.currentTimeMillis();
		if((  temp - npc.getTime()  ) > getSpeed(npc)){
			return true;
		}
		return false;
	}

	private double getSpeed(NPC npc){
		return 1000/npc.getMonster().getSpeed();
	}

	public void acted(NPC npc){
		long temp = System.currentTimeMillis();
		npc.setTime(temp);
	}

	@Override
	public void update(  NPC npc  ) {
		//System.out.println("update: " + canAct(npc));
		changeState = AIStateManager.MOVE;
		if(  canAct(  npc  )  ){
			if(  npc.getPath().isEmpty() || (  npc.getPath() == null  ) || (  npc.getActCounter() > wait)  || npc.getMoveCounter() > 2){
				npc.resetMoveCounter();
				if(  npc.getPath() != null  ){
					npc.clearPath();
					//					System.out.println("npc.clearPath()");
				}
				npc.setPath(  AStar.runAStar(  npc.getMonster().getCoordinates(),  npc.getTarget().onlineOn().getCharacter().getCoordinates(),  30,  ServerPassMap.setPass(System.getProperty("user.dir") + "\\tileMap.map")));
				//				System.out.println("npc.setPath(AStar.runAStar(...))");
				if(npc.getActCounter() > wait){
					npc.resetActCounter();
				}
			}
			else{
				//System.out.println("Update() Move_State at mytime: " + npc.getTime() + " for id " + npc.getID());
				//System.out.println("Gamla X: " + npc.getMonster().getCoordinates().x + "Gamla Y: " + npc.getMonster().getCoordinates().y);
				//System.out.println("Nya X: " + npc.getPathHead().x + "Nya Y: " + npc.getPathHead().y);

				////
				//				LinkedList<Point> anders = npc.getPath();
				//				//			
				//				System.out.println();
				//				for(int i = 0; i<anders.size();i++){
				//					System.out.print("(" +anders.get(i).x + " " + anders.get(i).y +") ");
				//				}
				//				System.out.println();
				//			////
				//			Objects.editObject(npc.getMonster().getCoordinates(), new Point(npc.getMonster().getCoordinates().x+1,npc.getMonster().getCoordinates().y));
				//			npc.getMonster().setCoordinates(new Point(npc.getMonster().getCoordinates().x+1,npc.getMonster().getCoordinates().y));
				if(  !(  Objects.containKey(  npc.getPathHead()  )  )  ){

					Objects.editObject(npc.getMonster().getCoordinates(), npc.getPathHead());
					//					System.out.println("Rad 100");

					npc.getMonster().setCoordinates(  npc.getPathHead()  );

					Broadcast.npcBroadcast(npc.getMonster().getCoordinates(), npc.getMonster().getCoordinates(), npc);
					//					System.out.println("Rad 105");

					//System.out.println("Kommer jag n�gonsin hit?");
					npc.removeHead();
					//					System.out.println("Rad 109");
					acted(npc);
					npc.addMoveCounter();
					//					System.out.println(npc.getID() + "s kordinater efter move: " + npc.getMonster().getCoordinates());
					npc.resetActCounter();
				}
				else{
					npc.addActCounter();
					if(npc.getActCounter() > wait){
						System.out.println(  npc.getActCounter()  );

					}
				}
			}
		}
	}

	@Override
	public void exit(NPC npc) {
		npc.clearPath();
		npc.resetActCounter();
		//		System.out.println("Exit() Move_State "  + npc.getID());
	}
}