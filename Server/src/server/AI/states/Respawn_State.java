package server.AI.states;

import server.NPC.NPC;
import server.broadcast.Broadcast;
import server.broadcast.Objects;

public class Respawn_State implements AIState {

	private String changeState = AIStateManager.RESPAWN;

	@Override
	public String whichState(NPC npc) {
		// TODO Auto-generated method stub
		return AIStateManager.RESPAWN;
	}

	@Override
	public String changeState(NPC npc) {

		// TODO Auto-generated method stub
		changeState = AIStateManager.RESPAWN;
		if (npc.getMonster().getCoordinates() == npc.getMonster().getSpawn()) {
			changeState = AIStateManager.INACTIVE; 
		}

		if(  !npc.getMonster().isAlive()  ){
			changeState = AIStateManager.DEAD;
		}

		return changeState;

	}

	@Override
	public void enter(NPC npc) {
		// TODO Auto-generated method stub
		npc.getMonster().reset();
//		System.out.println("Enters Respawn_State(): " + npc.getID());

	}

	@Override
	public void update(NPC npc) {
		changeState = AIStateManager.RESPAWN;
		if(!Objects.containKey(npc.getMonster().getSpawn()) ){
			Broadcast.npcBroadcast(npc.getMonster().getCoordinates(), npc.getMonster().getSpawn(), npc);
			Objects.editObject(npc.getMonster().getCoordinates(), npc.getMonster().getSpawn());
			npc.getMonster().setCoordinates(  npc.getMonster().getSpawn()  );
			Broadcast.npcBroadcast(npc.getMonster().getCoordinates(), npc.getMonster().getCoordinates(), npc);
		}
	}

	@Override
	public void exit(NPC npc) {
		// TODO Auto-generated method stub
//		System.out.println("Exit() Respawn_State: " + npc.getID());
	}

	@Override
	public boolean canAct(NPC npc) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void acted(NPC npc) {

	}

}