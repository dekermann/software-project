package server.AI.states;

import server.NPC.NPC;
import server.broadcast.Broadcast;

public class Dead_State implements AIState {

	private String changeState = AIStateManager.DEAD;
	private final long timer = 1200;

	@Override
	public String whichState(NPC npc) {
		return AIStateManager.DEAD;
	}

	@Override
	public String changeState(NPC npc) {
		changeState = AIStateManager.DEAD;
		if((System.currentTimeMillis() - npc.getTime())  > (30000)){
			changeState= AIStateManager.RESPAWN;
		}

		return changeState;
	}

	@Override
	public void enter(NPC npc) {  
//		System.out.println("Enter() id " + npc.getID());
		acted(npc);
		Broadcast.npcBroadcast(npc.getMonster().getCoordinates(), npc.getMonster().getCoordinates(), npc);
	}

	@Override
	public void update(NPC npc) {
		changeState = AIStateManager.DEAD;
	}

	@Override
	public void exit(NPC npc) {
//		System.out.println("Exit() npc " + npc.getID());

	}

	public boolean canAct(NPC npc){
		long temp = System.currentTimeMillis();
		if((  temp - npc.getTime()  ) > timer){
			return true;
		}
		return false;
	}

	public void acted(NPC npc){
		npc.setTime(  System.currentTimeMillis()  );
	}
}