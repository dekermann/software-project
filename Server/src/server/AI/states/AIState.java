package server.AI.states;


import server.NPC.*;

public interface AIState {
	
	/**
	 * returns the state of the current npc
	 * @param npc the npc to return the state from
	 * @return the state of the npc
	 */
	public String whichState(NPC npc);
	
	/**
	 * this function checks if the npc should change its current state into something else
	 * @param npc the npc to check if the state is supposed to be changed for
	 * @return the current state of the npc
	 */
	public String changeState(NPC npc);
	
	/**
	 * the function that is run as soon as the npc enters a new state
	 * @param npc the npc that is updated
	 */
	public void enter(NPC npc);
	
	/**
	 * the method that is run everytime the ai reaches that specific npc
	 * @param npc the npc that is updated
	 */
	public void update(NPC npc);
	
	/**
	 * the method that is run when a state is exited
	 * @param npc the npc to exit the state in
	 */
	public void exit(NPC npc);
	
	/**
	 * this function declares if an NPC can act or not. It can do so once every second (or depending on the value given)
	 * @param npc the NPC that is checked for
	 * @return true if the NPC can act or false if they can't
	 */
	public boolean canAct(NPC npc);
	public void acted(NPC npc);

}
