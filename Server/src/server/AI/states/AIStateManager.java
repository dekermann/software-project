package server.AI.states;


import java.util.LinkedList;

import server.NPC.*;
/**
 * 
 * @author Anders Gemstone and Jonas Hastaen
 *
 */

public class AIStateManager extends Thread {

	public static final String MOVE = "MOVE_STATE";
	public static final String INACTIVE = "INACTIVE_STATE";
	public static final String ATTACK = "ATTACK_STATE";
	public static final String DEAD = "DEAD_STATE";
	public static final String RESPAWN = "RESPAWN_STATE";
	private boolean run;

	private LinkedList<NPC> ActiveList = new LinkedList<NPC>();

	public Move_State move = new Move_State();
	public Inactive_State inactive = new Inactive_State();
	public Attack_State attack = new Attack_State();
	public Dead_State dead = new Dead_State();
	public Respawn_State respawn = new Respawn_State();

	private String currentState;

	/**
	 * called to update the state of the AI
	 * @return true
	 */
	public boolean update(){
		return true;
	}

	/**
	 * removes a specific npc from the active list. It sets the ai thread 
	 * to inactive before doing this so that there are less concurrency problems. It sets the ai thread to true before returning
	 * @param npc the npc to remove from the list
	 * @return true if it managed to remove the npc from the list, or false if there was no such npc to remove.
	 */
	public boolean removeActiveNPC(NPC npc){
		setRun(false);

		if(ActiveList.contains(npc)){
			ActiveList.remove(npc);
			setRun(true);
			return true;
		}

		setRun(true);
		return false;
	}

	/**
	 * adds another npc to the active list. It sets the ai thread to 
	 * false for a while so it can do this without interruption. it
	 * sets the thread to true again before returning.
	 * @param npc the npc to add to the active list.
	 */
	public void addActiveNPC(NPC npc){
		setRun(false);
		System.out.println("Size of list: " + ActiveList.size());
		ActiveList.add(npc);
		System.out.println("Size of list: " + ActiveList.size());
		System.out.println("NPCSTATE: " + npc.getState());
		setRun(true);
		System.out.println(run);
	}

	/**
	 * checks if the inparameter already exists in the active list or not
	 * @param npc the npc to check if its contained in the activelist already
	 * @return true if the item is in the list, false if not
	 */
	public boolean containsID(int npcID){
		for(int i = 0; i<ActiveList.size();i++){
			if(ActiveList.get(i).getID() == npcID){
				return true;
			}
		}
		return false;
	}

	/**
	 * inceptionfunktionen
	 * ain ska egentligen bara hela tiden rulla igenom listan av npcer och kolla vad varje npc ska g�ra
	 */
	public void run(){
		System.out.println("running AISM");
		System.out.println(run);
		while(true){
		//	System.out.println("true loop aism");
			while(true){
			//	System.out.println("inside run");
				//System.out.println(ActiveList.size());
				//				if(ActiveList.size() == 0){
				//					run = false;
				//				}
				Long tmp = System.currentTimeMillis();
				//System.out.println("Size of ActiveList: " + ActiveList.size());
				for(int i = 0;i<ActiveList.size();i++){
					NPC currentNPC = ActiveList.get(i);
					//k�r update f�r npc i statet som den npcn har
					currentNPC.getState().update(currentNPC);
					//System.out.println(" ");
					String temp = currentNPC.getState().changeState(currentNPC); 


					//INACTIVE_STATE != INACTIVE_STATE

					if(temp != currentNPC.getState().whichState(currentNPC)){
						currentNPC.getState().exit(currentNPC);


						switch(temp){
						case MOVE:
							ActiveList.get(i).setState(move);
							break;
						case INACTIVE:
							ActiveList.get(i).setState(inactive);
							break;
						case ATTACK:
							ActiveList.get(i).setState(attack);
							break;
						case RESPAWN:
							ActiveList.get(i).setState(respawn);
							break;
						case DEAD:
							ActiveList.get(i).setState(dead);
						}
						ActiveList.get(i).getState().enter(ActiveList.get(i));
					}
				}	
//				System.out.println("-----------TIME IT TOOK----------:" + (System.currentTimeMillis() - tmp));
			}
		}
	}
	
	/**
	 * turns on the ai or turns off the ai
	 * @param run true if the ai is supposed to be on, false if not. note that the ai thread is still running, it just doesn't do anything.
	 */
	public void setRun(boolean run){
		this.run = run;
	}
}