package server.AI.states;

import server.NPC.*;
import server.broadcast.Broadcast;
import server.broadcast.Objects;
import server.client.*;

public class Inactive_State implements AIState{

	private String changeState = AIStateManager.INACTIVE;
	private final long timer = 1000;

	@Override
	public String whichState(NPC npc) {
		return AIStateManager.INACTIVE;
	}

	@Override
	public String changeState(NPC npc) {
		changeState = AIStateManager.INACTIVE;
		/*
		 * this function checks if the monster is within a specific range of the target it has aquired
		 */
//		System.out.println("ChangeState() Inactive_State");
		if(npc.hasTarget()){
			int xDistance = Math.abs(npc.getMonster().getCoordinates().x - npc.getTarget().onlineOn().getCharacter().getCoordinates().x); 
			int yDistance = Math.abs(npc.getMonster().getCoordinates().y - npc.getTarget().onlineOn().getCharacter().getCoordinates().y);
			double distance = Math.hypot(xDistance, yDistance); 
			
			if( (  distance < 15 )  ){
				changeState = AIStateManager.MOVE;
			}

			if(  distance > 15  ){
				npc.clearTarget();
			}
		}
		
		//TODO: the 2000000 value is 'bona fida', as we have no way of measuring it to be exactly 20 seconds
		if((System.currentTimeMillis() - npc.getTime()) > 2000){
//			System.out.println("ENTERING RESPAWN");
			changeState= AIStateManager.RESPAWN;
		}
		
		if(  !npc.getMonster().isAlive()  ){
			changeState = AIStateManager.DEAD;
		}
		
		return changeState;
	}

	@Override
	public void enter(NPC npc) {	
		/*
		 * the moment an npc enters the inactive state, it clears its target and doesn't do anything more
		 */
		changeState = AIStateManager.INACTIVE;
//		System.out.println("Enters Inactive_State " + npc.getID());
		acted(npc);
	}

	public boolean canAct(NPC npc){
		long temp = System.currentTimeMillis();
		if((  temp - npc.getTime()  ) > timer){
			return true;
		}
		return false;
	}

	public void acted(NPC npc){
		long temp = System.currentTimeMillis();
		npc.setTime(temp);
	}

	@Override
	public void update(NPC npc) {
		// TODO Auto-generated method stub
		changeState = AIStateManager.INACTIVE;
//	System.out.println("Update() Inactive_State");
		
		/*
		 * ge en sorts 'healbuff' till botten, dvs. �ka dess health reg.
		 * g�r inget annat �n att sitta och trolla
		 */
	}

	@Override
	public void exit(NPC npc) {
		/*
		 * enda buffen om det �r en buff, annars g�r inget annat
		 */
		// TODO Auto-generated method stub
//		System.out.println("Exit() Inactive_State " + npc.getID());
	}
}