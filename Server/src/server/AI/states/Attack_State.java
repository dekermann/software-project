package server.AI.states;

import java.awt.Point;
import java.util.LinkedList;

import server.NPC.NPC;
import server.broadcast.Broadcast;
import server.broadcast.Objects;
import server.mechanics.AttackInteraction;

import server.NPC.NPC;

public class Attack_State implements AIState{

	private String changeState = AIStateManager.ATTACK;
	private final long timer = 2000;
	private final int wait = 5;

	@Override
	public String whichState(NPC npc) {
		return AIStateManager.ATTACK;
	}

	@Override
	public String changeState(NPC npc) {
		changeState = AIStateManager.ATTACK;
		
		int xDistance = Math.abs(npc.getMonster().getCoordinates().x - npc.getTarget().onlineOn().getCharacter().getCoordinates().x); 
		int yDistance = Math.abs(npc.getMonster().getCoordinates().y - npc.getTarget().onlineOn().getCharacter().getCoordinates().y);

		if(npc.getTarget() == null){
			changeState = AIStateManager.INACTIVE;
		}
		if( Math.hypot(xDistance, yDistance) > 1.5){
			changeState = AIStateManager.MOVE;
		}

		if(npc.getTarget().DEADPLAYER){
			npc.clearTarget();
			changeState = AIStateManager.INACTIVE;
		}
		
		if(  !npc.getMonster().isAlive()  ){
			changeState = AIStateManager.DEAD;
		}
		
		return changeState;
	}

	@Override
	public void enter(NPC npc) {
//		System.out.println("Enter() Attack_State: " + npc.getID());
	}

	@Override
	public void update(NPC npc) {
		// TODO Auto-generated method stub
		changeState = AIStateManager.ATTACK;
		if(canAct(npc)){
//			System.out.println("ATTACK!");
			AttackInteraction.attackObject(npc.getID(), npc.getTarget().getID());
			acted(npc);
		}
	}

	@Override
	public void exit(NPC npc) {
//		System.out.println("Exit() Attack_State: " + npc.getID());

	}

	public boolean canAct(NPC npc){
		long temp = System.currentTimeMillis();
		if((  temp - npc.getTime()  ) > timer){
			return true;
		}
		return false;
	}

	public void acted(NPC npc){
		long temp = System.currentTimeMillis();
		npc.setTime(temp);
	}
}
