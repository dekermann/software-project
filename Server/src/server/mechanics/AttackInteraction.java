package server.mechanics;

import java.util.Random;

import server.NPC.NPCHandler;
import server.broadcast.Broadcast;
import server.client.ClientHandler;
import server.database.items.Item;

public class AttackInteraction {
	private static ClientHandler ClientH;
	private static NPCHandler NPCH;

	public static void attackObject(int fromID, int toID){
		Random rnd = new Random();
		int Damage = 0;
		int DamageReduction = 0;
		if(fromID<0){
			byte[] Weapon = Item.getItem(ClientH.getClientbyID(fromID).onlineOn().getCharacter().getInventory(0));
			int WeaponDamage = Weapon[1];
			int Skill = 0;
			double Buffer = 0;
			switch(Weapon[0]){
			case 0:
				//One hand
				Skill = ClientH.getClientbyID(fromID).onlineOn().getCharacter().getSkill(4);
				Buffer = ((double)(rnd.nextInt(20)+90))/100;
				break;
			case 1:
				//Two hand
				Skill = ClientH.getClientbyID(fromID).onlineOn().getCharacter().getSkill(5);
				Buffer = (rnd.nextInt(50)+75)/100;
				break;
			case 2:
				//Bow
				Skill = ClientH.getClientbyID(fromID).onlineOn().getCharacter().getSkill(2);
				Buffer = (rnd.nextInt(10)+95)/100;
				break;
			case 3:
				//Thrown
				Skill = ClientH.getClientbyID(fromID).onlineOn().getCharacter().getSkill(3);
				Buffer = (rnd.nextInt(10)+95)/100;
				break;
			}
			Damage = (int) (((WeaponDamage)*(1+(Skill/10))*(Buffer))+0.5);
		}
		else{
			//From NPC
			int npcDamage;
			//Prototyp
			npcDamage = NPCH.getNPCbyID(fromID).getMonster().getAttack();
			// buffer = 
			Damage = npcDamage;
//			MonsterH.getMonsterbyID(fromID);
		}
		if(toID<0){
			int Armor = 0;
			for(int i = 0;i<9;i++){
				Armor += Item.getItem(ClientH.getClientbyID(toID).onlineOn().getCharacter().getInventory(0))[1];
			}
			int DefenseSkill = ClientH.getClientbyID(toID).onlineOn().getCharacter().getSkill(0);
			
			DamageReduction = (int) ((Math.log10(10+Armor*(1+DefenseSkill/10))*30-30)+0.5);
			
//			System.out.println("Damage: " + Damage);
//			System.out.println("DamageReduction: " + (Damage*(((double)(DamageReduction+1))/100)));
			
			int DamageDealt = (int) ((Damage - (Damage*(((double)(DamageReduction+1))/100)))+0.5);
			
			System.out.println(DamageDealt);
			if(ClientH.getClientbyID(toID).onlineOn().getCharacter().damage(DamageDealt)){
				ClientH.getClientbyID(toID).DEADPLAYER = true;
				Broadcast.userDamageBroadcast(ClientH.getClientbyID(toID), DamageDealt+9999999);
			}
			else{
				Broadcast.userDamageBroadcast(ClientH.getClientbyID(toID), DamageDealt);
			}
			

//			System.out.println("Player:" + toID +  " took " + DamageDealt + " damage, from player/npc" + fromID );
//			System.out.println("DAMAGE DEALT: " + DamageDealt);
		}
		else{
		//TO NPC
			
			DamageReduction = NPCH.getNPCbyID(toID).getMonster().getDefense();
			int DamageDealt2 = (int) ((Damage - (Damage*(((double)(DamageReduction+1))/100)))+0.5);
			NPCH.getNPCbyID(toID).damage(DamageDealt2, fromID);
//			System.out.println("NPC:" + toID +  " took " + DamageDealt2 + " damage, from player" + fromID );
//			MonsterH.getMonsterbyID(fromID);
			
			Broadcast.npcDamageBroadcast(toID, NPCH.getNPCbyID(toID).getMonster().getCoordinates(), DamageDealt2);
		}

	}
	
	public static void setClientHandler(ClientHandler CH){
		ClientH = CH;
	}
	
	public static void setNPCHandler(NPCHandler npch){
	NPCH = npch;
}
}
