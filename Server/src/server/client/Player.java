package server.client;

import input_output.files.ReadXML;
import java.awt.Point;
import java.util.ArrayList;
import server.database.account.Character;

public class Player {
	private Character Character;
	
	public Player(String Character, String AccountName){
		loadPlayer(Character, AccountName);
	}
	
	public void loadPlayer(String Char, String AccountName){
		ArrayList<String> temp = new ArrayList<String>();
		temp.add("Currenthp");
		temp.add("Currentmp");
		temp.add("Slot");
		temp.add("Defense");
		temp.add("Magic");
		temp.add("Bow");
		temp.add("Thrown");
		temp.add("OneHanded");
		temp.add("TwoHanded");
		temp.add("DefenseExp");
		temp.add("MagicExp");
		temp.add("BowExp");
		temp.add("ThrownExp");
		temp.add("OneHandedExp");
		temp.add("TwoHandedExp");
		temp.add("Level");
		temp.add("Experience");
		temp.add("UsedCapacity");
		temp.add("X-axis");
		temp.add("Y-axis");
		temp.add("Bag");
		ArrayList<String[]> LoadedCharacter = ReadXML.readConfig("Database\\World\\Account\\" + AccountName + ".xml", Char, temp);
		for(int i = 0;i<LoadedCharacter.size();i++){
			for(int j = 0;j<LoadedCharacter.get(i).length;j++){
				System.out.println(LoadedCharacter.get(i)[j]);
			}
		}
		short[] Inventory = new short[10];
		String[] temp2 = LoadedCharacter.get(0)[2].split("&");
		for(int i = 0;i<temp2.length;i++){
			Inventory[i] = (short) Integer.parseInt(temp2[i]);
		}
		int[] Skill = new int[6];
		Skill[0] = Integer.parseInt(LoadedCharacter.get(0)[3]);
		Skill[1] = Integer.parseInt(LoadedCharacter.get(0)[4]);
		Skill[2] = Integer.parseInt(LoadedCharacter.get(0)[5]);
		Skill[3] = Integer.parseInt(LoadedCharacter.get(0)[6]);
		Skill[4] = Integer.parseInt(LoadedCharacter.get(0)[7]);
		Skill[5] = Integer.parseInt(LoadedCharacter.get(0)[8]);
		int[] SkillExp = new int[6];
		SkillExp[0] = Integer.parseInt(LoadedCharacter.get(0)[9]);
		SkillExp[1] = Integer.parseInt(LoadedCharacter.get(0)[10]);
		SkillExp[2] = Integer.parseInt(LoadedCharacter.get(0)[11]);
		SkillExp[3] = Integer.parseInt(LoadedCharacter.get(0)[12]);
		SkillExp[4] = Integer.parseInt(LoadedCharacter.get(0)[13]);
		SkillExp[5] = Integer.parseInt(LoadedCharacter.get(0)[14]);
		short[] BagSpace = new short[40];
		temp2 = LoadedCharacter.get(0)[20].split("&");
		for(int i = 0;i<temp2.length;i++){
			BagSpace[i] = (short) Integer.parseInt(temp2[i]);
//			System.out.println((short) Integer.parseInt(temp2[i]));
		}
		Point Coordinates = new Point(Integer.parseInt(LoadedCharacter.get(0)[18]),Integer.parseInt(LoadedCharacter.get(0)[19]));
		Character = new Character(Char, Integer.parseInt(LoadedCharacter.get(0)[0]),
				Integer.parseInt(LoadedCharacter.get(0)[1]), Inventory, Skill, SkillExp,
				Integer.parseInt(LoadedCharacter.get(0)[15]), Integer.parseInt(LoadedCharacter.get(0)[16]),
				Integer.parseInt(LoadedCharacter.get(0)[17]), BagSpace, Coordinates);
		System.out.println("Character " + Character.getName() + " has been successfully created!");
	}
	
	public Character getCharacter(){
		return this.Character;
	}

	public static void main(String[] args){
		Player p = new Player("niggahtron", "Account1");
	}
}
