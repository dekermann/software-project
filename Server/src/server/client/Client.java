/*
 * A client is seen as a confirmed user, a user who successfully logged
 * in to a account. Not necessarily logged in on a 'Character'.
 * 
 * The Client class will contain all the useful information for how the server
 * should contact and handle the Client. Contact information as IP, Port and
 * dynamic database as Player->Character.
 */

package server.client;

import input_output.files.InitXML;
import input_output.files.WriteXML;
import input_output.network.ObjectSender;

import java.awt.Point;
import java.net.InetAddress;

import server.ActionChecks;

import server.broadcast.Broadcast;
import server.broadcast.Objects;
import server.mechanics.AttackInteraction;

public class Client {
	private int ID;
	private InetAddress IP_Address;
	private int Port;
	private boolean Admin;
	private Player Player;
	private String AccountName;
	public boolean DEADPLAYER = false;
	
	/**
	 * Client constructor without constraints
	 */
	public Client(){
		
	}
	
	/**
	 * Constructs a client with a specified IP_Address, and ID.
	 * @param ID to be added for the client
	 * @param IP_Address to be added for the client
	 */
	public Client(int ID, InetAddress IP_Address, int Port, String AccountName){
		this.ID = ID;
		this.IP_Address = IP_Address;
		this.Port = Port;
		this.AccountName = AccountName;
	}
	
	/**
	 * Method for getting the ID of selected client
	 * @return ID
	 */
	public int getID(){
		return ID;
	}
	
	/**
	 * Constructs a client with a specified IP_Address
	 * @param IP_Address to be added for the client
	 */
	public Client(InetAddress IP_Address){
		this.IP_Address = IP_Address;
	}
	
	/**
	 * Method for getting the IP Address of selected client
	 * @return IP_Address
	 */
	public InetAddress getIP(){
		return IP_Address;
	}
	
	/**
	 * Method for setting the IP Address of selected client
	 * @param IP_Address will be the new address for the client
	 */
	public void setIP(InetAddress IP_Address){
		this.IP_Address = IP_Address;
	}
	
	/**
	 * Method for getting the Port of selected client
	 * @return Port number
	 */
	public int getPort(){
		return Port;
	}
	
	/**
	 * Method for setting the IP Address of selected client
	 * @param IP_Address will be the new address for the client
	 */
	public void setPort(int Port){
		this.Port = Port;
	}
	
	/**
	 * checks if account got admin status
	 * @return true if admin false if not
	 */
	public boolean isAdmin() {
		return Admin;
	}

	/**
	 * sets admin status of account
	 * @param admin either true or false
	 */
	public void setAdmin(boolean admin) {
		Admin = admin;
	}
	
	/**
	 * returns the player of the client
	 * @return Player
	 */
	public Player onlineOn(){
		return Player;
	}
	
	private void downloadChar(String Port){
		ObjectSender.sendObject(Player.getCharacter(), IP_Address, Integer.parseInt(Port));
	}
	
	private void createChar(String Character, String World){
		InitXML.addCharacter(World, AccountName, Character);
	}
	
	private void logIn(String Character){
		Player = new Player(Character, AccountName);
		Objects.addObject(Player.getCharacter().getCoordinates(), this.ID, Player.getCharacter().getHP());
	}
	
	private void deleteChar(String Character, String World){
		WriteXML.deleteCharacter(World, AccountName, Character);
	}
	

	
	/**
	 * The user requested something which is sent here threw network and is after
	 * that handled as it if came from this internal client
	 */
	public String userRequest(String[] Request){
		switch(Request[0]) {
		case "COMMAND":
			String[] temp1 = Request[1].split("&");
			if(temp1[0].equals("0")){
				//REQUEST TO DOWNLOAD CHAR
				downloadChar(temp1[1]);
				return null;
			}
			if(temp1[0].equals("1")){
				//REQUEST TO DOWNLOAD NERBY OBJECTS
				Broadcast.userBroadcast(Player.getCharacter().getCoordinates(), this);
				return null;
			}
			if(temp1[0].equals("2")){
				//PLAYER DIED
				System.out.println("QWYTEUQWTEUTWQEYUQWEUYTWQEUYQWYEUQYTWEUQYTWEUTQWUEYTUQWEU");
				DEADPLAYER = true;
				return null;
			}
			return ("10N");
		case "CREATE CHARACTER": 
			String[] temp2 = Request[1].split("&");
			createChar(temp2[0], "World");
			return ("11N");
		case "CHOOSE CHARACTER":
			String[] temp3 = Request[1].split("&");
			logIn(temp3[0]);
			return ("12K");
		case "DELETE CHARACTER":
			String[] temp4 = Request[1].split("&");
			System.out.println("DELETING: " + temp4[0]);
			deleteChar(temp4[0], "World");
			return ("13N");
		case "MOVE":
			String[] temp5 = Request[1].split("&");
			int x = Integer.parseInt(temp5[0]);
			int y = Integer.parseInt(temp5[1]);
			if(ActionChecks.Move(Player.getCharacter(),x,y)){
				Objects.editObject(Player.getCharacter().getCoordinates(), new Point(x,y));
				Broadcast.userMoveBroadcast(new Point(x,y), this);
				Player.getCharacter().setCoordinates(new Point(x,y));
				return("14K");
			}
			return ("14N");
		case "ATTACK":
			String[] temp6 = Request[1].split("&");
			AttackInteraction.attackObject(this.ID, Integer.parseInt(temp6[0]));
			return ("15N");
		case "SPECIAL":
			Player.getCharacter().addHP(1);
			return ("16K");
		case "SELF SPECIAL":
			this.Player.getCharacter().addHP(1);
			return ("17K");
		case "USE OBJECT":
			String[] temp7 = Request[1].split("&");
			System.out.println("getting id: " + temp7[0]);
				
			return Broadcast.sendLoot(Integer.parseInt(temp7[0]), this);
		case "CHAT":
			
			return ("19N");
		}
		return ("N");
	}
}
