/*
 * The Client Handler will detect which client made requests and forward
 * all the requests to those clients. It will also contact the server if
 * the request has been confirmed by the Client class. The server will 
 * then mass update all the diffrent nodes.
 */

package server.client;

import java.net.InetAddress;
import java.util.ArrayList;
import server.Server;

public class ClientHandler {
	private ArrayList<Client> Clients = new ArrayList<Client>();
	private Server Server;
	
	/**
	 * Constructs the ClientHandler
	 * ClientHandler contains a list of all the connected clients together with methods for handling these
	 */
	public ClientHandler(Server Server){
		this.Server = Server;
	}
	
	/**
	 * Adds a client to the clients array list
	 * @param Client to be added
	 */
	public void addClient(Client Client){
		Clients.add(Client);
	}
	
	/**
	 * Adds a client to the clients array list
	 * @param Client to be added
	 */
	public void addNewClient(int ID, InetAddress IP, int Port, String AccountName){
		Clients.add(new Client(ID, IP, Port, AccountName));
	}
	
	/**
	 * Gets the client with the specified value
	 * @param ID the ID of the client
	 * @return Client
	 */
	public Client getClientbyID(int ID){
		for(int i = 0;i<Clients.size();i++){
			if(Clients.get(i).getID() == ID){
				return Clients.get(i);
			}
		}
		return null;
	}
	
	/**
	 * Removes a client from the client array list
	 * @param Client to be removed
	 */
	public void removeClient(Client Client){
		Clients.remove(Client);
	}
	
	/**
	 * A user has made a request!
	 * This method finds out if the user is authed as a client and forwards the request to the client
	 */
	public void userRequest(String[] Request, InetAddress IP){
		String temp;
//		System.out.println(Request[0]);
		for(int i = 0;i<Clients.size();i++){
			if(IP.equals(Clients.get(i).getIP())){
				temp = Clients.get(i).userRequest(Request);
				if(temp !=null){
					Server.ConfirmUserRequest(temp, Clients.get(i).getIP(), Clients.get(i).getPort());
				}
				break;
			}
		}	
	}
}