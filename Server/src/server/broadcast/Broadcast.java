/*
 * The broadcast class will broadcast all events from monsters and players to the surroundings.
 */

package server.broadcast;

import input_output.network.Sender;

import java.awt.Point;

import server.NPC.NPC;
import server.NPC.NPCHandler;
import server.client.Client;
import server.client.ClientHandler;

public class Broadcast {

	private static final int BROADCASTRANGE = 30;
	private static Sender S;
	private static ClientHandler CH;
	private static NPCHandler NPCH;

	/**
	 * Will broadcast all players close by to the client and broadcast the client itself to all others
	 * @param P Where the client is moving
	 * @param Client The client moving
	 */
	public static void userBroadcast(Point P, Client Client){
		for(int y = 0;y<BROADCASTRANGE;y++){
			for(int x = 0;x<BROADCASTRANGE;x++){
				Integer[] temp = Objects.getObject(new Point(P.x+x-BROADCASTRANGE/2,P.y+y-BROADCASTRANGE/2));
				if(temp != null){
					if(temp[0] < 0 && !temp[0].equals(Client.getID())){
						S.sendMessage("2" + Client.getID() + "&" + Client.onlineOn().getCharacter().getHP() + "&" + P.x + "&" + P.y + "&", CH.getClientbyID(temp[0]).getIP(),CH.getClientbyID(temp[0]).getPort());
					}
					if(temp[0] != Client.getID()){
						S.sendMessage("2" + temp[0] + "&" + temp[1] + "&" + (P.x+x-BROADCASTRANGE/2) + "&" + (P.y+y-BROADCASTRANGE/2) + "&", Client.getIP(), Client.getPort());
					}
					if(temp[0] > 999 ){
						//found npc
						//						System.out.println("Found npc:"  + temp[0]);
						NPCH.npcTrigger(temp[0], Client);
						S.sendMessage("2" + NPCH.getNPCbyID(temp[0]).getID() + "&" + temp[1] + "&" + (P.x+x-BROADCASTRANGE/2) + "&" + (P.y+y-(BROADCASTRANGE/2)) + "&", Client.getIP(), Client.getPort());
					}
				}
			}
		}
	}
	
	public static void userMoveBroadcast(Point P, Client Client){
		for(int y = 0;y<BROADCASTRANGE;y = BROADCASTRANGE){
			for(int x = 0;x<BROADCASTRANGE;x = BROADCASTRANGE){
				Integer[] temp = Objects.getObject(new Point(P.x+x-BROADCASTRANGE/2,P.y+y-BROADCASTRANGE/2));
				if(temp != null){
					if(temp[0] < 0 && !temp[0].equals(Client.getID())){
						S.sendMessage("2" + Client.getID() + "&" + Client.onlineOn().getCharacter().getHP() + "&" + P.x + "&" + P.y + "&", CH.getClientbyID(temp[0]).getIP(),CH.getClientbyID(temp[0]).getPort());
					}
					if(temp[0] != Client.getID()){
						S.sendMessage("2" + temp[0] + "&" + temp[1] + "&" + (P.x+x-BROADCASTRANGE/2) + "&" + (P.y+y-BROADCASTRANGE/2) + "&", Client.getIP(), Client.getPort());
					}
					if(temp[0] > 999 ){
						//found npc
						//						System.out.println("Found npc:"  + temp[0]);
						NPCH.npcTrigger(temp[0], Client);
						S.sendMessage("2" + NPCH.getNPCbyID(temp[0]).getID() + "&" + temp[1] + "&" + (P.x+x-BROADCASTRANGE/2) + "&" + (P.y+y-(BROADCASTRANGE/2)) + "&", Client.getIP(), Client.getPort());
					}
				}
			}
		}
	}
	
	public static void userDamageBroadcast(Client Client, int DamageTaken){
		for(int y = 0;y<BROADCASTRANGE;y++){
			for(int x = 0;x<BROADCASTRANGE;x++){
				Integer[] temp = Objects.getObject(new Point(Client.onlineOn().getCharacter().getCoordinates().x+x-BROADCASTRANGE/2,Client.onlineOn().getCharacter().getCoordinates().y+y-BROADCASTRANGE/2));
				if(temp != null){
					if(temp[0] < 0 && !temp[0].equals(Client.getID())){
						S.sendMessage("3" + Client.getID() + "&" + DamageTaken + "&", CH.getClientbyID(temp[0]).getIP(),CH.getClientbyID(temp[0]).getPort());
					}
					if(temp[0] == Client.getID()){
						S.sendMessage("30" + DamageTaken + "&", Client.getIP(), Client.getPort());
					}
				}
			}
		}
	}
	
	public static void npcDamageBroadcast(int ID, Point NPCPoint, int DamageTaken){
		for(int y = 0;y<BROADCASTRANGE;y++){
			for(int x = 0;x<BROADCASTRANGE;x++){
				Integer[] temp = Objects.getObject(new Point(NPCPoint.x+x-BROADCASTRANGE/2,NPCPoint.y+y-BROADCASTRANGE/2));
				if(temp != null){
					if(temp[0] < 0){
						S.sendMessage("3" + ID + "&" + DamageTaken + "&", CH.getClientbyID(temp[0]).getIP(),CH.getClientbyID(temp[0]).getPort());
					}
				}
			}
		}
	}

	/**
	 * Needs to know the sender for broadcasts
	 * @param Sender the one and only
	 */
	public static void setSender(Sender Sender){
		S = Sender;
	}

	/**
	 * the broadcast of the npc. It should be done whenever an npc moves
	 * @param P the point the npc broadcasts that it moves too
	 * @param npc the npc that makes the broadcast
	 */
	public static void npcBroadcast(Point P, Point End, NPC npc){
		for(int y = 0;y<BROADCASTRANGE;y++){
			for(int x = 0;x<BROADCASTRANGE;x++){
				Integer[] temp = Objects.getObject(new Point(P.x+x-BROADCASTRANGE/2,P.y+y-BROADCASTRANGE/2));
				if(temp != null){
					if(temp[0] < 0 ){
						S.sendMessage("2" + npc.getID() + "&" + npc.getMonster().getHP() + "&" + End.x + "&" + End.y + "&", CH.getClientbyID(temp[0]).getIP(),CH.getClientbyID(temp[0]).getPort());
						//npc.aggro(npc, 1)
					}
				}
			}
		}
	}
	
	/**
	 * Needs to know the ClientHandler so it can search for the player with
	 * a specific ID
	 * @param ClientHandler
	 */
	public static void setClientHandler(ClientHandler ClientHandler){
		CH = ClientHandler;
	}
	
	/**
	 * sets the npc handler for the Broadcast
	 * @param npch the npc handler to be used
	 */
	public static void setNPCHandler(NPCHandler npch){
		NPCH = npch;
	}
	
	public static String sendLoot(int id, Client client  ){
		String mess = NPCH.getNPCbyID(  id  ).getMonster().Loot();
		System.out.println(mess);
		S.sendMessage(mess,client.getIP(),client.getPort());
		if(mess == "8&0&"){
			return "18N";
		}
		return "18K";
	}
}
