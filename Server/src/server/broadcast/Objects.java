package server.broadcast;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

public class Objects {
	static Map<Point, Integer[]> ObjectList = new HashMap<Point, Integer[]>();
	static Map<Integer, Point> ObjectListbyID = new HashMap<Integer, Point>();

	/**
	 * Checks for an object on a specific coordinate
	 * @param P the coordinate to check on
	 * @return the object ID
	 */
	public static Integer[] getObject(Point P){
		return ObjectList.get(P);
	}

	public static Integer[] getObjectbyID(int ID){
		return ObjectList.get(ObjectListbyID.get(ID));
	}

	/**
	 * Adds an object to the dynamic database
	 * @param P Where the object is
	 * @param ID What the object is
	 * @param CurrentHP the current HP of the added object
	 */
	public static void addObject(Point P, int ID, int CurrentHP){
		ObjectList.put(P, new Integer[] {ID,CurrentHP});
		ObjectListbyID.put(ID, P);
	}

	public static boolean containKey(Point p){
		return ObjectList.containsKey(p);
	}

	/**
	 * Removes an object from the dynamic database
	 * @param P Where the object is
	 */
	public static void removeObject(Point P){
		ObjectListbyID.remove(ObjectList.get(P)[0]);
		ObjectList.remove(P);
	}

	/**
	 * Edits an objects key
	 * @param P1 old point
	 * @param P2 new point
	 */
	public static void editObject(Point P1, Point P2){
		Integer[] temp = ObjectList.get(P1);
		removeObject(P1);
		addObject(P2, temp[0], temp[1]);
	}
}