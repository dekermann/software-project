package server.database.items;

import input_output.files.ReadXML;

import java.util.ArrayList;

public class ItemLoader {

	public static void LoadItems(String Path){
		ArrayList<String> loadElements = new ArrayList<String>();
		loadElements.add("OneHanded");
		loadElements.add("TwoHanded");
		loadElements.add("Thrown");
		loadElements.add("Bow");
		loadElements.add("Helmets");
		loadElements.add("Chests");
		loadElements.add("Legs");
		loadElements.add("Boots");
		loadElements.add("Gloves");
		loadElements.add("Rings");
		loadElements.add("Necklaces");
		loadElements.add("Bracelets");
		loadElements.add("OffHands");
		ArrayList<String[]> LoadedItems = ReadXML.readConfig(Path + ".xml", "Items", loadElements);
		
		
//		for(int i = 0;i<LoadedItems.size();i++){
//			for(int j = 0;j<LoadedItems.get(i).length;j++){
//				System.out.println(LoadedItems.get(i)[j]);
//			}
//		}

		String[] temp = LoadedItems.get(0)[0].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {0,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<3000;i++){
			Item.addItem(new byte[] {0,0});
		}
		
		temp = LoadedItems.get(0)[1].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {1,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<3000;i++){
			Item.addItem(new byte[] {1,0});
		}
		
		temp = LoadedItems.get(0)[2].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {2,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<2000;i++){
			Item.addItem(new byte[] {2,0});
		}
		
		temp = LoadedItems.get(0)[3].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {3,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<2000;i++){
			Item.addItem(new byte[] {3,0});
		}

		temp = LoadedItems.get(0)[4].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {4,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<1000;i++){
			Item.addItem(new byte[] {4,0});
		}

		temp = LoadedItems.get(0)[5].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {5,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<1000;i++){
			Item.addItem(new byte[] {5,0});
		}

		temp = LoadedItems.get(0)[6].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {6,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<1000;i++){
			Item.addItem(new byte[] {6,0});
		}

		temp = LoadedItems.get(0)[7].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {7,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<1000;i++){
			Item.addItem(new byte[] {7,0});
		}

		temp = LoadedItems.get(0)[8].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {8,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<1000;i++){
			Item.addItem(new byte[] {8,0});
		}

		temp = LoadedItems.get(0)[9].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {9,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<1000;i++){
			Item.addItem(new byte[] {9,0});
		}

		temp = LoadedItems.get(0)[10].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {10,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<1000;i++){
			Item.addItem(new byte[] {10,0});
		}

		temp = LoadedItems.get(0)[11].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {11,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<1000;i++){
			Item.addItem(new byte[] {11,0});
		}

		temp = LoadedItems.get(0)[12].split("&");
		for(int i = 0;i<temp.length;i++){
			Item.addItem(new byte[] {12,(byte) Integer.parseInt(temp[i])});
		}
		for(int i = temp.length+1;i<2000;i++){
			Item.addItem(new byte[] {12,0});
		}
	}
}
