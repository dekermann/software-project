package server.database.items;

import java.util.ArrayList;

public class Item {
	private static ArrayList<byte[]> Items = new ArrayList<byte[]>();
	
	public static byte[] getItem(int Item){
		return Items.get(Item);
	}
	
	public static void addItem(byte[] Info){
		Items.add(Info);
	}
}
