/*
 * Account Handler will receive connection requests and check with the database
 * if the connection is accepted a new client will be created for the user.
 */

package server.database.account;

import input_output.files.ReadXML;
import java.net.InetAddress;
import java.util.ArrayList;
import server.Server;

public class AccountHandler {
	private Server Server;
	ArrayList<String[]> TempAccounts = new ArrayList<String[]>();
	
	public AccountHandler(Server Server){
		this.Server = Server;
	}
	
	public void loginRequest(String AccountName, String AccountPassword, String ClientPort, InetAddress IP){
		String Characters = checkDatabase(AccountName, AccountPassword);
		if(Characters !=null){
			//Account exists and the password was correct return the AccountData to the client.
			Server.addClient(IP, Characters, AccountName, Integer.parseInt(ClientPort));
		}
		else{
			//Something went wrong logging in or the accounts doesn't exist.
		}
	}
	
	private String checkDatabase(String AccountName, String AccountPassword){
		ArrayList<String> temp = new ArrayList<String>();
		temp.add("Password");
		temp.add("Character");
		TempAccounts = ReadXML.readConfig("Database\\World\\Account\\Account List.xml", AccountName, temp);
		if(TempAccounts.get(0)[0].equals(AccountPassword)){
			return TempAccounts.get(0)[1];
		}
		return null;
	}
}
