package server.NPC.MonsterTypes;


import java.awt.Point;
import server.NPC.*;

public class Zombie extends Monster {
	public Zombie(Point coordinates,boolean walk){
		super(1005,50,00,coordinates,walk);
		this.attack = 10;
		this.defense = 45;
		this.speed = 0.50;
		this.resist = 0;
		this.experience = 50;
		this.iq = 0;
		LootTable();

	}

	public void LootTable(){
		loot.increaseChance(1,1);
		loot.increaseChance(2,1);
		loot.increaseChance(3,1);
	}

}