package server.NPC.MonsterTypes;


import java.awt.Point;
import server.NPC.*;

public class Flesh extends Monster {
 public Flesh(Point coordinates,boolean walk){
  super(1000,120,10,coordinates,walk);
  this.attack = 20;
  this.defense = 50;
  this.speed = 1.5;
  this.resist = 0;
  this.experience = 200;
  this.iq = 1;
  LootTable();
  
 }
 
 public void LootTable(){
		//loot.increaseChance(1,0);
		loot.increaseChance(2,2);
		loot.increaseChance(3,1);
 }
 
}