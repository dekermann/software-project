package server.NPC.MonsterTypes;

import java.awt.Point;

import server.NPC.*;

public class Crab extends Monster{
	public Crab(Point coordinates,boolean walk){
		super(1005,10,00,coordinates,walk);
		this.attack = 3;
		this.defense = 5;
		this.speed = 1;
		this.resist = 0;
		this.experience = 10;
		this.iq = 0;
		LootTable();

	}

	public void LootTable(){
		loot.increaseChance(1,3);
//		loot.increaseChance(1,2);
//		loot.increaseChance(1,3);
	}
}