package server.NPC.MonsterTypes;

import java.awt.Point;

import server.NPC.Monster;

public class Elemental extends Monster{
 public Elemental(Point coordinates,boolean walk){
  super(1005,20,00,coordinates,walk);
  this.attack = 6;
  this.defense = 20;
  this.speed = 2.5;
  this.resist = 0;
  this.experience = 25;
  this.iq = 0;
  LootTable();
  
 }
 
 public void LootTable(){
//		loot.increaseChance(1,0);
//		loot.increaseChance(2,0);
		loot.increaseChance(3,3);
 }
}