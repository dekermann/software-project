package server.NPC;


import java.util.ArrayList;

public class Table {

	protected ArrayList<Integer> Loot = new ArrayList<Integer>();

	/**
	 * returns the loot at index in
	 * @param in the index that is used to return the loot
	 * @return the id of the loot at that index in the list
	 */
	protected Integer getLoot(int in){
		return Loot.get(in);

	}

	public int Size(){
		return Loot.size();
	}

	/**
	 * returns the head of the loot list and returns it, or 0 if there's no loot
	 * @return the id of the item dropped
	 */
	public int dropLoot(){
		int temp = 0;
		if(Loot.size()>0){
			temp = Loot.get(0);
			Loot.remove(0);
		}
		return temp;
	}

	/**
	 * the size of the current Loot list
	 * @return the size of the list in items
	 */
	public int size(){
		return Loot.size();
	}

	/**
	 * empties the loot Table
	 */
	public void emptyLootTable(){
		Loot.clear();
	}

	/**
	 * sets up the loot table
	 * @param id the ide to add to the table
	 * @param increase the number of times the item should be added
	 */
	public void increaseChance(int id, int increase){
		for(  int i =0;  i<increase;  i++){
			Loot.add(id);
		}
	}

	/**
	 * increases the chance to drop a specific item id.
	 * @param id the id to increase the chance off
	 * @param increase the number of extra chances to drop this item
	 */
	public void increaseChance2(int id, int increase){
		if(Loot.contains(id)){
			for(  int i =0;  i<increase;  i++){
				Loot.add(id);
			}
		}
	}

	/**
	 * decreases the chance of a mob dropping a specific item id if the id is already on the
	 * item list. if there's no such item, this function doesnt do anything other than
	 * take up time
	 * @param id the item to decrease the chance of dropping
	 * @param decrease the number of chances to drop the item.
	 */
	public void decreaseChance(int id, int decrease){
		if(Loot.size() != 0){
			for(  int i=0;  i<decrease;  i++){
				if(Loot.contains(id)){
					Loot.remove(id);
				}
			}
		}
	}
}