package server.NPC;

import input_output.network.ObjectSender;
import java.awt.Point;
import server.Server;
import server.broadcast.Broadcast;
import server.broadcast.Objects;
import server.client.Client;
import input_output.network.ObjectSender;
import java.awt.Point;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import server.AI.*;
import server.AI.states.AIState;
import server.AI.states.Inactive_State;


public class NPC {
	/**
	 * 
	 */
	private NPCHandler NPCH;
	private int ID;
	private Monster Monster;


	private String aiState;
	private LinkedList<Point> path = new LinkedList<Point>();
	private Client target = null;
	private long timeStamp;
	private AIState state;
	private int actCounter = 0;
	private int moveCounter = 0;
	Map<Integer, Integer> Attackers = new HashMap<Integer, Integer>();



	//terrainIgnore

	/**
	 * Client constructor without constraints
	 */
	public NPC(){

	}

	/**
	 * the constructor of the NPC object. 
	 * @param ID
	 * @param flying
	 * @param MonsterType
	 * @param server
	 * @param ObjList
	 */

	public NPC(int ID, Monster monster, NPCHandler NPCH){
		this.NPCH = NPCH;
		this.ID = ID;
		this.Monster = monster;
		this.state = new Inactive_State();
		this.path = new LinkedList<Point>();
	}

	// public void addAggro(NPC npc, int aggro){
	//  for(int i = 0;i<5;i++){
	//   
	//  }
	// }

	/**
	 * this function returns the actCounter for the npc soo you can check its value
	 * @return the number of 'ai ticks' the npc has waited
	 */
	public int getActCounter(){
		return actCounter;
	}

	/**
	 * resets the act counter to zero
	 */
	public void resetActCounter(){
		actCounter = 0;
	}

	public void addActCounter(){
		actCounter++;
	}


	/**
	 * sets the time when the NPC acts.
	 * @param time the time (in milliseconds since january first 1970) when the NPC acts
	 */
	public void setTime(long time){
		timeStamp = time;
	}

	/**
	 * checks the time when the NPC last acted
	 * @return the time (in milliseconds since january first 1970) when the NPC last acted.
	 */
	public long getTime(){
		return timeStamp;
	}

	/**
	 * this function sets the target of the NPC without any checks.
	 * @param target the Client to set the target too
	 */
	private void setTarget(Client target){
		this.target = target;
	}

	/**
	 * this function is used to try to set the target of an npc when the npc is discovered by a player
	 * if there's already a target, this function does nothing.
	 * @param target the client to set as target
	 */
	public void triggerTarget(Client target){
		//   if(target == null){
		setTarget(target);
		//   }
		System.out.println("NPC has target");
	}

	public void addMoveCounter(){
		moveCounter++;
	}

	public void resetMoveCounter(){
		moveCounter = 0;
	}

	public int getMoveCounter(){
		return moveCounter;
	}

	/**
	 * gives you the current target of the npc
	 * @return the client/player that is the target of the npc
	 */
	public Client getTarget(){
		return target;
	}

	public boolean hasTarget(){
		if(target == null){
			return false;
		}
		return true;
	}

	/**
	 * stes the path of the npc to the specified input. if the input is null
	 * @param tmpPath
	 */
	public void setPath(LinkedList<Point> tmpPath){
		path.clear();
		if(tmpPath != null){
			for(int i = 0;i<tmpPath.size();i++){
				int x = tmpPath.get(i).x;
				int y = tmpPath.get(i).y;
				path.add(new Point(x,y));
			}
			tmpPath.clear();
		}
		else{
			/*
			 * TODO: handling of null paths
			 */
			path = null;
			System.out.println("Krasch! Null path");
		}
	}

	/**
	 * returns the path the npc has set to wander along
	 * @return the path the npc walks along
	 */
	public LinkedList<Point> getPath(){
		return path;
	}

	/**
	 * clears the path of the NPC
	 */
	public void clearPath(){
		path.clear();
	}

	/**
	 * returns the path head (i.e. the next step) the npc is supposed to take
	 * @return the coordinates to the next step the npc is supposed to take
	 */
	public Point getPathHead(){
		return path.getFirst();
	}

	/**
	 * removes the head of the path list, i.e. clears the next step in the path
	 */
	public void removeHead(){
		path.removeFirst();
	}

	/**
	 * Will get the ID of the NPC
	 * @return ID
	 */
	public int getID(){
		return ID;
	}

	/**
	 * returns the state of the NPC
	 * @return the state of the NPC
	 */
	public AIState getState(){
		return state;
	}

	/**
	 * sets the current state of the NPC
	 * @param state the state to set the npc too
	 */
	public void setState(AIState state){
		this.state = state;
	}

	/**
	 * a get function for the NPCs monster
	 * @return the monster associated with the NPC object
	 */

	public Monster getMonster(){
		return Monster;
	}

	/**
	 * Will damage the npc and add the attacker to the Attackers list. Which will be used in xp calc.
	 * @param damage
	 * @param AttackerID
	 */
	public void damage(int damage, int AttackerID){
		int hp = Monster.getHP();
		if(Monster.isAlive()){
			int temp = hp - damage;
			if(temp < 0){
				damage += temp;
			}
			hp -= damage;
			Monster.setHP(hp);
			
			if(Attackers.containsKey(AttackerID)){
				int tempVal = Attackers.get(AttackerID);
				Attackers.remove(AttackerID);
				Attackers.put(AttackerID, (tempVal+damage));
			}
			else{
				Attackers.put(AttackerID, damage);
			}
			
			if(hp<1){
				//dead
//				System.out.println("I am dead, npc");
				experienceBroadcast();
			}
		}
	}

	/**
	 * Sends xp to those that helped to kill the npc.
	 */
	public void experienceBroadcast(){

		// Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		int Key,Value;
		int Xp;
		for (Map.Entry<Integer, Integer> entry : Attackers.entrySet()) {
			Key =  entry.getKey();
			Value = entry.getValue();
			Xp = (int)((Value/Monster.getMaxHP())*Monster.getExp());
			NPCH.xpSender(Key, Xp);
//			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}
		Attackers.clear();

	}
	/**
	 * clears the target of the npc
	 */
	public void clearTarget() {
		// TODO Auto-generated method stub
		target = null;
	}
}