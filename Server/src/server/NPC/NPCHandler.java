package server.NPC;


import input_output.network.Sender;

import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedList;

import server.Server;
import server.broadcast.Objects;
import server.client.Client;
import server.client.ClientHandler;
import server.AI.*;
import server.AI.states.AIStateManager;
import server.NPC.MonsterTypes.*;

public class NPCHandler {

	private ArrayList<NPC> NPClist = new ArrayList<NPC>();
	private Server Server;
	private AIStateManager AISM;
	private Sender S;
	private ClientHandler CH;


	private int zombieID = 1000;
	private int elementalID = 1200;
	private int fleshID = 1400; 
	private int crabID = 1600;
	public enum MonsterTypes {
		ZOMBIE, CRAB , FLESH , ELEMENTAL

	}

	/**
	 * debug kod, ta bort referensen till denna innan final release
	 */
	public void setUpNPC(){
		addNewNPC(MonsterTypes.ZOMBIE,(new Point(40,40)));

		// addNewNPC(new Zombie(new Point(40,41),false), 3);
		addNewNPC(MonsterTypes.ZOMBIE,(new Point(40,80)));
		//  addNewNPC(new Zombie(new Point(41,61),false), 5);
		//  addNewNPC(new Zombie(new Point(42,62),false), 5);
		//  addNewNPC(new Zombie(new Point(43,63),false), 5);

	}

	/**
	 * Constructs the NPCHandler
	 * NPCHandler will have an ArrayList containing all NonPlayableCharacters
	 * It will also start the AistateManger which will be updating active npcs
	 */
	public NPCHandler(Server Server){
		this.Server = Server;

		AISM = new AIStateManager();
		AISM.start();
		System.out.println("AI initiated...");
	}

	/**
	 * When a player finds an npc during a probe it will be triggerd, setting the client as a target. Pushing the npc
	 * into the activeList
	 * @param npcID the id of the npc
	 * @param Client the client which found the npc and will be set as a target
	 */
	public void npcTrigger(int npcID , Client Client ){
		//set npc target
		if(!getNPCbyID(npcID).hasTarget()){
			getNPCbyID(npcID).triggerTarget(Client);
		}
		if(  !(  AISM.containsID(  npcID  )  )  ){
			AISM.addActiveNPC(getNPCbyID(npcID));
			System.out.println("NPC triggerd");
		}
	}


	/**
	 * this function returns the length of the npcList  
	 * @return the length of the npc list
	 */
	public int npcListLength(){
		return NPClist.size();
	}

	/**
	 * creates a new npc and adds to the npc list
	 * @param ID a filter for the object list
	 * @param monster the monster type you wish to add
	 * @param MonsterType the id of the monstertype that you wish to add
	 */
	public void addNewNPC(MonsterTypes MT,Point spawnCoord){
		NPC npc = null;
		switch (MT) {
		case ZOMBIE: 
			if(!(zombieID == (zombieID + 200 )) ){
				NPC npcZombie = new NPC(zombieID, new Zombie(spawnCoord, false), this);
				zombieID++;
				npc = npcZombie;
			}
			else{
				System.out.println("Zombie npc Cap reached!");
			}
			break;

		case CRAB: 
			if(!(crabID == (crabID + 200 )) ){
				NPC npcCrab = new NPC(crabID, new Crab(spawnCoord, false), this);
				crabID++;
				npc = npcCrab;
			}
			else{
				System.out.println("Crab npc Cap reached!");
			}
			break;

		case FLESH:
			if(!(fleshID == (fleshID + 200 )) ){
				NPC npcFlesh = new NPC(fleshID, new Flesh(spawnCoord, false), this);
				fleshID++;
				npc = npcFlesh;
			}
			else{
				System.out.println("Flesh npcCap reached!");
			}
			break;

		case ELEMENTAL:
			if(!(elementalID == (elementalID + 200 )) ){
				NPC npcElemental = new NPC(elementalID, new Elemental(spawnCoord, false), this);
				elementalID++;
				npc = npcElemental;

			}
			else{
				System.out.println("Elemental npcCap reached!");
			}
			break;
		}

		NPClist.add(npc);
		Objects.addObject(npc.getMonster().getCoordinates(),npc.getID(),npc.getMonster().getHP());
	}

	/**
	 * adds a new npc to the npc list
	 * @param npc the npc to add to the list
	 */
	public void addNPC(NPC npc){
		System.out.println("addNPC");
		NPClist.add(npc);
		//Objects.addObject(npc.getMonster().getCoordinates(), npc.getID(), npc.getMonster().getHP());
		System.out.println("addToObjectList");
	}

	/**
	 * Removes a NPC from the NPCList
	 * @param NPC to be removed from container
	 */
	public void removeNPC(NPC npc){
		NPClist.remove(npc);
		Objects.removeObject(npc.getMonster().getCoordinates());
	}

	/**
	 * returns the the npc that is associated with the unique id parameter
	 * @param ID the id of the npc you wish to return
	 * @return the npc of that id
	 */
	public NPC getNPCbyID(int ID){
		for (int i = 0; i < NPClist.size(); i++) {
			if(NPClist.get(i).getID() == ID){
				return NPClist.get(i);
			}
		}
		return null;
	}
	public void xpSender(int toID, int xp ){
		String Msg = "40"+xp+"&";
		S.sendMessage(Msg, CH.getClientbyID(toID).getIP(), CH.getClientbyID(toID).getPort());
		CH.getClientbyID(toID).onlineOn().getCharacter().addExperience(xp);
	}

	public void setNPCHsender(Sender s){
		this.S = s;
	}

	public void setNPCHch(ClientHandler ch){
		this.CH = ch;
	}

}