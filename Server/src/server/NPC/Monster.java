package server.NPC;


import java.awt.Point;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/*
 * A Monster works as a dynamic database for the server and is implemented on top of the NPC
 */

public class Monster extends NPC{

	protected int hp; 
	protected int maxhp;
	protected int mp; 
	protected int maxmp;

	protected int attack;
	protected int defense;
	protected int resist;

	protected int experience;

	protected double speed;

	protected Monster leader;
	protected int iq;
	protected Table loot = new Table();

	protected Point coordinates;
	protected Point spawn;

	//if flying or not
	protected boolean terrainIgnore;





	public Monster(  int id, int maxhp, int maxmp, Point coordinates, boolean walk  ){

		this.hp = maxhp;
		this.maxhp = maxhp;
		this.maxmp = maxmp;
		this.mp = maxmp;
		this.coordinates = coordinates;
		this.spawn = coordinates;
		this.terrainIgnore = walk;
		LootTable();
	}

	public void LootTable() {

	}

	public Monster(int id, int maxhp, int maxmp, int attack, int defense, int resist, int experience, int speed, int iq, Table loot, Point coordinates){
		this.hp=maxhp;  this.maxhp=maxhp;  this.mp = maxmp;  this.maxmp = maxmp;  this.attack = attack; this.resist = resist; this.experience = experience;
		this.speed = speed;  this.iq = iq;  this.loot = loot;  this.coordinates = coordinates;
		LootTable();
	}

	public Point getSpawn(){
		return spawn;
	}

	/**
	 * resets the monster to its starting values
	 */
	public void reset(){
		mp = maxmp;
		hp = maxhp;
		giveLoot();
	}

	public void giveLoot(){
		loot.emptyLootTable();
		LootTable();
	}

	public String Loot(){
		String message = "8&";
		if(isAlive()){
			message += "18N";
		}
		else{
			if(loot.Size()<1){
				message += "0&";
			}
			else{
				for(int i = 0;i<loot.Size();i++){
					message += loot.dropLoot() + "&";
				}
			}
		}
		loot.emptyLootTable();
		return message;

	}

	public boolean getTerrainIgnore(){
		return terrainIgnore;
	}

	/***
	 * sets the current walking mode of the mob in case of a Snare or Fly spell
	 * @param walk the method to walk. false = ground, true = flyer, 5 = moves like jagger
	 */
	public void setTerrainIgnore(boolean walk){
		this.terrainIgnore = walk;
	}


	/**
	 * only for unique monsters which has a name., to be implemented
	 * @param Name the name of the unique Monster
	 */

	public int getAttack(){
		return attack;
	}

	public int getHP(){
		return hp;
	}
	public void setHP(int hp){
		this.hp = hp;
	}

	public void changeResist(int change){
		resist += change;
	}

	public void changeDefense(int change){
		defense += change;
	}

	public void changeAttack(int change){
		attack += change;
	}

	/**
	 * totaly un-implemented
	 * @return the resist value of the monster
	 */
	public int getResist(){
		return resist;
	}

	/**
	 * to be implemented
	 * @return the iq level of the monster
	 */
	public int getIq(){
		return iq;
	}

	public int getDefense(){
		return defense;
	}

	/**
	 * lowers the hp of the monster
	 * @param change the number of hit points dealt as damage
	 * @return false if the creature is dead, true if it's alive
	 */
	// public void damage(int damage, int AttackerID){
	//  
	//
	// }

	/**
	 * heals the monster a number of health points equal to the in value
	 * @param heal the number of health points healed
	 */
	public void heal(int heal){
		if(hp+heal > maxhp){
			hp = maxhp;
		}
		else{
			hp += heal;
		}
	}

	/**
	 * checks if the monster is alive or not
	 * @return true if the monster is alive, false if it isn't
	 */
	public boolean isAlive(){
		if(hp < 1){
			return false;
		}
		return true;
	}

	public void manaHeal(int heal){
		if(mp + heal > maxmp){
			mp = maxmp;
		}
		else{
			mp += heal;
		}
	}

	/**
	 * attempts to drain mana. if there's not enough mana in the pool, no mana is drained
	 * @param drain the amount of mana drained from the monster
	 * @return false if no mana was drained because there's to little in the pool, true if it drained the mana
	 */
	public boolean useMana(int drain){
		if(mp - drain < 1){
			return false;
		}
		mp = mp - drain;
		return true;
	}

	public double getSpeed(){
		return speed;
	}

	public void changeSpeed(double change){
		speed += change;
	}

	/**
	 * sets the new leader of the monster for purpose of the ai
	 * @param newLeader the new leader of the monster (can be a player?)
	 */
	public void setLeader(Monster newLeader){
		leader = newLeader;
	}

	/**
	 * returns the current leader of the Monster for the purpose of the ai
	 * @return the current leader of the Monster
	 */
	public Monster getLeader(){
		return leader;
	}

	/**
	 * this function picks an item from the list of the monsters current loot table.
	 * @param in the value which determines which item is dropped.
	 * @return the id of the item dropped
	 */
	public int dropLoot(int in){
		return loot.getLoot(in);
	}

	public void setTable(Table loot){
		this.loot = loot;
	}

	/**
	 * Gets the Coordinates of the character
	 * @return Point(x,y)
	 */
	public Point getCoordinates(){
		return coordinates;
	}

	/**
	 * Sets the Coordinates of the character
	 * @param Point(x,y)
	 */
	public void setCoordinates(Point Point){
		coordinates = Point;
	}

	/**
	 * returns the amount of exp one gets for slaying said monster
	 * @return
	 */
	public int getExp(){
		return experience;
	}

	public int getMaxHP() {
		return maxhp;
	}






}