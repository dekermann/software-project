/*
 * Action checks is a class for static methods checking validity of actions. It will take a user request 
 * compare it to the database and accept or deny the request.
 */

package server;

import server.database.account.Character;
import java.lang.Math;

public class ActionChecks {
	
	/**
	 * A movement check for if the desired movement is okey or not.
	 * @param Ch to move contains old coordinates
	 * @param requestX the new requested coordinates x-axis
	 * @param requestY the new requested coordinates y-axis
	 * @return true or false depending on if the movement was okey
	 */
	public static boolean Move(Character Ch, int requestX, int requestY) {

		if((Math.abs(Ch.getCoordinates().getX()-(requestX)) == 0 || 
				Math.abs(Ch.getCoordinates().getX()-(requestX)) == 1)
				&&
				(Math.abs(Ch.getCoordinates().getY()-(requestY)) == 0 || 
				Math.abs(Ch.getCoordinates().getY()-(requestY)) == 1)) {
			return true;
		}
		return false;
	}
}
