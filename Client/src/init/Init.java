/*
 * Client version 0.1
 * 
 * To use the code or parts of it please contact: 
 * David Ekermann on daeke@kth.se or dekermann on bitbucket.
 */

package init;

import client.Client;

public class Init {

	public static void main(String[] args) {
		Client Client = new Client();
		Client.start();
	}
}
