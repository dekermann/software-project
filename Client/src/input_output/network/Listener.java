package input_output.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import state.State_Manager;

public class Listener extends Thread{

	private DatagramSocket DS;
	private DatagramPacket DP_buffer;
	private State_Manager SM;

	/**
	 * Attempts to create a thread Listening on a port ranged from PortStart - PortStop.
	 * @param PortStart attempts to start with this port if it fails increases the port by +1
	 * @param PortStop the last port to be tried
	 * @param 
	 */
	public Listener(int PortStart, int PortStop, State_Manager SM){
		this.SM = SM;
		for(int i = PortStart;i<PortStop;i++){
			try {
				DS = new DatagramSocket(i);
				System.out.println("Successfully created a Socket on port: " + i);
				break;
			}
			catch (SocketException e) {
				e.printStackTrace();
				System.err.println("Failed to create server on port: " + i);
			}
		}
		
		byte[] buffer = new byte[100];
		DP_buffer = new DatagramPacket(buffer, buffer.length);
	}

	/**
	 * Starts the listening thread. This thread will listen to incoming data
	 * on the chosen socket and relay it to the *****.
	 */
	public void run(){
		System.out.println("Listener has been started on port: " + DS.getLocalPort());
		while(true){
			try {
				DS.receive(DP_buffer);
			} catch (IOException e) {
				e.printStackTrace();
			}
			SM.receive(DP_buffer.getData());
		}
	}
}
