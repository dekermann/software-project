/*
 * This class will be waiting in it's own thread to receive a object
 * from the Server.
 * 
 * When an object is received it is (type casted) and sent to the server.
 */


package input_output.network;

import java.net.*;
import java.io.*;
import server.database.account.Character;
import state.states.Loading_State;

public class Downloader extends Thread{

	private ServerSocket SS;
	private Socket S;
	private InputStream IS;
	private ObjectInputStream OIS;
	private String Object;
	private boolean active = true;
	private boolean destroy = false;
	private Loading_State loading_state;

	/**
	 * Attempts to create a thread for Downloads on a port ranged from PortStart - PortStop.
	 * @param PortStart attempts to start with this port if it fails increases the port by +1
	 * @param PortStop the last port to be tried
	 * @param Client the client where the downloaded objects should be sent
	 */
	public Downloader(int PortStart, int PortStop, Loading_State loading_state){
		this.loading_state = loading_state;

		for(int i = PortStart;i<PortStop;i++){
			try {
				SS = new ServerSocket(i);
				System.out.println("Successfully created a Socket on port: " + i);
				break;
			}
			catch (Exception e) {
				e.printStackTrace();
				System.err.println("Failed to create server on port: " + i);
			}
		}
	}

	/**
	 * Sets the object to be downloaded to
	 * @param Object name in String
	 */
	public void setObject(String Object){
		this.Object = Object;
	}

	public void run(){
		while(!destroy){
			while(active){
				try {
					S = SS.accept();
					IS = S.getInputStream();
					if(IS!=null && IS.available()>0){
						OIS = new ObjectInputStream(IS);
						switch(Object){
						case "Character":
							Character Char = (Character)OIS.readObject();
							loading_state.setChar(Char);
							pause();
						case "Map":
							//TODO MAP
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Pauses the downloader
	 */
	private void pause(){
		this.active = false;
	}

	/**
	 * Unpauses the downloader
	 */
	public void unpause(){
		this.active = true;
	}
	
	/**
	 * Will kill the thread permanently
	 */
	@Override
	public void destroy(){
		destroy = true;
	}

	/**
	 * Unpauses the downloader and sets the object to be downloaded to
	 * @param Object name in String
	 */
	public void unpause(String Object){
		this.Object = Object;
		this.active = true;
	}
	
	/**
	 * 
	 * @return the used port number.
	 */
	public int getPort(){
		return SS.getLocalPort();
	}
}
