package database;

import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Map {

	private static short[][] loadedTiles;
	private static Point loadedTilesOrigin;	

	public static short[][] loadZone(int xPosIn, int yPosIn, int xBufferSize, int yBufferSize, String path){
		loadedTiles = new short[xBufferSize][yBufferSize];
		loadedTilesOrigin = new Point(xPosIn-xBufferSize/2,yPosIn-yBufferSize/2);
		try {

			File file = new File(path);
			Scanner scanner = new Scanner(file);
			short mapSizeX = scanner.nextShort();
			short mapSizeY = scanner.nextShort();
			System.out.println(mapSizeY + " " + mapSizeX);

			int rowCount=0; int colCount=0;

			for(int currentRow = 0;currentRow<yBufferSize;currentRow++)
			{
				for(int currentCol = 0;currentCol<xBufferSize;currentCol++)
				{

					if(xPosIn+currentCol -(xBufferSize/2) < 0 || yPosIn+currentRow-(yBufferSize/2) < 0 || xPosIn+currentCol-(xBufferSize/2) >mapSizeX-1 || yPosIn+currentRow-(yBufferSize/2) > mapSizeY-1){
						loadedTiles[currentRow][currentCol] = -1;
					}
					else
					{ 
						while (rowCount < 1+yPosIn+currentRow-yBufferSize/2){
							scanner.nextLine();
							rowCount++;
						}
						while (colCount < xPosIn+currentCol-(xBufferSize/2)){
							scanner.next();
							colCount++;
						}

						loadedTiles[currentRow][currentCol]=scanner.nextShort();
						colCount++;
					}
				}
				//must reset buffer, otherwise, it intendents wrong
				colCount = 0;
			}
			//closes the file
			scanner.close(); 
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return loadedTiles;
	}



	/**
	 * this method returns true if the current tile tested 
	 * is passable. Else, it returns false
	 * 
	 * @param in the value of the tile sent to the method
	 * @return true if the tile is passable or false if the tile 
	 * is UNpassable
	 */
	public static boolean checkPassage(int x, int y){
		short tile = loadedTiles[y-loadedTilesOrigin.y][x-loadedTilesOrigin.x];
		switch(tile){
		  case -1:
		  case 1: 
		  case 2: 
		  case 3: 
		  case 4: 
		  case 6: 
		  case 7: 
		  case 8: 
		  case 9: 
		  case 10:
		  case 11:
		  case 12:
		  case 13:
		  case 14:
		   //15 till 27 = passable
		  case 28:
		  case 29:
		  case 30:
		  case 31:
		   //32 till 44 = passable
		  case 45:
		  case 46:
		  case 47:
		  case 48:
		   return false;
		  default:
		   return true;
		}
	}

	/**
	 * This method will take take in to account if the characters is on the move
	 * and return the short[][] of the tiles that need to be drawn. For example
	 * if the character is moving to the right that means the double x will be
	 * some number + a float. If this is the case the method will return the tile
	 * IDs to the right of the screen as well as the normal field of vision.
	 * 
	 * This method will get its info from the loadedTiles!
	 * @param px the current x point of the char
	 * @param py the current y point of the char
	 * @return Field of Vision
	 */
	public static short[][] getVision(double px, double py){
		if(((((int) (px+0.5)) - px) > 0) && ((((int) (py+0.5)) - py) > 0)){
			//R�R SIG UPP �T V�NSTER
			//Skapar pointen l�ngst upp till v�nster i View Field
			Point start = new Point((int) px-10,(int) py-7);

			//Skapar pointen l�ngst upp till v�nster i View Field BEROENDE p� zonens interna kordinatsystem
			Point start2 = new Point((start.x)-(loadedTilesOrigin.x),(start.y)-(loadedTilesOrigin.y));

			//Skapar ny tempor�r field som ska returneras �t clienten (dvs det den ska se)
			short[][] fieldofVision = new short[16][22];

			//Loop som ska ladda �ver zonens interna tiles till Views vision field
			for(int y = 0;y<16;y++){
				for(int x = 0;x<22;x++){
					/*
					 * Tar h�r de kordinaterna l�ngst upp till v�nster i det nya field of vision fr�n zonens totala field
					 * beroende p� vart i zonen field of vision befinner sig
					 */

					fieldofVision[y][x] = loadedTiles[(start2.y)+y][(start2.x)+x];
				}
			}
			return fieldofVision;
		}
		else if(((((int) (px+0.5)) - px) > 0) && ((((int) (py+0.5)) - py) < 0)){
			//R�R SIG NER �T V�NSTER
			Point start = new Point((int) px-10,(int) py-7);

			Point start2 = new Point((start.x)-(loadedTilesOrigin.x), (start.y)-(loadedTilesOrigin.y));

			short[][] fieldofVision = new short[16][22];

			for(int y = 0;y<16;y++){
				for(int x = 0;x<22;x++){			
					fieldofVision[y][x] = loadedTiles[(start2.y)+y][(start2.x)+x];
				}
			}
			return fieldofVision;
		}
		else if(((((int) (px+0.5)) - px) < 0) && ((((int) (py+0.5)) - py) > 0)){
			//R�R SIG UPP �T H�GER
			Point start = new Point((int) px-10,(int) py-7);

			Point start2 = new Point((start.x)-(loadedTilesOrigin.x), (start.y)-(loadedTilesOrigin.y));

			short[][] fieldofVision = new short[16][22];

			for(int y = 0;y<16;y++){
				for(int x = 0;x<22;x++){			
					fieldofVision[y][x] = loadedTiles[(start2.y)+y][(start2.x)+x];
				}
			}
			return fieldofVision;
		}
		else if(((((int) (px+0.5)) - px) < 0) && ((((int) (py+0.5)) - py) < 0)){
			//R�R SIG NER �T H�GER
			Point start = new Point((int) px-10,(int) py-7);

			Point start2 = new Point((start.x)-(loadedTilesOrigin.x), (start.y)-(loadedTilesOrigin.y));

			short[][] fieldofVision = new short[16][22];

			for(int y = 0;y<16;y++){
				for(int x = 0;x<22;x++){			
					fieldofVision[y][x] = loadedTiles[(start2.y)+y][(start2.x)+x];
				}
			}
			return fieldofVision;
		}
		else if((((int) (px+0.5)) - px) > 0){
			//R�R SIG �T V�NSTER
			//			System.out.println("MOVE LEFT");
			Point start = new Point((int) px-10,(int) py-7);

			Point start2 = new Point((start.x)-(loadedTilesOrigin.x), (start.y)-(loadedTilesOrigin.y));

			short[][] fieldofVision = new short[15][22];

			for(int y = 0;y<15;y++){
				for(int x = 0;x<22;x++){			
					fieldofVision[y][x] = loadedTiles[(start2.y)+y][(start2.x)+x];
				}
			}
			return fieldofVision;
		}
		else if((((int) (px+0.5)) - px) < 0){
			//R�R SIG �T H�GER
			//			System.out.println("MOVE RIGHT");
			Point start = new Point((int) px-10,(int) py-7);

			Point start2 = new Point((start.x)-(loadedTilesOrigin.x), (start.y)-(loadedTilesOrigin.y));

			short[][] fieldofVision = new short[15][22];

			for(int y = 0;y<15;y++){
				for(int x = 0;x<22;x++){			
					fieldofVision[y][x] = loadedTiles[(start2.y)+y][(start2.x)+x];
				}
			}
			return fieldofVision;
		}
		else if((((int) (py+0.5)) - py) > 0){
			//R�R SIG UPP�T
			//			System.out.println("MOVE UP");
			Point start = new Point((int) px-10,(int) py-7);

			Point start2 = new Point((start.x)-(loadedTilesOrigin.x), (start.y)-(loadedTilesOrigin.y));

			short[][] fieldofVision = new short[16][21];

			for(int y = 0;y<16;y++){
				for(int x = 0;x<21;x++){			
					fieldofVision[y][x] = loadedTiles[(start2.y)+y][(start2.x)+x];
				}
			}
			return fieldofVision;
		}
		else if((((int) (py+0.5)) - py) < 0){
			//R�R SIG NER�T
			//			System.out.println("MOVE DOWN");
			Point start = new Point((int) px-10,(int) py-7);

			Point start2 = new Point((start.x)-(loadedTilesOrigin.x), (start.y)-(loadedTilesOrigin.y));

			short[][] fieldofVision = new short[16][21];

			for(int y = 0;y<16;y++){
				for(int x = 0;x<21;x++){			
					fieldofVision[y][x] = loadedTiles[(start2.y)+y][(start2.x)+x];
				}
			}
			return fieldofVision;
		}
		else{
			//R�R SIG INTE
			//			System.out.println("STAND PUT");
			Point start = new Point((int) px-10,(int) py-7);

			Point start2 = new Point((start.x)-(loadedTilesOrigin.x), (start.y)-(loadedTilesOrigin.y));

			short[][] fieldofVision = new short[15][21];

			for(int y = 0;y<15;y++){
				for(int x = 0;x<21;x++){			
					fieldofVision[y][x] = loadedTiles[(start2.y)+y][(start2.x)+x];
				}
			}
			return fieldofVision;
		}
	}

	public static boolean enteredBuffertZone(Point P){
		//TA H�R REDA P� OM MAN KOMMIT MOT KANTEN OCH IS�FALL LADDA OM KARTAN
		return false;
	}


	//	//NEDAN F�LJER TEST METOD F�R DETTA
	//	public static void main(String[] args){
	//		loadedTilesOrigin = new Point(0,0);
	//		loadTiles(new String("lol"), loadedTilesOrigin);
	//		for(int y = 0;y<loadedTiles.length;y++){
	//			for(int x = 0;x<loadedTiles[y].length;x++){
	//				System.out.print(loadedTiles[y][x] + " ");
	//			}
	//			System.out.println();
	//		}
	//		System.out.println("------------");
	//		short[][] temp = getVision(17, 15.1);
	//
	//		for(int y = 0;y<temp.length;y++){
	//			for(int x = 0;x<temp[y].length;x++){
	//				if (temp.length/2 == y && temp[y].length/2 == x){
	//					System.out.print("X ");
	//				}
	//				else{
	//					System.out.print(temp[y][x] + " ");
	//				}
	//			}
	//			System.out.println();
	//		}
	//	}
}
