/*
 * A Character works as a dynamic database for a logged in player.
 */

package database;

import java.awt.Point;
import java.io.Serializable;

public class Character implements Serializable{
	private static final long serialVersionUID = -6390272842934614484L;
	
	private String name;
	private int CurrentHealthPool;
	private int MaximumHealthPool;
	private int CurrentManaPool;
	private int MaximumManaPool;
	private short[] Inventory = new short[10];
	private int[] Skill = new int[6];
	private int Level;
	private int Experience;
	private int UsedCapacity;
	private int MaximumCapacity;
	private short[] BagSpace = new short[40];
	private Point Coordinates;
	private float MovementSpeed;

	/**
	 * Constructs a character with specified values
	 * @param name of the character
	 * @param CHP CurrentHealthPool
	 * @param CMP CurrentManaPool
	 * @param Inventory An array of all the items in the 10 inventory slots
	 * @param Skill An array of all the skill levels within each skill
	 * @param Level The level of the character
	 * @param Exp The experience at that level
	 * @param UC UsedCapacity
	 */
	public Character(String name, int CHP, int CMP, short[] Inventory, int[] Skill, int Level, int Exp, int UC, short[] BagSpace, Point Point){
		this.name = name;
		this.Level = Level;
		this.Experience = Exp;
		this.CurrentHealthPool = CHP;
		this.MaximumHealthPool = 90+this.Level*10;
		this.CurrentManaPool = CMP;
		this.MaximumManaPool = 45+this.Level*5;
		this.Inventory = Inventory;
		this.Skill = Skill;
		this.UsedCapacity = UC;
		this.MaximumCapacity = 190+this.Level*10;
		this.BagSpace = BagSpace;
		this.Coordinates = Point;
		this.MovementSpeed = ((float) this.Level)*(((float)0.01)+1);
	}
	
	/**
	 * Constructs a new character!
	 */
	public Character(String name){
		this.name = name;
		this.Level = 1;
		this.Experience = 0;
		this.CurrentHealthPool = 100;
		this.MaximumHealthPool = 100;
		this.CurrentManaPool = 50;
		this.MaximumManaPool = 50;
		this.Inventory = new short[10];
		for (int i = 0;i<Inventory.length;i++){
			Inventory[i] = 0; //Starts without inventory
		}
		this.Skill = new int[6];
		for (int i = 0;i<Skill.length;i++){
			Skill[i] = 0; //Starts at 0 in all skills
		}
		this.UsedCapacity = 0;
		this.MaximumCapacity = 200;
		for (int i = 0;i<BagSpace.length;i++){
			BagSpace[i] = 0; //Starts without anything in the bags
		}
		this.Coordinates = new Point(50,50); //Starts at 7,5
	}
	
	/**
	 * Gets the characters name
	 * @return name as String
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Returns the characters healthpool
	 * @return currentHP
	 */
	public int getHP(){
		return CurrentHealthPool;
	}
	
	/**
	 * Returns the characters manapool
	 * @return currentMP
	 */
	public int getMP(){
		return CurrentManaPool;
	}
	
	/**
	 * Gets the skill from the array Skill[skill]
	 * @param skill picks the desired skill
	 * @return the skill
	 */
	public int getSkill(int skill){
		return Skill[skill];
	}
	
	/**
	 * Damages the character
	 * @param amount of damage
	 * @return true if lethal hit false if not
	 */
	public boolean damage(int amount){
		CurrentHealthPool -= amount;
		return checkDeath();
	}
	
	/**
	 * checks if character took lethal damage
	 * @return true if dead or false if alive
	 */
	private boolean checkDeath(){
		if(CurrentHealthPool<=0){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Adds to the current health if below the cap.
	 * @param amount to be added
	 */
	public void addHP(int amount){
		CurrentHealthPool += amount;
		if(CurrentHealthPool>MaximumHealthPool){
			CurrentHealthPool = MaximumHealthPool;
		}
	}
	
	/**
	 * Adds to the current mana if below the cap.
	 * @param amount to be added
	 */
	public void addMP(int amount){
		CurrentManaPool += amount;
		if(CurrentManaPool>MaximumManaPool){
			CurrentManaPool = MaximumManaPool;
		}
	}
	
	/**
	 * Adds experience to a character if he reached enough to level up the method will return true otherwise false
	 * Will also automatically update the characters healthpool, manapool and capacity.
	 * @param amount experience to gain
	 * @return false if no level was reached or true if a new level was reached
	 */
	public boolean addExperience(int amount){
		Experience += amount;
		if(Experience >= Level*100){
			Experience -= Level*100;
			levelUp();
			return true;
		}
		return false;
	}
	
	/**
	 * When a character levels up his maximum healthpool, manapool and capacity increases.
	 */
	private void levelUp(){
		MaximumHealthPool += 10;
		MaximumManaPool += 5;
		MaximumCapacity += 10;
		addHP(10);
		addMP(5);
		MovementSpeed += 0.01;
	}
	
	/**
	 * Adds used capacity to the player
	 * @param amount to be added
	 * @return false if the added capacity exceeded the maximum cap otherwise true (does not add capacity if it exceeds the max cap)
	 */
	public boolean addCapacity(int amount){
		if(UsedCapacity+amount>MaximumCapacity){
			return false;
		}
		UsedCapacity += amount;
		return true;
	}
	
	/**
	 * Decreases the used capacity
	 * @param amount to be removed from used capacity
	 */
	public void removeCapacity(int amount){
		UsedCapacity -= amount;
	}
	
	/**
	 * Gets the Coordinates of the character
	 * @return Point(x,y)
	 */
	public Point getCoordinates(){
		return Coordinates;
	}
	
	/**
	 * Sets the Coordinates of the character
	 * @param Point(x,y)
	 */
	public void setCoordinates(Point Point){
		Coordinates = Point;
	}
	
	/**
	 * Gets the MovementSpeed of the character
	 * @return MovementSpeed
	 */
	public float getMovementSpeed(){
		return MovementSpeed;
	}
}
