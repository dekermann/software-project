package database;

public class Monsterlist {

	public static int getMonsterType(int ID){
		for(int i = 1200;i<10000;i += 200){
			if(ID<i){
				return ((i-1200)/200)+1;
			}
		}
		return 0;
	}
	
	public static int getMonsterHP(int ID){
		switch(ID){
		case 1:
			return 50;
		case 2:
			return 20;
		case 3:
			return 120;
		case 4:
			return 10;
		}
		return 0;
	}
}
