package gui.entities;

import java.awt.Point;
import java.util.ArrayList;

public class EntityBucket {

	public static boolean flaga = false; 
	public static boolean flag1b = false;
	public static boolean flag2b = false;
	public static boolean flag3b = false;
	
	private static ArrayList<String> StaticEntityBucket = new ArrayList<String>();
	private static ArrayList<String> DynamicEntityBucket1 = new ArrayList<String>();
	private static ArrayList<String> DynamicEntityBucket2 = new ArrayList<String>();
	
	private static ArrayList<Integer[]> StaticObjects = new ArrayList<Integer[]>();
	private static ArrayList<Integer[]> DynamicObjects1 = new ArrayList<Integer[]>();
	private static ArrayList<Integer[]> DynamicObjects2 = new ArrayList<Integer[]>();
	private static ArrayList<Integer[]> TStaticObjects = new ArrayList<Integer[]>();
	private static ArrayList<Integer[]> TDynamicObjects1 = new ArrayList<Integer[]>();
	private static ArrayList<Integer[]> TDynamicObjects2 = new ArrayList<Integer[]>();
	
	public static void addEntity(String[] paths, int Animations){
		if(Animations < 2){
			StaticEntityBucket.add(paths[0]);
		}
		else if(Animations == 3){
			for(int i = 0;i<3;i++){
				DynamicEntityBucket1.add(paths[i]);
			}
		}
		else if(Animations == 16){
			for(int i = 0;i<16;i++){
				DynamicEntityBucket2.add(paths[i]);
			}
		}
	}

	public static ArrayList<String> getStaticEntities(){
		return StaticEntityBucket;
	}
	
	public static ArrayList<String> getDynamicEntities1(){
		return DynamicEntityBucket1;
	}
	
	public static ArrayList<String> getDynamicEntities2(){
		return DynamicEntityBucket2;
	}
	
	public static void addStaticDisplayEntity(int ID, Point Point, int SizeX, int SizeY){
		TStaticObjects.add(new Integer[] {ID,Point.x,Point.y,SizeX,SizeY});
	}
	
	public static void addDynamicDisplayEntity1(int ID, Point Point, int SizeX, int SizeY){
		TDynamicObjects1.add(new Integer[] {ID,Point.x,Point.y,SizeX,SizeY});
	}
	
	public static void addDynamicDisplayEntity2(int ID, Point Point, int SizeX, int SizeY, int Animation){
		TDynamicObjects2.add(new Integer[] {(ID*16)+(Animation*4),Point.x,Point.y,SizeX,SizeY});
	}
	
	public static ArrayList<Integer[]> getStaticeDisplayObjects(){
		return StaticObjects;
	}
	
	public static ArrayList<Integer[]> getDynamicDisplayObjects1(){
		return DynamicObjects1;
	}
	
	public static ArrayList<Integer[]> getDynamicDisplayObjects2(){
		return DynamicObjects2;
	}
	
	public static void setEntities(){
		StaticObjects = TStaticObjects;
		DynamicObjects1 = TDynamicObjects1;
		DynamicObjects2 = TDynamicObjects2;
		StaticObjects.clear();
		DynamicObjects1.clear();
		DynamicObjects2.clear();
	}
	
	public static void clearDisplay(){
		StaticEntityBucket.clear();
		DynamicEntityBucket1.clear();
		DynamicEntityBucket2.clear();
	}
	
	public static void clear(){
		StaticEntityBucket.clear();
		DynamicEntityBucket1.clear();
		DynamicEntityBucket2.clear();
		StaticObjects.clear();
		DynamicObjects1.clear();
		DynamicObjects2.clear();
	}
}
