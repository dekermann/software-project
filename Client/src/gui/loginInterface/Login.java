package gui.loginInterface;

import java.net.InetAddress;
import java.net.UnknownHostException;

import state.states.Login_State;
import client.Client;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.EditField;
import de.matthiasmann.twl.Event;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Timer;
import de.matthiasmann.twl.EditField.Callback;

public class Login extends DialogLayout {

	// Login panel initiation
	static DialogLayout loginPanel = null;
	EditField efIP = null;
	EditField efName = null;;
	EditField efPassword = null;
	Label lIP = null;
	Label lName = null;
	Label lPassword = null;
	Button btnLogin = null;

	public Login() {

		loginPanel = new DialogLayout();
		loginPanel.setTheme("login-panel");

		this.efIP = new EditField();
		efIP.setText("127.0.0.1"); // Temporary Auto add text
		efIP.addCallback(new Callback() {
			public void callback(int key) {
				if (key == Event.KEY_RETURN) {
					efName.requestKeyboardFocus();
				}
			}
		});

		this.efName = new EditField();
		efName.setText("Account1"); // Temporary code
		efName.addCallback(new Callback() {
			public void callback(int key) {
				if (key == Event.KEY_RETURN) {
					efPassword.requestKeyboardFocus();
				}
			}
		});

		this.efPassword = new EditField();
		efPassword.setText("Password1"); // Temporary code
		efPassword.setPasswordMasking(true);
		efPassword.addCallback(new Callback() {
			public void callback(int key) {
				if (key == Event.KEY_RETURN) {
					LoginFunktion();
				}
			}
		});

		this.lIP = new Label("Server IP");
		lIP.setLabelFor(efIP);

		this.lName = new Label("Name");
		lName.setLabelFor(efName);

		this.lPassword = new Label("Password");
		lPassword.setLabelFor(efPassword);

		this.btnLogin = new Button("Login");
		btnLogin.addCallback(new Runnable() {
			public void run() {
				LoginFunktion();
			}
		});

		DialogLayout.Group hLabels = loginPanel.createParallelGroup(lIP, lName,
				lPassword);
		DialogLayout.Group hFields = loginPanel.createParallelGroup(efIP,
				efName, efPassword);
		// Right align the button by using a variable gap
		DialogLayout.Group hBtn = loginPanel.createSequentialGroup().addGap()
				.addWidget(btnLogin);

		loginPanel.setHorizontalGroup(loginPanel.createParallelGroup()
				.addGroup(loginPanel.createSequentialGroup(hLabels, hFields))
				.addGroup(hBtn));
		loginPanel
				.setVerticalGroup(loginPanel
						.createSequentialGroup()
						.addGroup(loginPanel.createParallelGroup(lIP, efIP))
						.addGroup(loginPanel.createParallelGroup(lName, efName))
						.addGroup(
								loginPanel.createParallelGroup(lPassword,
										efPassword)).addWidget(btnLogin));
	}

	public static DialogLayout getFrame() {
		return loginPanel;
	}

	void LoginFunktion() {
		if (LoginInterface.gui != null) {
			// Step 1: disable all controls
			efIP.setEnabled(false);
			efName.setEnabled(false);
			efPassword.setEnabled(false);
			btnLogin.setEnabled(false);

			/*
			 * Step 2: Get IP & name & password from UI and then format the Name
			 * and Password for the server.
			 */
			String IP = efIP.getText();
			try {
				Client.S.setIP(InetAddress.getByName(IP));
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String name = efName.getText();
			int name_buffer = 16 - name.length();
			String password = efPassword.getText();
			int password_buffer = 16 - password.length();

			Login_State.request = name;
			for (int i = 0; i < name_buffer; i++)
				Login_State.request += "0";
			Login_State.request += password;
			for (int i = 0; i < password_buffer; i++)
				Login_State.request += "0";
			System.out.println(Login_State.request);
			Login_State.request += "4321";

			// step 3: start a timer to simulate the process of talking to a
			// remote server
			Timer timer = LoginInterface.gui.createTimer();
			timer.setCallback(new Runnable() {
				public void run() {
					// once the timer fired re-enable the controls and clear the
					// password
					efIP.setEnabled(true);
					efName.setEnabled(true);
					efPassword.setEnabled(true);
					efPassword.setText("");
					efPassword.requestKeyboardFocus();
					btnLogin.setEnabled(true);
					if (Login_State.SelectionState == 0) {
						System.out
								.println("Wrong password or bad connection to server!");
					} else if (Login_State.SelectionState == 1) {
						LoginInterface.character.updateCharacterSelection();
						LoginInterface.characterPanel.setVisible(true);
						LoginInterface.loginPanel.setVisible(false);
					} else {
						LoginInterface.characterPanel.setVisible(false);
						LoginInterface.loginPanel.setVisible(false);
					}
				}
			});

			// A time delay so the server and client may signal each other
			timer.setDelay(1000);
			timer.start();
		}
	}
}
