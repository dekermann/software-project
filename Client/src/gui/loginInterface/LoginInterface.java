package gui.loginInterface;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;

import state.states.Login_State;

import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.GUI;
import de.matthiasmann.twl.Widget;
import de.matthiasmann.twl.renderer.lwjgl.LWJGLRenderer;
import de.matthiasmann.twl.theme.ThemeManager;

public class LoginInterface extends Widget {
	/*
	 * Tillagt av Anders Bea: En inbyggd renderer som kommer att ers�tta
	 * render(), samt flera andra verktyg som kommer att integrera TWL i v�rt
	 * spel.
	 */
	public static LWJGLRenderer renderer = null;
	public static GUI gui = null;
	public static ThemeManager theme = null;
	static LoginInterface gameUI = null;
	public int MAX_NR_CHAR = 5;
	public static String deleteCharacterQueue = null;

	public static void initTWL() {
		/*
		 * Tillagt av Anders Bea: H�r initieras de olika komponenterna som
		 * kommer att anv�ndas av TWL.
		 */
		try {
			renderer = new LWJGLRenderer();
			gameUI = new LoginInterface();
			gui = new GUI(gameUI, renderer);
			// File file = new File("Media\\Login_State\\twlgui.xml");
			File file = new File("Media\\Login_State\\login.xml");
			@SuppressWarnings("deprecation")
			URL url = file.toURL();
			theme = ThemeManager.createThemeManager(url, renderer);
			gui.applyTheme(theme);
		} catch (LWJGLException lwjglException) {
			showErrMsg(lwjglException);
		} catch (Exception ee) {
			showErrMsg(ee);
		}
	}

	/**
	 * Displays exception info on the screen, and on system console
	 * 
	 * @param ex
	 */
	public static void showErrMsg(Throwable ex) {
		ex.printStackTrace();
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		pw.flush();
		Sys.alert("Error!", sw.toString());
	}

	static Login login = null;
	static DialogLayout loginPanel = null;
	
	static CharacterSelect character = null;
	static DialogLayout characterPanel = null;
	
	static Answer answer = null;
	static DialogLayout answerPanel = null;
	
	static CharacterCreate create = null;
	static DialogLayout createPanel = null;

	public LoginInterface() {
		
		login = new Login();
		loginPanel = Login.getFrame();
		add(loginPanel);
		
		character = new CharacterSelect();
		characterPanel = character.getFrame();
		add(characterPanel);
		
		characterPanel.setVisible(false);
		
		answer = new Answer();
		answerPanel = Answer.getFrame();
		add(answerPanel);
		answerPanel.setVisible(false);
		
		create = new CharacterCreate();
		createPanel = CharacterCreate.getFrame();
		add(createPanel);
		createPanel.setVisible(false);
		
	}
	
	public static void characterLogin(String Character) {
		Login_State.request = "2" + Character + "&";
		System.out.println("Load Character: " + Character);
		gui.setVisible(false);
		gameUI.setVisible(false);
		gui.setEnabled(false);
		gameUI.setEnabled(false);
	}

	/*
	 * protected boolean handleEvent(Event evt) { boolean hitGUI =
	 * super.handleEvent(evt); if (!hitGUI) { // Handle the event as a click on
	 * the game world. if (evt.getType() == Event.Type.MOUSE_BTNDOWN) {
	 * System.out.println("You missed the button!!!"); } } return hitGUI; // Not
	 * really important }
	 */

	protected void layout() {
		loginPanel.adjustSize();
		loginPanel.setPosition(
				getInnerX() + (getInnerWidth() - loginPanel.getWidth()) / 2,
				getInnerY() + (getInnerHeight() - loginPanel.getHeight()) / 2);

		characterPanel.adjustSize();
		characterPanel.setPosition(getInnerX()
				+ (getInnerWidth() - characterPanel.getWidth()) / 2,
				getInnerY() + (getInnerHeight() - characterPanel.getHeight())
						/ 2);

		answerPanel.adjustSize();
		answerPanel.setPosition(
				getInnerX() + (getInnerWidth() - answerPanel.getWidth()) / 2,
				getInnerY() + (getInnerHeight() - answerPanel.getHeight()) / 2);

		createPanel.adjustSize();
		createPanel.setPosition(
				getInnerX() + (getInnerWidth() - createPanel.getWidth()) / 2,
				getInnerY() + (getInnerHeight() - createPanel.getHeight()) / 2);
	}

	protected void resize(DialogLayout panel) {
		panel.adjustSize();
		panel.setPosition(getInnerX() + (getInnerWidth() - panel.getWidth())
				/ 2, getInnerY() + (getInnerHeight() - panel.getHeight()) / 2);
	}

	public void showCharacterSelection() {
		character.setVisible(true);
	}
	
	public static void updateCharacterSelect(){
		characterPanel = character.getFrame();
	}
}
