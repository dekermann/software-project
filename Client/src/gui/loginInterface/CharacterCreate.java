package gui.loginInterface;

import state.states.Login_State;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.EditField;
import de.matthiasmann.twl.Event;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Timer;
import de.matthiasmann.twl.Widget;
import de.matthiasmann.twl.EditField.Callback;

public class CharacterCreate extends Widget {

	static DialogLayout createPanel;
	Label efLabel;
	EditField efName;
	Button cancelBtn;
	Button createBtn;

	public CharacterCreate() {
		createPanel = new DialogLayout();
		createPanel.setTheme("login-panel");
		createPanel.setVisible(false);
		efLabel = new Label("Character Name");
		efName = new EditField();
		efName.addCallback(new Callback() {
			public void callback(int key) {
				if (key == Event.KEY_RETURN) {
					create(efName.getText());

				}
			}
		});
		
		createBtn = new Button("Create");
		createBtn.addCallback(new Runnable() {
			public void run() {
				create(efName.getText());
			}
		});
		
		cancelBtn = new Button("Cancel");
		cancelBtn.addCallback(new Runnable() {
			public void run() {
				createPanel.setVisible(false);
				LoginInterface.characterPanel.setVisible(true);
			}
		});

		DialogLayout.Group xLabels = createPanel.createParallelGroup(efLabel,
				efName);
		DialogLayout.Group xBtn = createPanel.createSequentialGroup().addGap()
				.addWidget(createBtn).addWidget(cancelBtn);

		createPanel.setHorizontalGroup(createPanel.createParallelGroup()
				.addGroup(createPanel.createSequentialGroup(xLabels))
				.addGroup(xBtn));
		createPanel.setVerticalGroup(createPanel
				.createSequentialGroup()
				.addGroup(createPanel.createParallelGroup(efLabel))
				.addGroup(createPanel.createParallelGroup(efName))
				.addGroup(
						createPanel.createParallelGroup().addWidget(createBtn)
								.addWidget(cancelBtn)));
	}

	public static DialogLayout getFrame() {
		return createPanel;
	}

	// Robin: Skapa karakt�ren och �terg� till "Character Selection" panelen
	void create(String newCharName) {
		Login_State.request = "1" + newCharName + "&";
		System.out.println("Creating Character: " + newCharName);
		LoginInterface.character.updateCharacterSelection();
		Timer timer = LoginInterface.gui.createTimer();
		timer.setCallback(new Runnable() {
			public void run() {
				LoginInterface.character.resetCharacterSelection();
				LoginInterface.character.updateCharacterSelection();
				LoginInterface.updateCharacterSelect();
				createPanel.setVisible(false);
				LoginInterface.characterPanel.setVisible(true);
			}
		});
		timer.setDelay(3000);
		timer.start();
	}

}
