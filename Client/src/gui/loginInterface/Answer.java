package gui.loginInterface;

import state.states.Login_State;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.Widget;

public class Answer extends Widget {
	
	public static DialogLayout answerPanel;
	public static Button btnYes, btnNo;
	
	public Answer() {

		answerPanel = new DialogLayout();
		answerPanel.setTheme("login-panel");

		btnYes = new Button("Yes");
		btnYes.addCallback(new Runnable() {
			public void run() {
				Login_State.request = "3" + LoginInterface.deleteCharacterQueue + "&";
				LoginInterface.answerPanel.setVisible(false);
				LoginInterface.character.resetCharacterSelection();
				LoginInterface.character.updateCharacterSelection();
				LoginInterface.updateCharacterSelect();
				LoginInterface.characterPanel.setVisible(true);
			}
		});

		btnNo = new Button("No");
		btnNo.addCallback(new Runnable() {
			public void run() {
				LoginInterface.characterPanel.setVisible(true);
				LoginInterface.answerPanel.setVisible(false);
			}
		});
		DialogLayout.Group hLabels = answerPanel.createParallelGroup(btnYes);
		DialogLayout.Group hFields = answerPanel.createParallelGroup(btnNo);

		answerPanel.setHorizontalGroup(answerPanel.createParallelGroup()
				.addGroup(answerPanel.createSequentialGroup(hLabels, hFields)));

		answerPanel.setVerticalGroup(answerPanel.createSequentialGroup()
				.addGroup(answerPanel.createParallelGroup(btnYes, btnNo)));
	}

	public static DialogLayout getFrame() {
		return answerPanel;
	}
}
