package gui.loginInterface;

import java.util.ArrayList;

import state.states.Login_State;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.Widget;

public class CharacterSelect extends Widget {

	// Character selection Panel initiation
	public static DialogLayout characterPanel;
	public static Button Character1;
	public static Button Character2;
	public static Button Character3;
	public static Button Character4;
	public static Button Character5;

	// Button for delete of Character
	static Button delCharacter1;
	static Button delCharacter2;
	static Button delCharacter3;
	static Button delCharacter4;
	static Button delCharacter5;

	public CharacterSelect() {
		characterPanel = new DialogLayout();
		characterPanel.setTheme("login-panel");
		Character1 = new Button("Add Character");
		Character1.addCallback(new Runnable() {
			public void run() {
				if(!Character1.getText().equals("Add Character"))
					LoginInterface.characterLogin(Character1.getText());
				else if(Character1.getText().equals("Add Character")) {
					characterPanel.setVisible(false);
					LoginInterface.createPanel.setVisible(true);
				}
			}
		});
		Character2 = new Button("Add Character");
		Character2.addCallback(new Runnable() {
			public void run() {
				if(!Character2.getText().equals("Add Character"))
					LoginInterface.characterLogin(Character2.getText());
				else if(Character2.getText().equals("Add Character")) {
					characterPanel.setVisible(false);
					LoginInterface.createPanel.setVisible(true);
				}
			}
		});
		Character3 = new Button("Add Character");
		Character3.addCallback(new Runnable() {
			public void run() {
				if(!Character3.getText().equals("Add Character"))
					LoginInterface.characterLogin(Character3.getText());
				else if(Character3.getText().equals("Add Character")) {
					characterPanel.setVisible(false);
					LoginInterface.createPanel.setVisible(true);
				}
			}
		});
		Character4 = new Button("Add Character");
		Character4.addCallback(new Runnable() {
			public void run() {
				if(!Character4.getText().equals("Add Character"))
					LoginInterface.characterLogin(Character4.getText());
				else if(Character4.getText().equals("Add Character")) {
					characterPanel.setVisible(false);
					LoginInterface.createPanel.setVisible(true);
				}
			}
		});
		Character5 = new Button("Add Character");
		Character5.addCallback(new Runnable() {
			public void run() {
				if(!Character5.getText().equals("Add Character"))
					LoginInterface.characterLogin(Character5.getText());
				else if(Character5.getText().equals("Add Character")) {
					characterPanel.setVisible(false);
					LoginInterface.createPanel.setVisible(true);
				}
			}
		});
		// To create delete Buttons

		delCharacter1 = new Button("Delete");
		delCharacter1.addCallback(new Runnable() {
			public void run() {
				if (!Character1.getText().equals("Add Character")) {
					LoginInterface.deleteCharacterQueue = Character1.getText();
					LoginInterface.characterPanel.setVisible(false);
					LoginInterface.answerPanel.setVisible(true);
				}
			}
		});

		delCharacter2 = new Button("Delete");
		delCharacter2.addCallback(new Runnable() {
			public void run() {
				if (!Character2.getText().equals("Add Character")) {
					LoginInterface.deleteCharacterQueue = Character2.getText();
					LoginInterface.characterPanel.setVisible(false);
					LoginInterface.answerPanel.setVisible(true);
				}
			}
		});

		delCharacter3 = new Button("Delete");
		delCharacter3.addCallback(new Runnable() {
			public void run() {
				if (!Character3.getText().equals("Add Character")) {
					LoginInterface.deleteCharacterQueue = Character3.getText();
					LoginInterface.characterPanel.setVisible(false);
					LoginInterface.answerPanel.setVisible(true);
				}
			}
		});

		delCharacter4 = new Button("Delete");
		delCharacter4.addCallback(new Runnable() {
			public void run() {
				if (!Character4.getText().equals("Add Character")) {
					LoginInterface.deleteCharacterQueue = Character4.getText();
					System.out.println(delCharacter4.getText());
					LoginInterface.characterPanel.setVisible(false);
					LoginInterface.answerPanel.setVisible(true);
				}
			}
		});

		delCharacter5 = new Button("Delete");
		delCharacter5.addCallback(new Runnable() {
			public void run() {
				if (!Character5.getText().equals("Add Character")) {
					LoginInterface.deleteCharacterQueue = Character5.getText();
					LoginInterface.characterPanel.setVisible(false);
					LoginInterface.answerPanel.setVisible(true);
				}
			}
		});

		DialogLayout.Group hLabels = characterPanel.createParallelGroup(
				Character1, Character2, Character3, Character4, Character5);
		DialogLayout.Group hFields = characterPanel.createParallelGroup(
				delCharacter1, delCharacter2, delCharacter3, delCharacter4,
				delCharacter5);

		characterPanel
				.setHorizontalGroup(characterPanel.createParallelGroup()
						.addGroup(
								characterPanel.createSequentialGroup(hLabels,
										hFields)));

		characterPanel.setVerticalGroup(characterPanel
				.createSequentialGroup()
				.addGroup(
						characterPanel.createParallelGroup(delCharacter1,
								Character1))
				.addGroup(
						characterPanel.createParallelGroup(delCharacter2,
								Character2))
				.addGroup(
						characterPanel.createParallelGroup(delCharacter3,
								Character3))
				.addGroup(
						characterPanel.createParallelGroup(delCharacter4,
								Character4))
				.addGroup(
						characterPanel.createParallelGroup(delCharacter5,
								Character5)));
	}

	public DialogLayout getFrame() {
		return characterPanel;
	}
	
	protected void updateCharacterSelection() {
		ArrayList<String> charArray = Login_State.getCharacters();
		if (charArray.size() >= 1) {
			Character1.setText(charArray.get(0));
		}
		if (charArray.size() >= 2) {
			Character2.setText(charArray.get(1));
		}
		if (charArray.size() >= 3) {
			Character3.setText(charArray.get(2));
		}
		if (charArray.size() >= 4) {
			Character4.setText(charArray.get(3));
		}
		if (charArray.size() >= 5) {
			Character5.setText(charArray.get(4));
		}
		
		resize(characterPanel);
	}
	
	protected void resize(DialogLayout panel) {
		panel.adjustSize();
		panel.setPosition(getInnerX() + (getInnerWidth() - panel.getWidth())
				/ 2, getInnerY() + (getInnerHeight() - panel.getHeight()) / 2);
	}
	
	protected void destroyCharacterSelection() {
		characterPanel.destroy();
		Character1.destroy();
		Character2.destroy();
		Character3.destroy();
		Character4.destroy();
		Character5.destroy();
		characterPanel.destroy();
	}

	protected void resetCharacterSelection(){
		Character1.setText("Add Character");
		Character2.setText("Add Character");
		Character3.setText("Add Character");
		Character4.setText("Add Character");
		Character5.setText("Add Character");
	}

}
