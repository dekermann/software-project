package gui.controller;

import java.awt.Point;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import state.states.Game_State;

/**
 * the Input class checks the Keyboard and the Mouse for input 
 * and forwards it to a bucket which can be taken from and thus 
 * read
 * @author David
 *
 */
public class Input {

	private static Point mousePos = new Point(0,0);

	public static void checkInput(){
		/*
		 * the if !Game_state.chatFLag checks so that the game
		 * isn't in chat mode and thus ignores any of the common
		 * requests.
		 */
		if(!Game_State.chatFlag){
			if (Keyboard.isKeyDown(Keyboard.KEY_1)){
				InputBucket.setInputType("1");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_E)){
				InputBucket.setInputType("E");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_W)){
				InputBucket.setInputType("W");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_A)){
				InputBucket.setInputType("A");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_S)){
				InputBucket.setInputType("S");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_D)){
				InputBucket.setInputType("D");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_K)){
				InputBucket.setInputType("K");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_Q)){
				InputBucket.setInputType("Q");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
				InputBucket.setInputType("SPACE");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_W) && Keyboard.isKeyDown(Keyboard.KEY_D)){
				InputBucket.setInputType("WD");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_W) && Keyboard.isKeyDown(Keyboard.KEY_A)){
				InputBucket.setInputType("WA");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_S) && Keyboard.isKeyDown(Keyboard.KEY_D)){
				InputBucket.setInputType("SD");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_S) && Keyboard.isKeyDown(Keyboard.KEY_A)){
				InputBucket.setInputType("SA");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_C)){
				InputBucket.setInputType("C");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_B)){
				InputBucket.setInputType("B");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_I)){
				InputBucket.setInputType("I");
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)){
				InputBucket.setInputType("ESCAPE");
			}
		}
		/*
		 * this part of the code checks for mouse-input. it firsts gets the
		 * x and y position of the mouse and then checks if any of the keys
		 * has been pressed. If it's down, it sets the position of the mouse
		 * into the mousePos, which can be read by other parts of the program
		 */
		mousePos.x = Mouse.getX();
		mousePos.y = Display.getHeight() - Mouse.getY();
		if (Mouse.isButtonDown(0)){
			InputBucket.setMousePoint(mousePos);
			InputBucket.setInputType("M0");
		}
		else if (Mouse.isButtonDown(1)){
			InputBucket.setMousePoint(mousePos);
			InputBucket.setInputType("M1");
		}
		else if (Mouse.isButtonDown(2)){
			InputBucket.setMousePoint(mousePos);
			InputBucket.setInputType("M2");
		}
	}
}