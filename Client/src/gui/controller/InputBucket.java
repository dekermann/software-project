package gui.controller;

import java.awt.Point;

public class InputBucket {

	private static Point Point;
	private static String Type;
	
	public static String getInputType(){
		String temp = Type;
		Type = null;
		return temp;
	}
	
	public static Point getMousePoint(){
		return Point;
	}
	
	public static void setInputType(String T){
		Type = T;
	}
	
	public static void setMousePoint(Point P){
		Point = P;
	}
	
	public static void clear(){
		Type = null;
		Point = null;
	}
}
