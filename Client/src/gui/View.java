package gui;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glViewport;
import gui.controller.Input;
import gui.entities.EntityBucket;
import gui.gameInterface.GameInterface;
import gui.loginInterface.LoginInterface;
import gui.texture.Tex;
import gui.texture.TextureBucket;
import gui.texture.TextureDisplayBucket;
import gui.texture.TextureHandler;

import java.util.ArrayList;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class View extends Thread {

	private int CrashCounter = 0;
	private int animationloop = 0;

	// Whether to enable VSync in hardware.
	public static final boolean VSYNC = false;

	// Width and height of our window
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;

	// Whether to use fullscreen mode
	public static final boolean FULLSCREEN = true;

	// Whether our game loop is running
	public static boolean running = false;

	private TextureBucket TB;
	private TextureDisplayBucket TDB;

	private ArrayList<Tex> currentTextures;
	private ArrayList<String[]> currentTextureInfos;
	private ArrayList<Tex> currentStaticObjects;
	private ArrayList<Tex> currentDynamicObjects1;
	private ArrayList<Tex> currentDynamicObjects2;
	private ArrayList<Integer[]> currentStaticObjectsInfos;
	private ArrayList<Integer[]> currentDynamicObjects1Infos;
	private ArrayList<Integer[]> currentDynamicObjects2Infos;

	public View(TextureBucket TB, TextureDisplayBucket TDB) {
		this.TB = TB;
		this.TDB = TDB;
	}

	// Start our game
	@Override
	public void run() {
		// Set up our display
		try {
			Display.setTitle("Game"); // title of our window
			Display.setResizable(true); // whether our window is resizable
//			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT)); 
			Display.setVSyncEnabled(VSYNC); // whether hardware VSync is enabled
			Display.setFullscreen(FULLSCREEN); //whether fullscreen is enabled

			// create and show our display
			Display.create();

			// Create our OpenGL context and initialize any resources
			create();

			// Call this before running to set up our initial size
			resize();
			
			//Starts and initializes the Twl gui
			LoginInterface.initTWL();
			LoginInterface.renderer.startRendering();
			
			/*
			 *  We initiate the GameInterface but wait until the game enters the 
			 *  GAME_STATE until we enable it.
			 */
			GameInterface.initGameInterface();
			GameInterface.gui.setEnabled(false);
			GameInterface.renderer.startRendering();

			running = true;

			// While we're still running and the user hasn't closed the
			// window...
			while (running && !Display.isCloseRequested()) {
				if (TextureBucket.flag) {
					getTextures();
				}
				if (EntityBucket.flaga) {
					getEntities();
				}
				// If the game was resized, we need to update our projection
				if (Display.wasResized()) {
					resize();
				}

				// System.out.println(getDelta());

				checkInput();
				
				
				//Render the game
				render();
				
				//Gets the gui
				if(LoginInterface.gui.isEnabled())
					LoginInterface.gui.update();
				
				if(GameInterface.gui.isEnabled())
					GameInterface.gui.update();
				
				// Flip the buffers and sync to 60 FPS
				Display.update();
				Display.sync(30);
			}

			// Dispose any resources and destroy our window
			dispose();
			LoginInterface.gui.destroy();
			LoginInterface.theme.destroy();
			GameInterface.gui.destroy();
			GameInterface.theme.destroy();
			Display.destroy();
			System.exit(0);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}

	}

	private void getTextures() {

		ArrayList<String> stringedTextures = new ArrayList<String>();
		stringedTextures = TB.getTextures();
		currentTextures.clear();
		for (int i = 0; i < stringedTextures.size(); i++) {
			currentTextures.add(TextureHandler.TextureCreation(stringedTextures
					.get(i)));
		}
		TextureBucket.flag = false;

	}

	private void getEntities() {
		ArrayList<String> stringedEntities = new ArrayList<String>();

		try {
			stringedEntities = EntityBucket.getStaticEntities();
			currentStaticObjects.clear();
			for (int i = 0; i < stringedEntities.size(); i++) {
				currentStaticObjects.add(TextureHandler
						.TextureCreation(stringedEntities.get(i)));
			}
			stringedEntities = EntityBucket.getDynamicEntities1();
			currentDynamicObjects1.clear();
			for (int i = 0; i < stringedEntities.size(); i++) {
				currentDynamicObjects1.add(TextureHandler
						.TextureCreation(stringedEntities.get(i)));
			}
			stringedEntities = EntityBucket.getDynamicEntities2();
			currentDynamicObjects2.clear();
			for (int i = 0; i < stringedEntities.size(); i++) {
				currentDynamicObjects2.add(TextureHandler
						.TextureCreation(stringedEntities.get(i)));
			}
		} catch (Exception e) {
			System.out.println("Null pointer");
			e.printStackTrace();
		}
		EntityBucket.flaga = false;

	}

	// Exit our game loop and close the window
	public void exit() {
		running = false;
	}

	// Called to setup our game and context
	protected void create() {
		// 2D games generally won't require depth testing
		glDisable(GL_DEPTH_TEST);

		// Enable blending
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Set clear to transparent black
		glClearColor(0f, 0f, 0f, 0f);

		glMatrixMode(GL_PROJECTION);
		glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_TEXTURE_2D);

		currentTextures = new ArrayList<Tex>();
		currentTextureInfos = new ArrayList<String[]>();
		currentStaticObjects = new ArrayList<Tex>();
		currentStaticObjectsInfos = new ArrayList<Integer[]>();
		currentDynamicObjects1 = new ArrayList<Tex>();
		currentDynamicObjects1Infos = new ArrayList<Integer[]>();
		currentDynamicObjects2 = new ArrayList<Tex>();
		currentDynamicObjects2Infos = new ArrayList<Integer[]>();
	}

	// Registers all user inputs
	public void checkInput() {
		Input.checkInput();
	}

	// Called to render our game
	protected void render() {
		// Clear the screen
//		glClear(GL_COLOR_BUFFER_BIT);

		if (TextureDisplayBucket.flag) {
			// currentTextureInfos.clear();
			currentTextureInfos = TDB.getTextures();
			TextureDisplayBucket.flag = false;
		}

		if (EntityBucket.flag1b) {
			// currentTextureInfos.clear();
			currentStaticObjectsInfos = EntityBucket.getStaticeDisplayObjects();
			EntityBucket.flag1b = false;
		}

		if (EntityBucket.flag2b) {
			// currentTextureInfos.clear();
			currentDynamicObjects1Infos = EntityBucket
					.getDynamicDisplayObjects1();
			EntityBucket.flag2b = false;
		}

		if (EntityBucket.flag3b) {
			// currentTextureInfos.clear();
			currentDynamicObjects2Infos = EntityBucket
					.getDynamicDisplayObjects2();
			EntityBucket.flag3b = false;
		}

		for (int i = 0; i < currentTextureInfos.size(); i++) {
			try {
				TextureHandler.TextureDraw(currentTextures.get(Integer
						.parseInt(currentTextureInfos.get(i)[0])),
						currentTextureInfos.get(i));
			} catch (Exception e) {
				System.out.println("CRASH: " + CrashCounter);
				CrashCounter++;
			}
		}

		for (int i = 0; i < currentStaticObjectsInfos.size(); i++) {
			try {
				TextureHandler.TextureDraw(currentStaticObjects
						.get(currentStaticObjectsInfos.get(i)[0]),
						currentStaticObjectsInfos.get(i));
			} catch (Exception e) {
				// e.printStackTrace();
				// System.out.println("CRASH: " + CrashCounter);
				// CrashCounter++;
			}
		}



		for (int i = 0; i < currentDynamicObjects2Infos.size(); i++) {
			try {
				TextureHandler
						.TextureDraw(
								currentDynamicObjects2.get((int) (currentDynamicObjects2Infos
										.get(i)[0] + animationloop / 7.5)),
								currentDynamicObjects2Infos.get(i));
			} catch (Exception e) {
				// e.printStackTrace();
				// System.out.println("CRASH: " + CrashCounter);
				// CrashCounter++;
			}
		}
		
		for (int i = 0; i < currentDynamicObjects1Infos.size(); i++) {
			try {
				TextureHandler.TextureDraw(
						currentDynamicObjects1.get(currentDynamicObjects1Infos
								.get(i)[0] + animationloop / 10),
						currentDynamicObjects1Infos.get(i));
			} catch (Exception e) {
				// e.printStackTrace();
				// System.out.println("CRASH: " + CrashCounter);
				// CrashCounter++;
			}
		}
		
		animationloop++;
		if (animationloop == 30) {
			animationloop = 0;
		}
	}

	// Called to resize our game
	protected void resize() {
		glViewport(0, 0, Display.getWidth(), Display.getHeight());
		// ... update our projection matrices here ...
	}

	// Called to destroy our game upon exiting
	protected void dispose() {
		// ... dispose of any textures, etc ...
	}
}