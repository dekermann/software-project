/*
 * 
*/

package gui.texture;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2i;

import java.io.File;

public class TextureHandler {

	public static Tex TextureCreation(String path) {
		try {
			return new Tex(new File(path).toURI().toURL());
		} catch (Exception e) {
			return null;
		}
	}
	
	public static void TextureDraw(Tex tex, String[] TextureDisplayInfo){
		
		int x = Integer.parseInt(TextureDisplayInfo[1]);
		int y = Integer.parseInt(TextureDisplayInfo[2]);
		int width = Integer.parseInt(TextureDisplayInfo[3]);
		int height = Integer.parseInt(TextureDisplayInfo[4]);
		
		tex.bind();
		
		glBegin(GL_QUADS);
			glTexCoord2f(0,0);
			glVertex2i(x,y);
			glTexCoord2f(1,0);
			glVertex2i(x+width,y);
			glTexCoord2f(1,1);
			glVertex2i(x+width,y+height);
			glTexCoord2f(0,1);
			glVertex2i(x,y+height);
		glEnd();
	}
	
	public static void TextureDraw(Tex tex, Integer[] TextureDisplayInfo){
		
		int x = TextureDisplayInfo[1];
		int y = TextureDisplayInfo[2];
		int width = TextureDisplayInfo[3];
		int height = TextureDisplayInfo[4];
		
//		for(int i = 0;i<TextureDisplayInfo.length;i++){
//			System.out.println(TextureDisplayInfo[i]);
//		}
		
		tex.bind();
		
		glBegin(GL_QUADS);
			glTexCoord2f(0,0);
			glVertex2i(x,y);
			glTexCoord2f(1,0);
			glVertex2i(x+width,y);
			glTexCoord2f(1,1);
			glVertex2i(x+width,y+height);
			glTexCoord2f(0,1);
			glVertex2i(x,y+height);
		glEnd();
	}
}
