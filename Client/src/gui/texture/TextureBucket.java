/*
 * Texture bucket �r en bucket med alla Textures i String format som View kommer att anv�nda i det statet som den befinner sig i.
 * View kommer att h�mta och ladda in alla dess i sitt minne f�r snabb �tkomst n�r de senare kallas som en referens 
 * av View f�r rendering. Referensen kommer att h�mtas ifr�n TextureDisplayBucket!
 */


package gui.texture;

import java.util.ArrayList;

public class TextureBucket {
	private ArrayList<String> TextureBucket;
	
	public static boolean flag = false;
	
	/**
	 * Initializes StringtureBucket, and creates the object StringtureBucket.
	 */
	public TextureBucket(){
		TextureBucket = new ArrayList<String>();
	}
	
	/**
	 * Adds a Texture to the bucket
	 * @param String Texture to be added
	 */
	public void addTexture(String String){
		TextureBucket.add(String);
	}
	
	/**
	 * Clears the bucket of all and any Textures
	 */
	public void clearTextures(){
		TextureBucket.clear();
	}
	
	/**
	 * 
	 * @return all the contents of the bucket
	 */
	public ArrayList<String> getTextures(){
		return TextureBucket;
	}
}