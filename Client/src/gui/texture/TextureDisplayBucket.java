/*
 * TextureDisplayBucket kommer att inneh�lla vart i displayen (View) som alla textures kommer att renderas.
 * Ett object kommer att inneh�lla en referens p� vilken texture det �r samt vart (Point/Kordinater) som
 * den ska renderas. Vart texturesen ska vara kommer att vara input fr�n den state som den ligger i.
 * Staten kommer i sin tur exempelvis f� information fr�n till exempel "map".
 */

package gui.texture;

import java.awt.Point;
import java.util.ArrayList;

public class TextureDisplayBucket {
	private ArrayList<String[]> TextureDisplayBucket;
	private ArrayList<String[]> TempTextureDisplayBucket;
	
	public static boolean flag = false;
	
	/**
	 * Initializes TextureDisplayBucket, creates the object
	 */
	public TextureDisplayBucket(){
		TextureDisplayBucket = new ArrayList<String[]>();
		TempTextureDisplayBucket = new ArrayList<String[]>();
	}
	
	/**
	 * Adds a texture reference and coordinates to the bucket
	 * @param Ref Texture ID
	 * @param X -axis coordinate
	 * @param Y -axis coordinate
	 */
	public void addTexture(int Ref, int X, int Y, int width, int height){
		String[] temp = new String[6];
		temp[0] = "" + Ref;
		temp[1] = "" + X;
		temp[2] = "" + Y;
		temp[3] = "" + width;
		temp[4] = "" + height;
		TempTextureDisplayBucket.add(temp);
	}
	
	/**
	 * Adds a texture reference and coordinates to the bucket
	 * @param Ref Texture ID
	 * @param P a Point containing both x and y axis coordinates
	 */
	public void addTexture(int Ref, Point P, int width, int height){
		String[] temp = new String[6];
		temp[0] = "" + Ref;
		temp[1] = "" + P.x;
		temp[2] = "" + P.y;
		temp[3] = "" + width;
		temp[4] = "" + height;
		TempTextureDisplayBucket.add(temp);
	}
	
	/**
	 * Clears the bucket of all and any textures
	 */
	public void clearTextures(){
		TempTextureDisplayBucket.clear();
	}
	
	/**
	 * Sets the current textures = the loaded textures.
	 */
	public void setTextures(){
		flag = false;
		TextureDisplayBucket = TempTextureDisplayBucket;
		flag = true;
	}
	
	/**
	 * 
	 * @return all the contents of the bucket
	 */
	public ArrayList<String[]> getTextures(){
		return TextureDisplayBucket;
	}
}