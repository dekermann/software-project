package gui.gameInterface;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;

import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.GUI;
import de.matthiasmann.twl.ResizableFrame;
import de.matthiasmann.twl.Widget;
import de.matthiasmann.twl.renderer.lwjgl.LWJGLRenderer;
import de.matthiasmann.twl.theme.ThemeManager;

/*
 * H�r �r gui:en f�r interfacet f�r n�r man spelar.
 */

public class GameInterface extends Widget {
	/*
	 * Here the various global variables are initiated.
	 */
	public static LWJGLRenderer renderer = null;
	public static GUI gui = null;
	public static ThemeManager theme = null;
	static GameInterface gameUI = null;

	/**
	 * Constructor for GameInterface, where all the panels and funktions are
	 * created.
	 */
	public static ResizableFrame bag = null;
	Bag bagPanel = null;
	
	public static Chat chat = null;
	public static DialogLayout chatPanel = null;
	
	public static ResizableFrame inventory = null;
	Inventory inventoryPanel = null;
	
	Options options = null;
	public static DialogLayout optionsPanel = null;
	
	QuickBar quickBar = null;
	
	Skill skill = null;
	
	public GameInterface() {
		// Add your Interface Panels here!
		
		bagPanel = new Bag(10, 5);
		bag = new ResizableFrame();
		bag.setTitle("Bag");
		bag.setResizableAxis(ResizableFrame.ResizableAxis.NONE);
		bag.add(bagPanel);
		bag.setVisible(false);
		add(bag);

		chat = new Chat();
		chat.setVisible(false);
		add(chat);
		
		inventoryPanel = new Inventory(2,5);
		inventory = new ResizableFrame();
		inventory.setTitle("Inventory");
		inventory.setResizableAxis(ResizableFrame.ResizableAxis.NONE);
		inventory.add(inventoryPanel);
		inventory.setVisible(false);
		add(inventory);
		
		options = new Options();
		optionsPanel = Options.getFrame();
		add(optionsPanel);

		/*
		quickBar = new QuickBar();
		add(quickBar);
		
		skill = new Skill();
		add(skill);*/
	}
	
	protected void layout(){
		
		optionsPanel.adjustSize();
		optionsPanel.setPosition(
				getInnerX() + (getInnerWidth() - optionsPanel.getWidth()) / 2,
				getInnerY() + (getInnerHeight() - optionsPanel.getHeight()) / 2);

		inventory.adjustSize();
		inventory.setPosition(getInnerRight() - inventory.getInnerWidth(), 0);
		
		bag.adjustSize();
		bag.setPosition(getInnerRight() - bag.getInnerWidth(), getInnerBottom() - bag.getHeight());
		
		chat.setSize(getInnerWidth()/3, getInnerHeight()/5);
		chat.setPosition(0, getInnerBottom() + chat.getHeight());
	}

	/**
	 * Here the gui is initiated with all its various components.
	 * This will be called from gui.View. 
	 */
	public static void initGameInterface() {
		try {
			renderer = new LWJGLRenderer();
			gameUI = new GameInterface();
			gui = new GUI(gameUI, renderer);
			File file = new File("Media\\Game_State\\GameInterface\\gameinterface.xml");
			@SuppressWarnings("deprecation")
			URL url = file.toURL();
			theme = ThemeManager.createThemeManager(url, renderer);
			gui.applyTheme(theme);
		} catch (LWJGLException lwjglException) {
			showErrMsg(lwjglException);
		} catch (Exception ee) {
			showErrMsg(ee);
		}
	}

	/**
	 * Displays exception info on the screen, and on system console
	 * @param ex
	 */
	public static void showErrMsg(Throwable ex) {
		ex.printStackTrace();
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		pw.flush();
		Sys.alert("Error!", sw.toString());
	}
}