package gui.gameInterface;

import state.State_Manager;
import gui.View;
//import gui.loginInterface.LoginInterface;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.Widget;

public class Options extends Widget {

	static DialogLayout optionPanel = null;
	Button btnLogout = null;
	Button btnQuit = null;

	public Options() {
		optionPanel = new DialogLayout();
		optionPanel.setTheme("options");
		optionPanel.setVisible(false);
		
		
		btnLogout = new Button("Logout");
		btnLogout.addCallback(new Runnable() {
			public void run() {
				LogoutFunktion();
			}
		});

		btnQuit = new Button("Quit");
		btnQuit.addCallback(new Runnable() {
			public void run() {
				QuitFunktion();
			}
		});
		
		DialogLayout.Group hButton = optionPanel.createParallelGroup(btnLogout, btnQuit);

		optionPanel.setHorizontalGroup(optionPanel.createParallelGroup()
				.addGroup(optionPanel.createSequentialGroup(hButton)));

		optionPanel.setVerticalGroup(optionPanel.createSequentialGroup()
				.addGroup(optionPanel.createParallelGroup(btnLogout))
				.addGroup(optionPanel.createParallelGroup(btnQuit)));
	}

	void LogoutFunktion() {
		State_Manager.changeState("LOGIN_STATE");
	}

	void QuitFunktion() {
		View.running = false;
	}

	public static DialogLayout getFrame() {
		return optionPanel;

	}

}
