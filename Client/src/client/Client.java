package client;

import gui.View;
import gui.texture.TextureBucket;
import gui.texture.TextureDisplayBucket;
import input_output.network.Listener;
import input_output.network.Sender;

import state.State_Manager;

public class Client {

	private Listener L;
	public static Sender S;
	private State_Manager SM;
	private View V;
	private TextureBucket TB;
	private TextureDisplayBucket TDB;
	public static int userPort = 1;

	public Client(){
		init();
	}

	private void init(){
		S = new Sender();
		TB = new TextureBucket();
		TDB = new TextureDisplayBucket();
		V = new View(TB, TDB);
	}

	public void start(){
		V.start();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SM = new State_Manager(TB, TDB);
		L = new Listener(4321,4322,SM);
		L.start();
		while(true){
			if (SM.update()){
				S.sendMessage(SM.request(), userPort);
			}
		}
	}
}
