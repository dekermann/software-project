package state;

import java.util.HashMap;
import java.util.Map;
import state.State;
import state.states.Game_State;
import state.states.Loading_State;
import state.states.Login_State;
import gui.texture.TextureBucket;
import gui.texture.TextureDisplayBucket;

public class State_Manager{
	private static String currentState;
	static Map<String, State> states = new HashMap<String, State>();

	public static final String LOGIN = "LOGIN_STATE";
	public static final String LOADING = "LOADING_STATE";
	public static final String GAME = "GAME_STATE";

	public State login_state;
	public State game_state;
	public State loading_state;
	
	private String request;
	private TextureBucket TB;
	private TextureDisplayBucket TDB;
	
	
	public State_Manager(TextureBucket TB, TextureDisplayBucket TDB) {
		this.TB = TB;
		this.TDB = TDB;
		initStates();
		startState(login_state.getState());

	}
	
	/**
	 * Called in every loop to update the current state of the game.
	 */
	public boolean update() {
		if (!currentState.equals(states.get(currentState).changeState())) {
			changeState(states.get(currentState).changeState());
		}
		states.get(currentState).update();
		
		request = states.get(currentState).request();
		
		if(request != null){
			System.out.println("REQUEST:" + request);
			return true;
		}
		return false;
	}
	
	public void receive(byte[] message){
		states.get(currentState).receive(message);
	}
	
	public String request(){
		String temp = request;
		request = "";
		return temp;
	}
	
	/**
	 * initializes the different states of the game
	 */
	public void initStates() {
		login_state = new Login_State(TB, TDB);
		loading_state = new Loading_State(TB, TDB);
		game_state = new Game_State(TB, TDB, (Loading_State) loading_state);
		
		states.put(login_state.getState(), login_state);
		states.put(loading_state.getState(), loading_state);
		states.put(game_state.getState(), game_state);

	}

	/**
	 * Starts with this state
	 * @param state to be started with
	 */
	private void startState(String state) {
		System.out.println("Start state = " + state);
		
		currentState = state;
		states.get(currentState).enter();
	}

	/**
	 * Changes from the current state to this new state
	 * @param state to be changed to
	 */
	public static void changeState(String state) {
		if (!currentState.equals(state)) {
		System.out.println("Changing from: " + currentState + " to " + state);
		
		states.get(currentState).exit();
		
		currentState = state;
		states.get(currentState).enter();

		}
	}
}
