package state.states;

import java.awt.Point;

import org.lwjgl.opengl.Display;

import server.database.account.Character;
import database.Map;
import database.Monsterlist;
import database.Objects;

import gui.controller.InputBucket;
import gui.entities.EntityBucket;
import gui.gameInterface.GameInterface;
import gui.texture.TextureBucket;
import gui.texture.TextureDisplayBucket;
import state.states.Loading_State;
import state.State;
import state.State_Manager;

public class Game_State implements State {

	private String InputType;
	// private Point InputMousePos;
	private String request = null;
	private String savedRequest = null;
	private TextureBucket TB;
	private TextureDisplayBucket TDB;
	private String changeState = State_Manager.GAME;
	public static boolean chatFlag = false;
	private Character Char;
	private double oldPointX;
	private double oldPointY;
	private Loading_State LS;
	private double deltaXR = 0;
	private double deltaXL = 0;
	private double deltaYU = 0;
	private double deltaYD = 0;
	private byte movingLeft = 0;
	private byte movingUp = 0;
	private byte movingDown = 0;
	private byte movingRight = 0;
	private int move = 0;
	private boolean InputBlock;
	private int currentSelect = 0;
	private int currentEnemy = 0;
	private long time;
	private long time2;

	private boolean DEAD = false;

	private int ExpGained = 0;
	private int LevelUp = 0;
	private int counter = 0;

	public Game_State(TextureBucket TB, TextureDisplayBucket TDB,
			Loading_State loading_state) {
		this.TB = TB;
		this.TDB = TDB;
		this.LS = loading_state;
	}

	@Override
	public void enter() {
		this.Char = LS.getChar();
		oldPointX = Char.getCoordinates().x;
		oldPointY = Char.getCoordinates().y;
		TB.clearTextures();
		TDB.clearTextures();
		InputBucket.clear();
		initTiles();
		initEntities();
		initInterface();
		TextureBucket.flag = true;
		EntityBucket.flaga = true;
		GameInterface.gui.setEnabled(true);
		GameInterface.chat.setEnabled(true);
		GameInterface.chat.setVisible(true);
		time = System.currentTimeMillis();
	}

	private void initTiles() {
		for (int i = 0; i < 53; i++) {
			TB.addTexture("Media\\Game_State\\Tiles\\" + i + ".png");
		}
	}

	private void initEntities() {
		String A = "Media\\Game_State\\Players\\";
		EntityBucket.addEntity(new String[] { A + "1.PNG", A + "2.PNG",
				A + "3.PNG", A + "4.PNG", A + "5.PNG", A + "6.PNG",
				A + "7.PNG", A + "8.PNG", A + "9.PNG", A + "10.PNG",
				A + "11.PNG", A + "12.PNG", A + "13.PNG", A + "14.PNG",
				A + "15.PNG", A + "16.PNG", }, 16);
		A = "Media\\Game_State\\Monsters\\Zombie\\";
		EntityBucket.addEntity(new String[] { A + "1.PNG", A + "2.PNG",
				A + "3.PNG", A + "4.PNG", A + "5.PNG", A + "6.PNG",
				A + "7.PNG", A + "8.PNG", A + "9.PNG", A + "10.PNG",
				A + "11.PNG", A + "12.PNG", A + "13.PNG", A + "14.PNG",
				A + "15.PNG", A + "16.PNG", }, 16);
		A = "Media\\Game_State\\Monsters\\Elemental\\";
		EntityBucket.addEntity(new String[] { A + "1.PNG", A + "2.PNG",
				A + "3.PNG", A + "4.PNG", A + "5.PNG", A + "6.PNG",
				A + "7.PNG", A + "8.PNG", A + "9.PNG", A + "10.PNG",
				A + "11.PNG", A + "12.PNG", A + "13.PNG", A + "14.PNG",
				A + "15.PNG", A + "16.PNG", }, 16);
		A = "Media\\Game_State\\Monsters\\Flesh\\";
		EntityBucket.addEntity(new String[] { A + "1.PNG", A + "2.PNG",
				A + "3.PNG", A + "4.PNG", A + "5.PNG", A + "6.PNG",
				A + "7.PNG", A + "8.PNG", A + "9.PNG", A + "10.PNG",
				A + "11.PNG", A + "12.PNG", A + "13.PNG", A + "14.PNG",
				A + "15.PNG", A + "16.PNG", }, 16);
		A = "Media\\Game_State\\Monsters\\Crab\\";
		EntityBucket.addEntity(new String[] { A + "1.PNG", A + "2.PNG",
				A + "3.PNG", A + "4.PNG", A + "5.PNG", A + "6.PNG",
				A + "7.PNG", A + "8.PNG", A + "9.PNG", A + "10.PNG",
				A + "11.PNG", A + "12.PNG", A + "13.PNG", A + "14.PNG",
				A + "15.PNG", A + "16.PNG", }, 16);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Effects\\Blood1.PNG" , 
						"Media\\Game_State\\Effects\\Blood3.PNG", 
				"Media\\Game_State\\Effects\\Blood1.PNG"}, 3);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Extra\\Selected.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Extra\\SelectedEnemy.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Players\\Aydin.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Objects\\Buske2.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Objects\\Rock2.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Effects\\ExpGained.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Effects\\LevelUp.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Players\\deadaydin.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Monsters\\Zombie\\deadzombie.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Monsters\\Elemental\\deadelemental.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Monsters\\Flesh\\deadflesh.PNG" }, 0);
		EntityBucket.addEntity(
				new String[] { "Media\\Game_State\\Monsters\\Crab\\deadcrab.PNG" }, 0);

	}

	private void initInterface() {
		for (int i = 1; i < 11; i++) {
			EntityBucket.addEntity(
					new String[] { "Media\\Game_State\\Interface\\hp_bar\\" + i + ".PNG" }, 0);
		}
	}

	@Override
	public void update() {
		InputType = InputBucket.getInputType();
		if (!InputBlock && !DEAD) {
			recognizeInput();
		}
		TDB.clearTextures();
		if (currentEnemy != 0 && (System.currentTimeMillis()-time)>2000) {
			time = System.currentTimeMillis();
			if (Char.getCoordinates().x == Objects.getObject(currentEnemy)[1] + 1
					|| Char.getCoordinates().x == Objects.getObject(currentEnemy)[1] - 1 
					|| Char.getCoordinates().x == Objects.getObject(currentEnemy)[1]) {
				if (Char.getCoordinates().y == Objects.getObject(currentEnemy)[2] + 1
						|| Char.getCoordinates().y == Objects.getObject(currentEnemy)[2] - 1
						|| Char.getCoordinates().y == Objects.getObject(currentEnemy)[2]) {
					request = "5" + currentEnemy + "&";
				}
			}
		}
		try{
			render(Char.getCoordinates());
		}
		catch(Exception e){
			System.out.println("WE FAIL");
		}

	}

	private void recognizeInput() {
		if (InputType != null && (System.currentTimeMillis()-time2)>300) {
			time2 = System.currentTimeMillis();
			System.out.println(InputType);
			System.out.println(savedRequest);
			switch (InputType) {
			case "W":
				if (Map.checkPassage(Char.getCoordinates().x,
						Char.getCoordinates().y - 1)
						&& Objects.checkPass(new Point(Char.getCoordinates().x,
								Char.getCoordinates().y - 1))) {
					if (savedRequest == null) {
						request = "4" + (Char.getCoordinates().x) + "&"
								+ (Char.getCoordinates().y - 1) + "&";
						move = 2;
					}
				}
				break;
			case "D":
				if (Map.checkPassage(Char.getCoordinates().x + 1,
						Char.getCoordinates().y)
						&& Objects.checkPass(new Point(
								Char.getCoordinates().x + 1, Char
								.getCoordinates().y))) {
					if (savedRequest == null) {
						request = "4" + (Char.getCoordinates().x + 1) + "&"
								+ (Char.getCoordinates().y) + "&";
						move = 3;
					}
				}
				break;
			case "S":
				if (Map.checkPassage(Char.getCoordinates().x,
						Char.getCoordinates().y + 1)
						&& Objects.checkPass(new Point(Char.getCoordinates().x,
								Char.getCoordinates().y + 1))) {
					if (savedRequest == null) {
						request = "4" + (Char.getCoordinates().x) + "&"
								+ (Char.getCoordinates().y + 1) + "&";
						move = 1;
					}
				}
				break;
			case "A":
				if (Map.checkPassage(Char.getCoordinates().x - 1,
						Char.getCoordinates().y)
						&& Objects.checkPass(new Point(
								Char.getCoordinates().x - 1, Char
								.getCoordinates().y))) {
					if (savedRequest == null) {
						request = "4" + (Char.getCoordinates().x - 1) + "&"
								+ (Char.getCoordinates().y) + "&";
						move = 4;
					}
				}
				break;
			case "WD":
				if (Map.checkPassage(Char.getCoordinates().x + 1,
						Char.getCoordinates().y - 1)
						&& Objects.checkPass(new Point(
								Char.getCoordinates().x + 1, Char
								.getCoordinates().y - 1))) {
					if (savedRequest == null) {
						request = "4" + (Char.getCoordinates().x + 1) + "&"
								+ (Char.getCoordinates().y - 1) + "&";
					}
				}
				break;
			case "WA":
				if (Map.checkPassage(Char.getCoordinates().x - 1,
						Char.getCoordinates().y - 1)
						&& Objects.checkPass(new Point(
								Char.getCoordinates().x - 1, Char
								.getCoordinates().y - 1))) {
					if (savedRequest == null) {
						request = "4" + (Char.getCoordinates().x - 1) + "&"
								+ (Char.getCoordinates().y - 1) + "&";
					}
				}

				break;
			case "SA":
				if (Map.checkPassage(Char.getCoordinates().x - 1,Char.getCoordinates().y + 1) && Objects.checkPass(new Point(Char.getCoordinates().x - 1, Char.getCoordinates().y + 1))) {
					if (savedRequest == null) {
						request = "4" + (Char.getCoordinates().x - 1) + "&"	+ (Char.getCoordinates().y + 1) + "&";
					}
				}
				break;
			case "SD":
				if (Map.checkPassage(Char.getCoordinates().x + 1,Char.getCoordinates().y + 1) && Objects.checkPass(new Point(Char.getCoordinates().x + 1, Char.getCoordinates().y + 1))) {
					if (savedRequest == null) {
						request = "4" + (Char.getCoordinates().x + 1) + "&"	+ (Char.getCoordinates().y + 1) + "&";
					}
				}
				break;

			case "M0":
				for (int y = 0; y < 15; y++) {
					for (int x = 0; x < 21; x++) {
						if (InputBucket.getMousePoint().y > (Display.getHeight() / 15) * y	&& InputBucket.getMousePoint().y < (Display.getHeight() / 15) * (y + 1)) {
							if (InputBucket.getMousePoint().x > (Display.getWidth() / 21) * x && InputBucket.getMousePoint().x < (Display.getWidth() / 21) * (x + 1)) {
								if (Objects.getObject(new Point((Char.getCoordinates().x - 10 + x), (Char.getCoordinates().y - 7 + y))) != null) {
									currentSelect = Objects.getObject(new Point((Char.getCoordinates().x - 10 + x),(Char.getCoordinates().y - 7 + y)));
									//									System.out.println("Selected: "	+ currentSelect);
								}
								else{
									currentSelect = 0;
								}
							}
						}
					}
				}
				break;
			case "M1":
				for (int y = 0; y < 15; y++) {
					for (int x = 0; x < 21; x++) {
						if (InputBucket.getMousePoint().y > (Display.getHeight() / 15) * y	&& InputBucket.getMousePoint().y < (Display.getHeight() / 15) * (y + 1)) {
							if (InputBucket.getMousePoint().x > (Display.getWidth() / 21) * x	&& InputBucket.getMousePoint().x < (Display.getWidth() / 21) * (x + 1)) {
								if (Objects.getObject(new Point((Char.getCoordinates().x - 10 + x), (Char.getCoordinates().y - 7 + y))) != null) {
									if (Objects.getObject(new Point((Char.getCoordinates().x - 10 + x),(Char.getCoordinates().y - 7 + y))) < 0) {

									} else if (Objects.getObject(new Point((Char.getCoordinates().x - 10 + x),(Char.getCoordinates().y - 7 + y))) < 1000) {

									} else {
										if(Objects.getObject(Objects.getObject(new Point((Char.getCoordinates().x - 10 + x),(Char.getCoordinates().y - 7 + y))))[3] <= 0){
											request = "8" + Objects.getObject(new Point((Char.getCoordinates().x - 10 + x),(Char.getCoordinates().y - 7 + y))) + "&";
										}
										else{
											currentEnemy = Objects.getObject(new Point((Char.getCoordinates().x - 10 + x),(Char.getCoordinates().y - 7 + y)));
											currentSelect = currentEnemy;
										}
									}
								}
							}
						}
					}
				}
				break;
			case "SPACE":
				if (currentSelect != 0 && Objects.getObject(currentSelect)[3] > 0) {
					if (currentEnemy == 0) {
						currentEnemy = currentSelect;
					} else {
						currentEnemy = 0;
					}
				}
				break;
			case "C":
				if (GameInterface.chat.isVisible())
					GameInterface.chat.setVisible(false);
				else
					GameInterface.chat.setVisible(true);
				break;
			case "B":
				if (GameInterface.bag.isVisible())
					GameInterface.bag.setVisible(false);
				else
					GameInterface.bag.setVisible(true);
				break;
			case "I":
				if (GameInterface.inventory.isVisible())
					GameInterface.bag.setVisible(false);
				else
					GameInterface.bag.setVisible(true);
				break;
			case "ESCAPE":
				if (GameInterface.optionsPanel.isVisible())
					GameInterface.optionsPanel.setVisible(false);
				else
					GameInterface.optionsPanel.setVisible(true);
				break;

			}
			InputBucket.clear();
		}
	}

	private void move() {
		String temp = savedRequest.substring(1, savedRequest.length());
		savedRequest = null;
		String[] temp2 = temp.split("&");
		oldPointX = Char.getCoordinates().x;
		oldPointY = Char.getCoordinates().y;
		Char.setCoordinates(new Point(Integer.parseInt(temp2[0]), Integer
				.parseInt(temp2[1])));
		InputBlock = true;

	}

	@Override
	public void receive(byte[] message) {
		switch ((char) message[0]) {
		case '1':
			// userRequestAccepted
			switch ((char) message[1]) {
			case '0':
				// COMMANDS
				break;
			case '1':
				// CREATE CHAR
				break;
			case '2':
				// CHOOSE CHAR
				break;
			case '3':
				// DELETE CHAR
				break;
			case '4':
				if (message[2] == ('K')) {
					move();
				} else {
					// DO NOTHING >_>
				}
				break;
			case '5':
				// BASIC ATTACK
				break;
			case '6':
				if (message[2] == ('K')) {
					Char.addHP(1);
				} else {
					// DO NOTHING >_>
				}
				break;
			case '7':
				// SELFCAST
				break;
			case '8':
				if (message[2] == ('K')) {
					savedRequest = null;
				} else {
					// DO NOTHING >_>
				}
				break;
			case '9':
				// CHAT
				break;
			}
			break;
		case '2':
			// ObjectBroadcast ETT OBJECT P� RUTAN
			String stringedMessage = "";
			for (int i = 1; i < message.length; i++) {
				stringedMessage += (char) message[i];
			}
			//			System.out.println(stringedMessage);
			String[] splitMessage = stringedMessage.split("&");

			if(Integer.parseInt(splitMessage[1]) < 0){
				currentSelect = 0;
				currentEnemy = 0;
			}

			//			if(Objects.getObject(Integer.parseInt(splitMessage[0])) != null){
			//				if((new Point(Objects.getObject(Integer.parseInt(splitMessage[0]))[1],Objects.getObject(Integer.parseInt(splitMessage[0]))[2])) == (new Point(Integer.parseInt(splitMessage[2]), Integer.parseInt(splitMessage[3])))){
			Objects.addObject(Integer.parseInt(splitMessage[0]),Integer.parseInt(splitMessage[1]),new Point(Integer.parseInt(splitMessage[2]), Integer.parseInt(splitMessage[3])));
			//				}
			//			}

			break;
		case '3':
			//			System.out.println((char) message[1]);
			if ((char) message[1] == '0') {
				String stringedMessage2 = "";
				for (int i = 2; i < message.length; i++) {
					stringedMessage2 += (char) message[i];
				}
				String[] splitMessage2 = stringedMessage2.split("&");
				System.out.println("YOU ARE TAKING DAMAGE: " + Integer.parseInt(splitMessage2[0]));
				if (Char.damage(Integer.parseInt(splitMessage2[0]))) {
					//TODO DEAD
					DEAD = true;
					System.out.println("DEAD");
				}
			} else if ((char) message[1] == '-') {
				String stringedMessage2 = "";
				for (int i = 1; i < message.length; i++) {
					stringedMessage2 += (char) message[i];
				}
				String[] splitMessage2 = stringedMessage2.split("&");
				int x = Objects.getObject(Integer.parseInt(splitMessage2[0]))[1]-(Char.getCoordinates().x-10);
				int y = Objects.getObject(Integer.parseInt(splitMessage2[0]))[2]-(Char.getCoordinates().y-7);
				EntityBucket.addDynamicDisplayEntity1(0, new Point(
						(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
						(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)),
						Display.getWidth()/21, Display.getHeight()/15);
			} else {
				String stringedMessage2 = "";
				for (int i = 1; i < message.length; i++) {
					stringedMessage2 += (char) message[i];
				}
				String[] splitMessage2 = stringedMessage2.split("&");

				int x = Objects.getObject(Integer.parseInt(splitMessage2[0]))[1]-(Char.getCoordinates().x-10);
				int y = Objects.getObject(Integer.parseInt(splitMessage2[0]))[2]-(Char.getCoordinates().y-7);
				EntityBucket.addDynamicDisplayEntity1(0, new Point(
						(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
						(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)),
						Display.getWidth()/21, Display.getHeight()/15);

				Integer[] temp = Objects.getObject(Integer.parseInt(splitMessage2[0]));

				Objects.addObject(Integer.parseInt(splitMessage2[0]),temp[0] - Integer.parseInt(splitMessage2[1]),new Point(temp[1],temp[2]));
			}
			break;
		case '4':
			// OTHER
			if ((char) message[1] == '0') {
				String stringedMessage3 = "";
				for (int i = 1; i < message.length; i++) {
					stringedMessage3 += (char) message[i];
				}
				String[] splitMessage3 = stringedMessage3.split("&");

				if(Char.addExperience(Integer.parseInt(splitMessage3[0]))){
					LevelUp = 1;
				}
				else{
					ExpGained = 1;
				}

			}
			break;
		}

	}

	@Override
	public String request() {
		String temp = request;
		if (request != null) {
			if(!((request.substring(0, 1).equals("5")) || (request.substring(0, 1).equals("6")) || (request.substring(0, 1).equals("8")))){
				savedRequest = request;
			}
		}
		request = null;
		return temp;
	}

	private void render(Point P) {

		// KOLLA H�R OM P �R I BUFFERT ZONEN
		if (Map.enteredBuffertZone(P)) {
			// HAR G�TT IN I BUFFERT ZONEN SKAPA NY TR�D OCH LADDA OM FIELD
			new Thread() {
				public void run() {
					// LADDA NY KARTA I EGEN TR�D SEN D�DA TR�DEN
				}
			}.run();
		}

		/*
		 * J�vligt lustigt fel som kom h�r! Eftersom att datorns tal �r
		 * representerade i bin�ra tal s� kan den inte representera 0.2, 0.3
		 * eftersom de i bin�ra talsystemet �r icke rationella! Det resulterar i
		 * att den f�rs�ker ta ett tal som �r j�vligt n�ra typ
		 * 1.20000000000000000000000000001 vad som h�nder d� �r att oldPoint
		 * aldrig blir exakt som P! Vilket g�r att den inte kommer att finna
		 * n�got stabilt l�ge och "vaggar" d�rmed fram och tillbaks.
		 */

		if (oldPointX < P.x) {
			// RIGHT
			deltaXR += 20;
			oldPointX += 0.00001;
			movingRight = 1;
			// System.out.println(deltaXR);
			if (deltaXR == 100) {
				oldPointX = P.x;
				deltaXR = 0;
				movingRight = 0;
				InputBlock = false;
			}
		} else if (oldPointX > P.x) {
			// LEFT
			deltaXL -= 20;
			oldPointX -= 0.00001;
			movingLeft = 1;
			// System.out.println(deltaXL);
			if (deltaXL == -100) {
				oldPointX = P.x;
				deltaXL = 0;
				movingLeft = 0;
				InputBlock = false;
			}
		}
		if (oldPointY < P.y) {
			// DOWN
			deltaYD += 20;
			oldPointY += 0.00001;
			movingDown = 1;
			// System.out.println(deltaYD);
			if (deltaYD == 100) {
				oldPointY = P.y;
				deltaYD = 0;
				movingDown = 0;
				InputBlock = false;
			}
		} else if (oldPointY > P.y) {
			// UP
			deltaYU -= 20;
			oldPointY -= 0.00001;
			movingUp = 1;
			// System.out.println(deltaYU);
			if (deltaYU == -100) {
				oldPointY = P.y;
				deltaYU = 0;
				movingUp = 0;
				InputBlock = false;
			}
		}

		if(movingUp == 0 && movingDown == 0 && movingLeft == 0 && movingRight == 0 && request == null && savedRequest == null && !DEAD){
			if(counter == 10){
				request = "6";
				counter = 0;
			}
			counter++;
		}


		short[][] currentVision = Map.getVision(oldPointX, oldPointY);

		// for(int y = 0;y<currentVision.length;y++){
		// for(int x = 0;x<currentVision[y].length;x++){
		// System.out.print(currentVision[y][x] + " ");
		// }
		// System.out.println();
		// }
		// System.out.println("-----------");
		// System.out.println("Moving: " + oldPointX + ", " + oldPointY);

		for (int y = 0; y < currentVision.length; y++) {
			for (int x = 0; x < currentVision[y].length; x++) {
				TDB.addTexture(
						currentVision[y][x],
						(int) (Display.getWidth() / 21 * x - ((deltaXR / 100) * Display.getWidth() / 21) - ((deltaXL / 100)	* Display.getWidth() / 21))	- ((Display.getWidth() / 21) * movingLeft),
						(int) (Display.getHeight() / 15 * y	- ((deltaYD / 100) * Display.getHeight() / 15) - ((deltaYU / 100) * Display.getHeight() / 15)) - ((Display.getHeight() / 15) * movingUp),
														Display.getWidth() / 21, Display.getHeight() / 15);
			}
		}

		Point tempP;
		for (int y = 0; y < currentVision.length; y++) {
			for (int x = 0; x < currentVision[y].length; x++) {
				tempP = new Point((P.x - 10 + x), (P.y - 7) + y);
				if (Objects.getObject(tempP) != null) {
					if (Objects.getObject(tempP) < 0) {
						if(Objects.getObject(Objects.getObject(tempP))[0] <= 0){
							if(Objects.getObject(tempP) == currentSelect){
								EntityBucket.addStaticDisplayEntity(0, new Point(
										(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
										(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
										Display.getWidth()/21, Display.getHeight()/15);
							}
							if(Objects.getObject(tempP) == currentEnemy){
								currentEnemy = 0;
							}
							EntityBucket.addStaticDisplayEntity(6, new Point(
									(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
									(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
									Display.getWidth()/21, Display.getHeight()/15);
						}
						else{
							switch (Objects.getObject(Objects.getObject(tempP))[3]) {
							case 0:
								//NO MOVEMENT
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
											Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
											Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(0, new Point(
										(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
										(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
										Display.getWidth()/21, Display.getHeight()/15, 0);
								break;
							case 1:
								//UP LEFT
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
															Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
															Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(0, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
												+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
														Display.getWidth()/21, Display.getHeight()/15, 3);
								break;
							case 2:
								//UP RIGHT
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
															Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
															Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(0, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
												- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
														Display.getWidth()/21, Display.getHeight()/15, 2);
								break;
							case 3:
								//UP
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(0, new Point(
										(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
										(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
												+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
												Display.getWidth()/21, Display.getHeight()/15, 1);
								break;
							case 4:
								//DOWN LEFT
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
															Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
															Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(0, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
												+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
														Display.getWidth()/21, Display.getHeight()/15, 3);
								break;
							case 5:
								//DOWN RIGHT
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
															Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
															Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(0, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
												- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
														Display.getWidth()/21, Display.getHeight()/15, 2);
								break;
							case 6:
								//DOWN
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
													Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
													Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(0, new Point(
										(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
										(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
												- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
												Display.getWidth()/21, Display.getHeight()/15, 0);
								break;
							case 7:
								//LEFT
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(0, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
												+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
												Display.getWidth()/21, Display.getHeight()/15, 3);
								break;
							case 8:
								//RIGHT
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(0, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
												- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
												Display.getWidth()/21, Display.getHeight()/15, 2);
								break;
								// WHY SHOULD STUFF MAKE SENSE?????? FUCK LOGIC
							}
						}
					}
					else if(Objects.getObject(tempP) < 1000){
						EntityBucket.addStaticDisplayEntity(Objects.getObject(tempP), new Point(
								(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
								(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)),
								Display.getWidth()/21, Display.getHeight()/15);
					}
					else{
						int monsterType = Monsterlist.getMonsterType(Objects.getObject(tempP));
						if(Objects.getObject(Objects.getObject(tempP))[0] <= 0){
							if(Objects.getObject(tempP) == currentSelect){
								EntityBucket.addStaticDisplayEntity(0, new Point(
										(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
										(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
										Display.getWidth()/21, Display.getHeight()/15);
							}
							if(Objects.getObject(tempP) == currentEnemy){
								currentEnemy = 0;
							}
							EntityBucket.addStaticDisplayEntity(7+monsterType, new Point(
									(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
									(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
									Display.getWidth()/21, Display.getHeight()/15);
						}
						else{
							switch (Objects.getObject(Objects.getObject(tempP))[3]) {
							case 0:
								//NO MOVEMENT
								for(int i = 10;i>0;i--){
									if(((double) Objects.getObject(Objects.getObject(tempP))[0])*10/(double) Monsterlist.getMonsterHP(monsterType) > (i-1)){
										EntityBucket.addStaticDisplayEntity(11+i, new Point(
												(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
												(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)-Display.getHeight()/40),
												Display.getWidth() / 21,
												Display.getHeight() / 60);
										break;
									}
								}
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
											Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
											Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(monsterType, new Point(
										(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
										(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
										Display.getWidth()/21, Display.getHeight()/15, 0);
								break;
							case 1:
								//UP LEFT
								for(int i = 10;i>0;i--){
									if(((double) Objects.getObject(Objects.getObject(tempP))[0])*10/(double) Monsterlist.getMonsterHP(monsterType) > (i-1)){
										EntityBucket.addStaticDisplayEntity(11+i, new Point(
												(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
														+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
														(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
																+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)-Display.getHeight()/40),
																Display.getWidth() / 21,
																Display.getHeight() / 60);
										break;
									}
								}
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
															Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
															Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(monsterType, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
												+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
														Display.getWidth()/21, Display.getHeight()/15, 3);
								break;
							case 2:
								//UP RIGHT
								for(int i = 10;i>0;i--){
									if(((double) Objects.getObject(Objects.getObject(tempP))[0])*10/(double) Monsterlist.getMonsterHP(monsterType) > (i-1)){
										EntityBucket.addStaticDisplayEntity(11+i, new Point(
												(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
														- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
														(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
																+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)-Display.getHeight()/40),
																Display.getWidth() / 21,
																Display.getHeight() / 60);
										break;
									}
								}
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
															Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
															Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(monsterType, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
												- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
														Display.getWidth()/21, Display.getHeight()/15, 2);
								break;
							case 3:
								//UP
								for(int i = 10;i>0;i--){
									if(((double) Objects.getObject(Objects.getObject(tempP))[0])*10/(double) Monsterlist.getMonsterHP(monsterType) > (i-1)){
										EntityBucket.addStaticDisplayEntity(11+i, new Point(
												(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)-Display.getHeight()/40),
														Display.getWidth() / 21,
														Display.getHeight() / 60);
										break;
									}
								}
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(monsterType, new Point(
										(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
										(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
												+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)), 
												Display.getWidth()/21, Display.getHeight()/15, 1);
								break;
							case 4:
								//DOWN LEFT
								for(int i = 10;i>0;i--){
									if(((double) Objects.getObject(Objects.getObject(tempP))[0])*10/(double) Monsterlist.getMonsterHP(monsterType) > (i-1)){
										EntityBucket.addStaticDisplayEntity(11+i, new Point(
												(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
														+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
														(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
																- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)-Display.getHeight()/40),
																Display.getWidth() / 21,
																Display.getHeight() / 60);
										break;
									}
								}
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
															Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
															Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(monsterType, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
												+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
														Display.getWidth()/21, Display.getHeight()/15, 3);
								break;
							case 5:
								//DOWN RIGHT
								for(int i = 10;i>0;i--){
									if(((double) Objects.getObject(Objects.getObject(tempP))[0])*10/(double) Monsterlist.getMonsterHP(monsterType) > (i-1)){
										EntityBucket.addStaticDisplayEntity(11+i, new Point(
												(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
														- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
														(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
																- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)-Display.getHeight()/40),
																Display.getWidth() / 21,
																Display.getHeight() / 60);
										break;
									}
								}
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
															Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
															- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
															Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(monsterType, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
												- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
														Display.getWidth()/21, Display.getHeight()/15, 2);
								break;
							case 6:
								//DOWN
								for(int i = 10;i>0;i--){
									if(((double) Objects.getObject(Objects.getObject(tempP))[0])*10/(double) Monsterlist.getMonsterHP(monsterType) > (i-1)){
										EntityBucket.addStaticDisplayEntity(11+i, new Point(
												(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
												(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
														- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)-Display.getHeight()/40),
														Display.getWidth() / 21,
														Display.getHeight() / 60);
										break;
									}
								}
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
													Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
											(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
													Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(monsterType, new Point(
										(int)(Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight),
										(int)(((Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown))
												- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getHeight()/15)),
												Display.getWidth()/21, Display.getHeight()/15, 0);
								break;
							case 7:
								//LEFT
								for(int i = 10;i>0;i--){
									if(((double) Objects.getObject(Objects.getObject(tempP))[0])*10/(double) Monsterlist.getMonsterHP(monsterType) > (i-1)){
										EntityBucket.addStaticDisplayEntity(11+i, new Point(
												(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
														+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
														(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)-Display.getHeight()/40),
														Display.getWidth() / 21,
														Display.getHeight() / 60);
										break;
									}
								}
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
													+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(monsterType, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight)
												+ (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
												Display.getWidth()/21, Display.getHeight()/15, 3);
								break;
							case 8:
								//RIGHT
								for(int i = 10;i>0;i--){
									if(((double) Objects.getObject(Objects.getObject(tempP))[0])*10/(double) Monsterlist.getMonsterHP(monsterType) > (i-1)){
										EntityBucket.addStaticDisplayEntity(11+i, new Point(
												(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
														- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
														(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)-Display.getHeight()/40),
														Display.getWidth() / 21,
														Display.getHeight() / 60);
										break;
									}
								}
								if(Objects.getObject(tempP) == currentSelect){
									EntityBucket.addStaticDisplayEntity(0, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								if(Objects.getObject(tempP) == currentEnemy){
									EntityBucket.addStaticDisplayEntity(1, new Point(
											(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
													- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
													(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
													Display.getWidth()/21, Display.getHeight()/15);
								}
								EntityBucket.addDynamicDisplayEntity2(monsterType, new Point(
										(int)(((Display.getWidth()/21*x-((deltaXR/100)*Display.getWidth()/21)-((deltaXL/100)*Display.getWidth()/21))-((Display.getWidth()/21)*movingLeft)+((Display.getWidth()/21)*movingRight) 
												- (((double)(100-Objects.getMove(Objects.getObject(tempP))))/100)*Display.getWidth()/21)),
												(int)(Display.getHeight()/15*y-((deltaYD/100)*Display.getHeight()/15)-((deltaYU/100)*Display.getHeight()/15))-((Display.getHeight()/15)*movingUp)+((Display.getHeight()/15)*movingDown)), 
												Display.getWidth()/21, Display.getHeight()/15, 2);
								break;
								// WHY SHOULD STUFF MAKE SENSE?????? FUCK LOGIC
							}
						}
					}
				}
			}
		}
		for(int i = 10;i>0;i--){
			if((double) Char.getHP()/(double) Char.getMaxHP()*10 > (i-1)){
				EntityBucket.addStaticDisplayEntity(11+i, new Point(Display.getWidth()
						/ 2 - Display.getWidth() / 42, Display.getHeight() / 2
						- Display.getHeight() / 27), Display.getWidth() / 21,
						Display.getHeight() / 60);
				break;
			}
		}
		if(DEAD){
			EntityBucket.addStaticDisplayEntity(7, new Point(Display.getWidth()
					/ 2 - Display.getWidth() / 42, Display.getHeight() / 2
					- Display.getHeight() / 30), Display.getWidth() / 21,
					Display.getHeight() / 15);
		}
		else{
			switch (move) {
			case 0:
				// System.out.println("STILL");
				EntityBucket.addStaticDisplayEntity(2, new Point(Display.getWidth()
						/ 2 - Display.getWidth() / 42, Display.getHeight() / 2
						- Display.getHeight() / 30), Display.getWidth() / 21,
						Display.getHeight() / 15);
				break;
			case 1:
				// System.out.println("UP");
				EntityBucket
				.addDynamicDisplayEntity2(0, new Point(Display.getWidth()
						/ 2 - Display.getWidth() / 42, Display.getHeight()
						/ 2 - Display.getHeight() / 30),
						Display.getWidth() / 21, Display.getHeight() / 15,
						0);
				break;
			case 2:
				// System.out.println("DOWN");
				EntityBucket
				.addDynamicDisplayEntity2(0, new Point(Display.getWidth()
						/ 2 - Display.getWidth() / 42, Display.getHeight()
						/ 2 - Display.getHeight() / 30),
						Display.getWidth() / 21, Display.getHeight() / 15,
						1);
				break;
			case 3:
				// System.out.println("RIGHT");
				EntityBucket
				.addDynamicDisplayEntity2(0, new Point(Display.getWidth()
						/ 2 - Display.getWidth() / 42, Display.getHeight()
						/ 2 - Display.getHeight() / 30),
						Display.getWidth() / 21, Display.getHeight() / 15,
						2);
				break;
			case 4:
				// System.out.println("LEFT");
				EntityBucket
				.addDynamicDisplayEntity2(0, new Point(Display.getWidth()
						/ 2 - Display.getWidth() / 42, Display.getHeight()
						/ 2 - Display.getHeight() / 30),
						Display.getWidth() / 21, Display.getHeight() / 15,
						3);
				break;
			}
		}
		if(ExpGained > 0){
			EntityBucket.addStaticDisplayEntity(5, new Point(Display.getWidth()
					/ 2 - Display.getWidth() / 21, Display.getHeight() / 2
					- Display.getHeight() / 10), Display.getWidth() / 10,
					Display.getHeight() / 15);
			ExpGained++;
			if(ExpGained == 20){
				ExpGained = 0;
			}
		}
		if(LevelUp > 0){
			EntityBucket.addStaticDisplayEntity(6, new Point(Display.getWidth()
					/ 2 - Display.getWidth() / 21, Display.getHeight() / 2
					- Display.getHeight() / 10), Display.getWidth() / 10,
					Display.getHeight() / 15);
			LevelUp++;
			if(LevelUp == 20){
				LevelUp = 0;
			}
		}

		try {
			Thread.sleep(100);
			EntityBucket.setEntities();
			TDB.setTextures();
			TextureDisplayBucket.flag = true;
			EntityBucket.flag1b = true;
			EntityBucket.flag2b = true;
			EntityBucket.flag3b = true;
		} catch (InterruptedException e) {

		}
	}

	@Override
	public void exit() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getState() {
		return State_Manager.GAME;
	}

	@Override
	public String changeState() {
		return changeState;
	}
}
