package state.states;

import gui.texture.TextureBucket;
import gui.texture.TextureDisplayBucket;
import input_output.network.Downloader;

import org.lwjgl.opengl.Display;

import client.Client;

import server.database.account.Character;
import state.State;
import state.State_Manager;
import database.Map;

public class Loading_State implements State{
	
	private String request = null;
	private boolean doneLoading = false;
	private Character Char = null;
	private String changeState = State_Manager.LOADING;
	private TextureBucket TB;
	private TextureDisplayBucket TDB;
	private Downloader D;
	
	public Loading_State(TextureBucket TB, TextureDisplayBucket TDB){
		this.TB = TB;
		this.TDB = TDB;
	}

	@Override
	public void enter() {
		TB.addTexture("Media\\Loading_State\\Loading.PNG");
		TextureBucket.flag = true;
		D = new Downloader(1001, 1002, this);
		D.setObject("Character");
		D.start();
	}

	@Override
	public void update() {
		request = "00&" + D.getPort() + "&";
		// why print this? well fuck you I print what I want!
		System.out.println("PRIVATE PORT: " + Client.userPort);
		if(Char != null){
			Map.loadZone(Char.getCoordinates().x,Char.getCoordinates().y,100,100,"Database\\Map\\tileMap.map");
			changeState = State_Manager.GAME;
			request = "01";
		}
		render();
	}
	
	private void render() {
		TDB.clearTextures();
		TDB.addTexture(0,0,0,Display.getWidth(),Display.getHeight());
		try {
			TDB.setTextures();
			Thread.sleep(200);
			TextureDisplayBucket.flag = true;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void receive(byte[] message) {
		String temp = "";
		for(int i = 0;i<message.length;i++){
			temp += (char) message[i];
		}
		System.out.println(temp);
		
	}

	@Override
	public String request() {
		String temp = request;
		request = null;
		return temp;
	}

	@Override
	public void exit() {
		TB.clearTextures();
		TDB.clearTextures();
		//Upon exit this state will kill the Downloader it created and set it to null,
		D.destroy();
		D = null;
	}

	@Override
	public String getState() {
		return State_Manager.LOADING;
	}

	@Override
	public String changeState() {
		if (doneLoading){
			changeState = State_Manager.GAME;
		}
		return changeState;
	}
	
	public Character getChar(){
		return Char;
		
	}
	
	public void setChar(Character Char){
		this.Char = Char;
		System.out.println(Char.getName());
		System.out.println(Char.getCoordinates());
//		D.unpause("Map");
	}
}
