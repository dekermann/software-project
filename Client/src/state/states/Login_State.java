package state.states;

import gui.controller.InputBucket;
import gui.entities.EntityBucket;
import gui.gameInterface.GameInterface;
import gui.texture.TextureBucket;
import gui.texture.TextureDisplayBucket;

import java.awt.Point;
import java.util.ArrayList;

import org.lwjgl.opengl.Display;

import client.Client;

import state.State;
import state.State_Manager;

public class Login_State implements State {

	private static String InputType;
	public static String request = null;
	private static TextureBucket TB;
	private static TextureDisplayBucket TDB;
	private String changeState = State_Manager.LOGIN;
	public static int SelectionState = 0;
	public static String[] Characters;

	/**
	 * 
	 * @param TB
	 *            the textureBucket that is to be used by the Login_State when
	 *            the object is constructed
	 * @param TDB
	 *            a bucket of textures that is to be used for quick reference
	 *            for the render
	 */
	public Login_State(TextureBucket TB, TextureDisplayBucket TDB) {
		Login_State.TB = TB;
		Login_State.TDB = TDB;
	}
	
	public static ArrayList<String> getCharacters(){
		ArrayList <String> CharactersArray = new ArrayList<String>(Characters.length);
		for(int i = 0; i < Characters.length; i++)
			CharactersArray.add((String) Characters[i]);
		return CharactersArray;
	}

	/**
	 * this runs when the "State" is changed into the 'login state'. It sets the
	 * background to game.png and also slaps on a login.png texture
	 */
	@Override
	public void enter() {
		/*EntityBucket.addEntity(new String[] {
				"Media\\Game_State\\Players\\aydin.PNG",
				"Media\\Game_State\\Players\\aydin_2.PNG",
				"Media\\Game_State\\Players\\aydin_3.PNG" }, 3);*/
		TB.addTexture("Media\\Login_State\\Game.PNG");
		TB.addTexture("Media\\Login_State\\Login.PNG");
		TextureBucket.flag = true;
		EntityBucket.flaga = true;
	}

	/**
	 * The update function updates the State and checks if one tries to login or
	 * not. For the moment, this is used to continue to the game state.
	 */
	@Override
	public void update() {
		InputType = InputBucket.getInputType();
		if (InputType != null) {
			if (InputType.equals("M0") || InputType.equals("M1")
					|| InputType.equals("M2")) {
				if (SelectionState == 0) {
					
				} else if (SelectionState == 1) {
					
				} else {
					changeState = State_Manager.LOADING;
				}
			}
		}
		render();
	}

	/**
	 * this method is run whenever a message is received. it prints a message to
	 * the console that a message was recieved
	 * 
	 * @param message
	 */
	@Override
	public void receive(byte[] message) {
		if (SelectionState == 0) {
			String temp = "";
			for (int i = 0; i < message.length; i++) {
				if (((char) message[i]) == ('?')) {
					break;
				}
				temp += (char) message[i];
			}
			System.out.println(temp);
			String[] splittemp = temp.split("&");

			Client.userPort = Integer.parseInt(splittemp[0]);

			Characters = new String[splittemp.length - 1];
			for (int i = 0; i < splittemp.length - 1; i++) {
				Characters[i] = splittemp[i + 1];
				System.out.println("CHAR: " + splittemp[i + 1]);
			}
			System.out.println("PRIAVTE PORT: " + Client.userPort);
			SelectionState = 1;
		} else if (SelectionState == 1) {
			String temp = "";
			for (int i = 0; i < 3; i++) {
				temp += (char) message[i];
			}
			if (temp.equals("12K")) {
				SelectionState = 2;
			}
		}
	}

	/**
	 * returns the current request from the Login_State and sets the temp String
	 * to the current Request
	 */
	@Override
	public String request() {
		String temp = request;
		request = null;
		return temp;
	}

	/**
	 * The render method renders the current textureBucket and then puts the
	 * thread to sleep for 200 milliseconds (?)
	 */
	public static void render() {
		TDB.clearTextures();
		TDB.addTexture(0, 0, 0, Display.getWidth(), Display.getHeight());
		if (SelectionState == 0) {
			TDB.addTexture(1, Display.getWidth() / 2 - 25,
					Display.getHeight() / 2 - 40, 50, 80);
		} else {
			for (int i = 0; i < Characters.length; i++) {
				EntityBucket.addDynamicDisplayEntity1(0,
						new Point(Display.getWidth() - 80, i * 80), 80, 80);
			}
		}
		try {
			Thread.sleep(200);
			EntityBucket.setEntities();
			TDB.setTextures();
			TextureDisplayBucket.flag = true;
			EntityBucket.flag2b = true;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * when the exiting the current state (Login_State), clear all the textures
	 * of the Login_State
	 */
	public void exit() {
		TB.clearTextures();
		TDB.clearTextures();
		EntityBucket.clear();
	}

	/**
	 * return the state
	 */
	@Override
	public String getState() {
		return State_Manager.LOGIN;
	}

	/**
	 * when changing the state, this method returns the current state so that
	 * the state_manager knows about the state change
	 */
	@Override
	public String changeState() {
		return changeState;
	}
}