package state;

public interface State {

	public void enter();
	public void update();
	public void receive(byte[] message);
	public String request();
	public void exit();
	public String getState();
	public String changeState();
}