Account Standards:

loginRequest() 		= 36 bytes
  AccountName		= 16 bytes
  AccountPassword		= 16 bytes
  DesiredPort		= 4 bytes

Unused bytes are always filled in with 0s
These standards always follow this order.

Example package:

loginRequestDatagramPackage
|16 bytes account name|16 bytes account pass|4 bytes desires port  = total 36 bytes